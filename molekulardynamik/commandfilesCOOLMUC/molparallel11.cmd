#!/bin/bash
#SBATCH -o /home/hpc/pr63so/ga78jey2/AutoPas/molekulardynamik/MolSim11Thread.%j.%N.out
#SBATCH -D /home/hpc/pr63so/ga78jey2/AutoPas/molekulardynamik
#SBATCH -J MolSim
#SBATCH --clusters=mpp2
#SBATCH --get-user-env
#SBATCH --ntasks=11
#SBATCH --mem=800mb
#SBATCH --time=07:30:00
export OMP_NUM_THREADS=11
./MolSim ../inputfiles/setting-liquid.xml
