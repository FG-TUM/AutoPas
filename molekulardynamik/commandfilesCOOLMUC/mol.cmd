#!/bin/bash
#SBATCH -o /home/hpc/pr63so/ga78jey2/molekulardynamik/output/mol.%j.%N.out
#SBATCH -D /home/hpc/pr63so/ga78jey2/molekulardynamik
#SBATCH -J Molsim13
#SBATCH --clusters=mpp2
#SBATCH --get-user-env
#SBATCH --mem=800mb
#SBATCH --time=00:30:00
./MolSim setting-testliquid.xml
