#!/bin/bash
#SBATCH -o /home/hpc/pr63so/ga78jey2/AutoPas/molekulardynamik/MolSim15Thread.%j.%N.out
#SBATCH -D /home/hpc/pr63so/ga78jey2/AutoPas/molekulardynamik
#SBATCH -J MolSim
#SBATCH --clusters=mpp2
#SBATCH --get-user-env
#SBATCH --ntasks=15
#SBATCH --mem=800mb
#SBATCH --time=07:30:00
export OMP_NUM_THREADS=15
./MolSim ../inputfiles/setting-liquid.xml
