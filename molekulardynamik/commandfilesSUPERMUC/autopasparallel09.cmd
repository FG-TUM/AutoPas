#!/bin/bash

#SBATCH -o /home/hpc/pr63so/ga78jey2/AutoPas/molsim/AutoPas9Thread.%j.%N.out
#SBATCH -D /home/hpc/pr63so/ga78jey2/AutoPas/molsim
#SBATCH -J MolAutoPas
#SBATCH --clusters=mpp2
#SBATCH --ntasks=9
#SBATCH --get-user-env
#SBATCH --mem=800mb
#SBATCH --time=07:30:00
export OMP_NUM_THREADS=9
./MolSim ../inputfiles/setting-liquid.xml

