#!/bin/bash

#@ job_type = parallel
#@ class = test
#@ island_count=1
#@ node = 1
#@ tasks_per_node = 1
#@ job_name = molsim2.5
#@ node_usage = not_shared
#@ wall_clock_limit = 0:30:00
#@ initialdir = $(home)/AutoPas/molekulardynamik
#@ output = molsim2.5.$(schedd_host).$(jobid).out 
#@ error = molsim2.5.$(schedd_host).$(jobid).err
#@ notification=always
#@ notify_user=raffael.duell@tum.de
#@ energy_policy_tag = my_energy_tag 
#@ minimize_time_to_solution = yes
#@ queue
. /etc/profile
. /etc/profile.d/modules.sh

export OMP_NUM_THREADS=14
export KMP_AFFINITY="granularity=core,compact,1"

./MolSim ../inputfiles/setting-worldrecord2.5.xml


