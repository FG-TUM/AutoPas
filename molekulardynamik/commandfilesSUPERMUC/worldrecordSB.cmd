#!/bin/bash

#@ job_type = MPICH
#@ class = test
#@ island_count=1
#@ node = 8
#@ tasks_per_node = 32
#@ job_name = worldrecord
#@ network.MPI = sn_all,not_shared,us
#@ wall_clock_limit = 2:30:00
#@ initialdir = $(home)/AutoPas/molsim
#@ output = worldrecord.$(schedd_host).$(jobid).out 
#@ error = worldrecord.$(schedd_host).$(jobid).err
#@ notification=always
#@ notify_user=raffael.duell@tum.de
#@ energy_policy_tag = my_energy_tag 
#@ minimize_time_to_solution = yes
#@ queue
. /etc/profile
. /etc/profile.d/modules.sh
export OMP_NUM_THREADS=256
export AUTOPAS_TRAVERSAL=C08

mpiexec -n 512  ./MolSim ../inputfiles/setting-worldrecord.xml


