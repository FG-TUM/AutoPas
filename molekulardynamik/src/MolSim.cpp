
#include "outputWriter/XYZWriter.h"
#include "outputWriter/VTKWriter.h"
#include "outputWriter/XMLWriter.h"
#include "outputWriter/CSVWriter.h"
#include "input/FileReader.h"
#include "ParticleContainer.h"
#include "LinkedCellsContainer.h"

#include <list>
#include <cstring>
#include <cstdlib>
#include <iostream>
#include <memory>
#include <sys/time.h>
#include <cppunit/ui/text/TestRunner.h>
#include "tests/ParticleContainerTest.h"
#include "tests/LinkedContainerTest.h"
#include "setting/setting.h"

#include <omp.h>
#include <chrono>

//#include <log4cxx/logger.h>
//#include <log4cxx/propertyconfigurator.h>

//using namespace log4cxx;
using namespace std;


/**** forward declaration ****/
void plotParticles(int iteration, ParticleContainer* particles);
void plotParticlesXML(ParticleContainer* particles, string filename);
void plotParticlesProfile(ParticleContainer* particles, int bucketNum, float sizeX, int iteration);

double start_time = 0;
double end_time = 1000;
double delta_t = 0.014;
string outputname = "output/MD_vtk";
string profileFile = "";

//#define LOGCONFIG "LogConfig"
//static LoggerPtr logger(Logger::getLogger("MolSim"));

#define PROFILE_Y_INTERVAL 10000

/** \mainpage

\section req_sec Requirements

- xerces-c
- cppunit
- log4cxx

Using linux is advised! 


\section args_sec Running Arguments

```
export OMP_NUM_THREADS=8
./MolSim filename
```
Run the simulation with configurations defined in 'filename'.
Set the environment variable **OMP_NUM_THREADS** to define the number of threads used.

```
./MolSim -test [TestSuite]
```
Run the testsuites. A specific suite can be passed. Otherwise all suites will be processed.


\subsection cfile_ssec Configuration File

Configuration files are xml file which specify needed setting.
These start with a **setting**-element which contains follwing property-elements:
- **outputname** - Base name of the output files
- **endfile** - Optional: save last particle states into a new xml input-file
- **inputfiles** - File containing information to create particles
- **frequency** - Write frequency of the output files
- **profileFile** - Optional: Write profile of y-velocity into this file
- **profileBucketsX** - Optional: Number of divisions in x-direction to profile y-velocity
- **delta_t** - The time step between iterations
- **t_end** - Time to simulate
- **sigma** - Sigma for Lennard-Jones calculation
- **epsilon** - Epsilon for Lennard-Jones calculation
- **b_factor** - Factor for Maxwell-Boltzmann Distribution
- **g_grav_x** - Acceleration due to gravity in x-direction
- **g_grav_y** - Acceleration due to gravity in y-direction
- **g_grav_z** - Acceleration due to gravity in z-direction
- **domainX** - x-Dimension of domain
- **domainY** - y-Dimension of domain
- **domainZ** - z-Dimension of domain
- **r_cutoff** - Maximum cutoff radius
- **bc_left** - Boundary condition at x=0: 1 is reflecting boundaries, 2 is periodic boundaries, else it is an outflow
- **bc_lower** - Boundary condition at y=0: 1 is reflecting boundaries, 2 is periodic boundaries, else it is an outflow
- **bc_upper** - Boundary condition at y=domainY: 1 is reflecting boundaries, 2 is periodic boundaries, else it is an outflow
- **bc_right** - Boundary condition at x=domainX: 1 is reflecting boundaries, 2 is periodic boundaries, else it is an outflow
- **bc_front** - Boundary condition at z=0: 1 is reflecting boundaries, 2 is periodic boundaries, else it is an outflow
- **bc_back** - Boundary condition at z=domainZ: 1 is reflecting boundaries, 2 is periodic boundaries, else it is an outflow
- **thermostat** - Optional: Defines a thermostat that keeps the simulation at given temperature. This will override **b_factor**!
  - **initial** - inital temperature
  - **timestep** - number of timesteps after which the thermostat is applied
  - **ignoreY** - Optional: true if the thermostat does not affect the y-component of the velocity
  - **heating** - Optional: defines a temperature change over time
      - **target** - target temperature
      - **temperature_step** - step size in which the temperature should be changed
      - **timestep** - number of timesteps after which the temperature is changed

Example file: setting.xml

\subsection ifile_ssec Input File
Input files are xml files which specify the particles in given simulation.
These start with a particle_input-element which can contain following elements with subelements:
- **types_input** - Defines the properties of a type
  - **id** - The unique identifier of the type
  - **mass** - The mass of a particle of this type
  - **sigma** - The sigma used for the Lennard-Jones formula
  - **epsilon** - The epsilon used for the Lennard-Jones formula
  - **fixed** - Optional: true if particle cannot be moved
  - **RtruncLJ** - Optional: distance at which the Lennard-Jones force calculation should be truncated, if not specified, it will be set to Rcutoff
- **single_input** - Create a single particle
  - **coord** - Position of particle
  - **force** - Optional: Current force enacting on the particle
  - **velocity** - Initial velocity of the partilce
  - **type** - id of the type of the particle
- **cuboid_input** - Create a cuboid of particles
  - **coord** - Position of lower left front corner of the cuboid
  - **dimension** - Number of particles in each dimension
  - **mesh** - Spacing between each particle
  - **velocity** - Initial mean velocity of each particle
  - **type** - id of the type of the particle
- **sphere_input** - Create a sphere of particles
  - **coord** - Center of the sphere
  - **radius** - Number of particles as radius
  - **mesh** - Spacing between each particle
  - **velocity** - Initial mean velocity of each particle
  - **type** - id of the type of the particle
- **membrane_input** - Create a membrane of particles
  - **stiffness** - stiffness k of the membrane
  - **r_zero** - average bond length of a molecule pair
  - **force** - Optional: force which acts on certain molecules
  - **t_end_force** - Optional: time when the optional force stops to act
  - **coord_force** - mesh coordinates of the particles where the force acts
  - **coord** - Position of lower left front corner of the cuboid
  - **dimension** - Number of particles in each dimension
  - **mesh** - Spacing between each particle
  - **velocity** - Initial mean velocity of each particle
  - **type** - id of the type of the particle


Example files: eingabe-cuboid.xml

\section perf_sec Performance
The following graph compares the runtime of Linked cells algorithm to the use of a non-optimized algorithm.  
The tests were runned on 1000, 2000, 4000 and 8000 particles for 5s with a time step of 0.0005s.
 \htmlonly <style>div.image img[src="test.svg"]{width:40cm;}</style> \endhtmlonly 
 \image html performance-test/test.svg "Performance test"
 \image latex performance-test/test.eps "Performance test" width=40cm

\section thread_sec Parallelization
The following graph compares the Molecule-Updates per Second depending
on the number of threads used. The configuration file used was setting-liquid.xml
 \htmlonly <style>div.image img[src="mups.svg"]{width:40cm;}</style> \endhtmlonly 
 \image html performance-test/mups.svg "Performance test"
 \image latex performance-test/mups.eps "Performance test" width=40cm

*/


int main(int argc, char* argsv[]) {
	// configure Log4cxx with a configuration file

//	PropertyConfigurator::configure(LOGCONFIG);

//	LOG4CXX_INFO(logger, "Hello from MolSim for PSE Group 13!");
	if ((argc>1)&&(string(argsv[1]) == "-test")){ // arguments: -test [TestSuite]
		std::cout << "Tests deleted" << std::endl;
//		string testsuite = "";
//		if (argc>2){
//			testsuite = argsv[2];
//		}
//		LOG4CXX_INFO(logger, "Start Tests:");
//		CppUnit::TextUi::TestRunner runner;
//		runner.addTest(ParticleContainerTest::suite());
//		runner.addTest(LinkedContainerTest::suite());
//		runner.run(testsuite);
//		return 0;
	}
	else if (argc != 2) {  // if invalid arguments
		std::cout << "Wrong number of args" << std::endl;
//		LOG4CXX_WARN(logger, "Errounous programme call! ")
//		LOG4CXX_WARN(logger,  "./molsym xml-file")
//		LOG4CXX_WARN(logger,  "./molsym -test [TestSuite]")
		return 1;
	}

//	LOG4CXX_INFO(logger, "Read setting-file...");
	unique_ptr<setting_t> s (setting (argsv[1], xml_schema::flags::dont_validate));
	delta_t = s->delta_t();
	end_time = s->t_end();

	float b_factor = s->b_factor();

	float domainX = s->domainX();
	float domainY = s->domainY();
	float domainZ = s->domainZ();
	float r_cutoff = s->r_cutoff();

	utils::Vector<int, 6> BoundCond;

	BoundCond[0] = s->bc_left();
	BoundCond[1] = s->bc_upper();
	BoundCond[2] = s->bc_right();
	BoundCond[3] = s->bc_lower();
	BoundCond[4] = s->bc_front();
	BoundCond[5] = s->bc_back();


	if ((BoundCond[0] == 2) != (BoundCond[2] == 2) ||
			(BoundCond[1] == 2) != (BoundCond[3] == 2) ||
			(BoundCond[4] == 2) != (BoundCond[5] == 2)) {
//		LOG4CXX_WARN(logger, "Error: Opposite sides must also be set to periodic");
		return 1;
	}


	int frequency = s->frequency();

	outputname = s->outputname();
	string endfile = "";
	if (s->endfile().present()) {
		endfile = s->endfile().get();
	}

	int profileBucketsX = 0;
	if (s->profileFile().present()) {
		profileFile = s->profileFile().get();
//		LOG4CXX_INFO(logger, "Parse y-velocity profile into " << profileFile);

		if (s->profileBucketsX().present()) {
			profileBucketsX = s->profileBucketsX().get();
		}
	}

	float temperature = nan("");
	int T_timestep = 0;
	float TT_target = nan("");
	bool T_ignoreY = false;
	float TT_Tstep = nan("");
	int TT_timestep = 0;
	if (s->thermostat().present()) {
		thermo_t thermo = s->thermostat().get();
		temperature = thermo.initial();
		T_timestep = thermo.timestep();

		b_factor = sqrt(temperature);

		if (thermo.ignoreY().present()) {
			T_ignoreY = thermo.ignoreY().get();
		}

		if (thermo.heating().present()) {
			thermo_target_t thermo_target = thermo.heating().get();
			TT_target = thermo_target.target();
			TT_Tstep = thermo_target.temperature_step();
			TT_timestep = thermo_target.timestep();
		}
	}

	int threads = 1;
	#ifdef _OPENMP
	threads = omp_get_max_threads();
	#endif

	LinkedCellsContainer particles(ParticleContainer::LennardJones, delta_t, b_factor, r_cutoff, domainX, domainY, domainZ, BoundCond, threads);
	//ParticleContainer particles(ParticleContainer::LennardJones, delta_t, b_factor);

//	LOG4CXX_INFO(logger, "Read input files:");
    std::cout << "Read input files:" << std::endl;
	FileReader fileReader;
	for (setting_t::inputfiles_const_iterator i (s->inputfiles().begin());
	         i != s->inputfiles().end();
	         ++i)
	{
		const char* filename = (*i).c_str();
//		LOG4CXX_INFO(logger, "Read " << filename << "...");
        std::cout << "Read : " << filename << std::endl;
		fileReader.readFile(&particles,  filename, r_cutoff);
	}

	utils::Vector<double, 3> g;
	g[0] = s->g_grav_x();
	g[1] = s->g_grav_y();
	g[2] = s->g_grav_z();
	particles.setGravity(g);

//	LOG4CXX_INFO(logger, "Total number: " << particles.getAll().size());

	double current_time = start_time;

	int iteration = 0;
	if (!isnan(temperature)) {
			particles.setTemperature(temperature, T_ignoreY);
	}
	plotParticles(iteration, &particles);

	// the forces are needed to calculate x, but are not given in the input file.
	particles.calculateF();

	//time measurement

	timeval start_timeofday;
	timeval end_timeofday;
	gettimeofday(&start_timeofday, 0);
    std::chrono::high_resolution_clock::time_point startForceCalc, endForceCalc;
    long totalTimeForceCalc = 0;

	// for this loop, we assume: current x, current f and current v are known
	while (current_time < end_time) {

		// calculate new x
		particles.calculateX();

		// calculate new f
        startForceCalc = std::chrono::high_resolution_clock::now();
		particles.calculateF();
        endForceCalc = std::chrono::high_resolution_clock::now();

        current_time += delta_t;
        auto timeForceCalc = chrono::duration_cast<chrono::milliseconds>(endForceCalc - startForceCalc).count();
        std::cout << "Force calculation in iteration " << iteration << " took: " << timeForceCalc << " ms" << std::endl;
        totalTimeForceCalc += timeForceCalc;

		// calculate new v
		particles.calculateV();

		// test end of force up

		particles.testForceEnd(current_time);


		if (!isnan(TT_target) && temperature != TT_target)
		{
			if (iteration % TT_timestep == 0) {
				if (TT_target - temperature < TT_Tstep) {
					temperature = TT_target;
				} else {
					temperature += TT_Tstep;
				}
			}
		}

		if (!isnan(temperature)) {
			if (iteration % T_timestep == 0) {
				particles.setTemperature(temperature, T_ignoreY);
			}
		}

		if (profileBucketsX > 0 && iteration % PROFILE_Y_INTERVAL == 0) {
			plotParticlesProfile(&particles, profileBucketsX, domainX, iteration);
		}

		iteration++;
//		LOG4CXX_DEBUG(logger, "Iteration " << iteration << " finished.");
		if (frequency != 0 && iteration > 0 && iteration % frequency == 0) {
//			LOG4CXX_INFO(logger, "Plot iteration " << iteration);
			plotParticles(iteration, &particles);
		}

		current_time += delta_t;
	}

	gettimeofday(&end_timeofday, 0);
	long long int computation_sec = end_timeofday.tv_sec - start_timeofday.tv_sec;
	long long int computation_usec = 1000000*computation_sec + end_timeofday.tv_usec - start_timeofday.tv_usec;

	//get Molecule update rate per second
	double MUPS = 1000000.0*(iteration*particles.getAll().size())/computation_usec;

	//compute flops
	double flops = particles.calculateFlops();

//	LOG4CXX_INFO(logger, "Molecule update rate per second: " << MUPS);
//	LOG4CXX_INFO(logger, "Flops per second: " << 1000000.0*iteration*flops / computation_usec);
//	LOG4CXX_INFO(logger, "Runtime: " << (computation_usec / 1000000.0) << "s")
    std::cout << "STATISTICS:" << std::endl << std::fixed
              << "Molecule update rate per second: " << MUPS << std::endl
              << "Flops per second:                " << 1000000.0*iteration*flops / computation_usec << std::endl
              << "Runtime:                         " << (computation_usec / 1000000.0) << "s" << std::endl
              << "Avg Force Calculation time:      " << (totalTimeForceCalc / (double)(iteration + 1)) << " ms", (totalTimeForceCalc / (double)(iteration + 1));


	if (endfile.compare("")) {
		plotParticlesXML(&particles, endfile);
	}

//	LOG4CXX_INFO(logger, "output written. Terminating...");
	return 0;
}

/**
 * plot the y-profile to a csv file
 */
void plotParticlesProfile(ParticleContainer* particles, int bucketNum, float sizeX, int iteration) {
	list<Particle>& plotlist = particles->getAll();
	outputWriter::CSVWriter writer(bucketNum, sizeX);

	writer.plotParticles(plotlist, profileFile, iteration);
}

/**
 * plot the particles to a vtk-file
 */
void plotParticles(int iteration, ParticleContainer* particles) {
	list<Particle>& plotlist = particles->getAll();

	outputWriter::VTKWriter writer;

	writer.initializeOutput(plotlist.size());
	list<Particle>::iterator iterator = plotlist.begin();

	while (iterator != plotlist.end()) {
		Particle& p = *iterator;

		writer.plotParticle(p);

		++iterator;
	}

	writer.writeFile(outputname, iteration);
}

/**
 * plot the particles to a xml-file
 */
void plotParticlesXML(ParticleContainer* particles, string filename) {
	list<Particle>& plotlist = particles->getAll();

	outputWriter::XMLWriter writer;
	writer.initializeOutput();

	list<Particle>::iterator iterator = plotlist.begin();
	while (iterator != plotlist.end()) {
		Particle& p = *iterator;

		writer.plotParticle(p);

		++iterator;
	}

	writer.writeFile(filename);
}
