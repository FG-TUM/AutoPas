/*
 * Particle.h
 *
 *  Created on: 23.02.2010
 *      Author: eckhardw
 */

#ifndef PARTICLE_H_
#define PARTICLE_H_

#include <list>
#include "utils/Vector.h"
#include "ParticleType.h"

class Particle {

private:

	/** the position of the particle */
	utils::Vector<double, 3> x;

	/** the velocity of the particle */
	utils::Vector<double, 3> v;

	/** the force effective on this particle */
	utils::Vector<double, 3> f;

	/** the force which was effective on this particle */
	utils::Vector<double, 3> old_f;

	/** a constant force always effective on this particle */
	utils::Vector<double, 3> const_f;

	/** the gravity acting on the particle */
	utils::Vector<double, 3> ggrav;

	/** list of pointers to all neighbouring particles */
	std::array<Particle*,4> direct_neighbours;
	std::array<Particle*,4> diagonal_neighbours;

	/** type of the particle. Use it for whatever you want (e.g. to seperate
	 * molecules belonging to different bodies, matters, and so on)
	 */
	int type;
	int type_id;




public:
	Particle(int type = 0);

	Particle(const Particle& other);

	Particle(
			// for visualization, we need always 3 coordinates
			// -> in case of 2d, we use only the first and the second
			utils::Vector<double, 3> x_arg,
	        utils::Vector<double, 3> v_arg,
	        int type = 0
	);

	virtual ~Particle();

	utils::Vector<double, 3>& getX();

	utils::Vector<double, 3>& getF();

	utils::Vector<double, 3>& getOldF();

	utils::Vector<double, 3>& getConstF();

	utils::Vector<double, 3>& getV();

	utils::Vector<double, 3>& getGrav();

	void actualiseNeighbours(Particle* newptr);

	double getM();
	double getHpart();
	double getSigma();
	double getRtruncLJ();
	bool getFixed();

	int getType();
	int getTypeID();

	std::array<Particle*,4>& getDirectNeighbours();

	std::array<Particle*,4>& getDiagonalNeighbours();

	bool operator==(Particle& other);

	std::string toString();
};

std::ostream& operator<<(std::ostream& stream, Particle& p);

inline utils::Vector<double, 3>& Particle::getX() {
	return x;
}

inline utils::Vector<double, 3>& Particle::getV() {
	return v;
}

inline utils::Vector<double, 3>& Particle::getF() {
	return f;
}

inline utils::Vector<double, 3>& Particle::getOldF() {
	return old_f;
}

inline utils::Vector<double, 3>& Particle::getConstF() {
	return const_f;
}

inline utils::Vector<double, 3>& Particle::getGrav(){
	return ggrav;
}

inline double Particle::getM() {
	return ParticleType::getM(type_id);
}
inline double Particle::getHpart() {
	return ParticleType::getHpart(type_id);
}
inline double Particle::getSigma() {
	return ParticleType::getSigma(type_id);
}
inline double Particle::getRtruncLJ() {
	return ParticleType::getRtruncLJ(type_id);
}
inline bool Particle::getFixed() {
	return ParticleType::getFixed(type_id);
}

inline int Particle::getType() {
	return type;
}

inline int Particle::getTypeID() {
	return type_id;
}

inline std::array<Particle*,4>& Particle::getDirectNeighbours() {
	return direct_neighbours;
}

inline std::array<Particle*,4>& Particle::getDiagonalNeighbours(){
	return diagonal_neighbours;
}


#endif /* PARTICLE_H_ */
