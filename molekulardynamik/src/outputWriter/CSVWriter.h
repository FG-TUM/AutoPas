/*
 * CSVWriter.h
 *
 *  Created on: 15.01.2018
 *      Author: nguyenj
 */

#ifndef CSVWRITER_H_
#define CSVWRITER_H_

#include "Particle.h"
#include <fstream>
#include <list>

namespace outputWriter {

class CSVWriter {

private:
	int bucketNum;
	float sizeX;

public:
	CSVWriter(int bucketNum, float sizeX);

	virtual ~CSVWriter();

	void plotParticles(std::list<Particle> particles, const std::string& filename, int iteration);

};

}

#endif /* CSVWRITER_H_ */
