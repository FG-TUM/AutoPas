/*
 * XMLWriter.cpp
 *
 *  Created on: Dec 14, 2017
 *      Author: jan
 */

#include "XMLWriter.h"
#include "ParticleType.h"
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>

//#include <log4cxx/logger.h>
//
//using namespace log4cxx;
//static LoggerPtr logger(Logger::getLogger("MolSim.outputWriter.XMLWriter"));

using namespace std;

namespace outputWriter {

XMLWriter::XMLWriter() {
	inputFile = NULL;
}

XMLWriter::~XMLWriter() {
	// TODO Auto-generated destructor stub
}

void XMLWriter::initializeOutput() {

	inputFile = new input_t();

	// write all used types
	vector<int> types = ParticleType::getTypes();
	for (vector<int>::iterator it = types.begin(); it != types.end(); ++it) {
		int id = *it;
		ParticleType& pt = ParticleType::getType(id);
		particletype_t pt_xml(id, pt.getM(), pt.getSigma(), pt.getEpsilon(), 0.);
		inputFile->types_input().push_back(pt_xml);
	}

}

void XMLWriter::writeFile(const std::string& filename) {

//	LOG4CXX_DEBUG(logger, "Started xml file writer");
	std::ofstream file (filename.c_str());
	particle_input(file, *inputFile);
	delete inputFile;
}

void XMLWriter::plotParticle(Particle& p) {

	utils::Vector<double, 3> x = p.getX();
	vector_t pos(x[0], x[1], x[2]);

	utils::Vector<double, 3> f = p.getF();
	vector_t force(f[0], f[1], f[2]);

	utils::Vector<double, 3> v = p.getV();
	vector_t velocity(v[0], v[1], v[2]);

	single_t sinput = single_t(pos, velocity, p.getType());
	sinput.force(force);
	inputFile->single_input().push_back(sinput);
}

} /* namespace outputWriter */
