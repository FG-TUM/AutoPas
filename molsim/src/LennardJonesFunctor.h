/* GravityFunctor.h
 *
 *  Created on: Apr 6, 2018
 *      Author: raffi
 */

#ifndef SRC_LENNARDJONESFUNCTOR_H_
#define SRC_LENNARDJONESFUNCTOR_H_

#include "../mdutils.h"
#include "MoleculeMS.h"

#include <assert.h>
#include <array>
#include <cmath>

using namespace autopas;

template <class Particle, class ParticleCell, class SoAArraysType = typename Particle::SoAArraysType>
class LennardJonesFunctor : public Functor<Particle, ParticleCell, SoAArraysType> {
 public:
  /**@brief Constructor of the functor
   * @param cut-off radius
   */
  LennardJonesFunctor<Particle, ParticleCell, SoAArraysType>(double rcut) {
    r_cutoff = rcut;
    r_cutoff_square = rcut * rcut;
  }

  /**
   * @brief Functor for arrays of structures (AoS).
   *
   * This functor should calculate the forces or any other pair-wise interaction
   * between two particles.
   * This should include a cutoff check if needed!
   */
  void AoSFunctor(Particle &i, Particle &j, bool newton3 = true) override {
    ArrayMath am;

    std::array<double, 3> r = am.sub(i.getR(), j.getR());

    double rabs2 = am.dot(r, r);
    assert(!(rabs2 == 0.0));

    if (rabs2 < r_cutoff_square) {
      double sigma = (i.getSigma() + j.getSigma()) / 2;
      double epsilon = ParticleType::getEpsilon(i.getTypeID(), j.getTypeID());

      double invrabs2 = 1. / rabs2;
      double fraction2 = sigma * sigma * invrabs2;
      double fraction6 = fraction2 * fraction2 * fraction2;
      double fraction12 = fraction6 * fraction6;

      std::array<double, 3> addforce = am.mulScalar(r, 24 * epsilon * invrabs2 * (fraction6 - 2 * fraction12));

      j.addF(addforce);
      i.subF(addforce);
    }
  }

  /**TODO
   * @brief Functor for structure of arrays (SoA)
   *
   * This functor should calculate the forces or any other pair-wise interaction
   * between all particles in soa.
   * This should include a cutoff check if needed!
   *
   * @param soa Structure of arrays
   */
  void SoAFunctor(SoA<SoAArraysType> &soa, bool newton3 = true) override {
    if (soa.getNumParticles() == 0) return;

    double *const __restrict__ posX = soa.template begin<MoleculeMS::posX>();
    double *const __restrict__ posY = soa.template begin<MoleculeMS::posY>();
    double *const __restrict__ posZ = soa.template begin<MoleculeMS::posZ>();

    double *const __restrict__ forceX = soa.template begin<MoleculeMS::forceX>();
    double *const __restrict__ forceY = soa.template begin<MoleculeMS::forceY>();
    double *const __restrict__ forceZ = soa.template begin<MoleculeMS::forceZ>();

    int *const __restrict__ uniquetypeID = soa.template begin<MoleculeMS::uniquetypeID>();
    double *const __restrict__ sigma = soa.template begin<MoleculeMS::sigma>();
    double *const __restrict__ epsilon = soa.template begin<MoleculeMS::epsilon>();

    for (unsigned int i = 0; i < soa.getNumParticles(); ++i) {
      double fxacc = 0;
      double fyacc = 0;
      double fzacc = 0;

#pragma omp simd reduction(+ : fxacc, fyacc, fzacc)
      for (unsigned int j = i + 1; j < soa.getNumParticles(); ++j) {
        if (i == j) continue;

        const double drx = posX[i] - posX[j];
        const double dry = posY[i] - posY[j];
        const double drz = posZ[i] - posZ[j];

        const double drx2 = drx * drx;
        const double dry2 = dry * dry;
        const double drz2 = drz * drz;

        const double dr2 = drx2 + dry2 + drz2;

        if (dr2 > r_cutoff_square) continue;

        // apply mixing rules
        const double SIGMA = (sigma[i] + sigma[j]) / 2;

        double EPSILON = 0;
        if (epsilon[i] == epsilon[j]) {
          EPSILON = epsilon[i];
        } else if (epsilon[j] > epsilon[i]) {
          EPSILON = epsilon_pairs[uniquetypeID[j] * (uniquetypeID[j] - 1) / 2 + uniquetypeID[i]];
        } else {
          EPSILON = epsilon_pairs[uniquetypeID[i] * (uniquetypeID[i] - 1) / 2 + uniquetypeID[j]];
        }

        const double invdr2 = 1. / dr2;
        const double frac2 = SIGMA * SIGMA * invdr2;
        const double frac6 = frac2 * frac2 * frac2;
        const double frac12 = frac6 * frac6;
        const double factor = 24. * EPSILON * (frac6 - 2 * frac12) * invdr2;

        const double fx = drx * factor;
        const double fy = dry * factor;
        const double fz = drz * factor;

        fxacc -= fx;
        fyacc -= fy;
        fzacc -= fz;

        forceX[j] += fx;
        forceY[j] += fy;
        forceZ[j] += fz;
      }

      forceX[i] += fxacc;
      forceY[i] += fyacc;
      forceZ[i] += fzacc;
    }
  }

  /**
   * @brief Functor for structure of arrays (SoA)
   *
   * This functor should calculate the forces or any other pair-wise interaction
   * between all particles of soa1 and soa2.
   * This should include a cutoff check if needed!
   *
   * @param soa1 First structure of arrays.
   * @param soa2 Second structure of arrays.
   */
  void SoAFunctor(SoA<SoAArraysType> &soa1, SoA<SoAArraysType> &soa2, bool newton3 = true) override {
    if (soa1.getNumParticles() == 0 || soa2.getNumParticles() == 0) return;

    // first cell
    unsigned long *const __restrict__ ID1 = soa1.template begin<MoleculeMS::id>();

    double *const __restrict__ posX1 = soa1.template begin<MoleculeMS::posX>();
    double *const __restrict__ posY1 = soa1.template begin<MoleculeMS::posY>();
    double *const __restrict__ posZ1 = soa1.template begin<MoleculeMS::posZ>();

    double *const __restrict__ forceX1 = soa1.template begin<MoleculeMS::forceX>();
    double *const __restrict__ forceY1 = soa1.template begin<MoleculeMS::forceY>();
    double *const __restrict__ forceZ1 = soa1.template begin<MoleculeMS::forceZ>();

    int *const __restrict__ uniquetypeID1 = soa1.template begin<MoleculeMS::uniquetypeID>();
    double *const __restrict__ sigma1 = soa1.template begin<MoleculeMS::sigma>();
    double *const __restrict__ epsilon1 = soa1.template begin<MoleculeMS::epsilon>();

    // second cell
    unsigned long *const __restrict__ ID2 = soa2.template begin<MoleculeMS::id>();

    double *const __restrict__ posX2 = soa2.template begin<MoleculeMS::posX>();
    double *const __restrict__ posY2 = soa2.template begin<MoleculeMS::posY>();
    double *const __restrict__ posZ2 = soa2.template begin<MoleculeMS::posZ>();

    double *const __restrict__ forceX2 = soa2.template begin<MoleculeMS::forceX>();
    double *const __restrict__ forceY2 = soa2.template begin<MoleculeMS::forceY>();
    double *const __restrict__ forceZ2 = soa2.template begin<MoleculeMS::forceZ>();

    int *const __restrict__ uniquetypeID2 = soa2.template begin<MoleculeMS::uniquetypeID>();
    double *const __restrict__ sigma2 = soa2.template begin<MoleculeMS::sigma>();
    double *const __restrict__ epsilon2 = soa2.template begin<MoleculeMS::epsilon>();

    for (unsigned int i = 0; i < soa1.getNumParticles(); ++i) {
      double fxacc = 0;
      double fyacc = 0;
      double fzacc = 0;

#pragma omp simd reduction(+ : fxacc, fyacc, fzacc)
      for (unsigned int j = 0; j < soa2.getNumParticles(); ++j) {
        if (ID1[i] == ID2[j]) continue;

        const double drx = posX1[i] - posX2[j];
        const double dry = posY1[i] - posY2[j];
        const double drz = posZ1[i] - posZ2[j];

        const double drx2 = drx * drx;
        const double dry2 = dry * dry;
        const double drz2 = drz * drz;

        const double dr2 = drx2 + dry2 + drz2;

        if (dr2 > r_cutoff_square) continue;

        // apply mixing rules
        const double SIGMA = (sigma1[i] + sigma2[j]) / 2;

        double EPSILON = 0;
        if (epsilon1[i] == epsilon2[j]) {
          EPSILON = epsilon1[i];
        } else if (epsilon2[j] > epsilon1[i]) {
          EPSILON = epsilon_pairs[uniquetypeID2[j] * (uniquetypeID2[j] - 1) / 2 + uniquetypeID1[i]];
        } else {
          EPSILON = epsilon_pairs[uniquetypeID1[i] * (uniquetypeID1[i] - 1) / 2 + uniquetypeID2[j]];
        }

        const double invdr2 = 1. / dr2;
        const double frac2 = SIGMA * SIGMA * invdr2;
        const double frac6 = frac2 * frac2 * frac2;
        const double frac12 = frac6 * frac6;
        const double factor = 24. * EPSILON * (frac6 - 2 * frac12) * invdr2;

        const double fx = drx * factor;
        const double fy = dry * factor;
        const double fz = drz * factor;

        fxacc -= fx;
        fyacc -= fy;
        fzacc -= fz;

        forceX2[j] += fx;
        forceY2[j] += fy;
        forceZ2[j] += fz;
      }

      forceX1[i] += fxacc;
      forceY1[i] += fyacc;
      forceZ1[i] += fzacc;
    }
  }
  /**
   * functor used for Verlet-lists
   */
  void SoAFunctor(SoA<SoAArraysType> &soa,
                  const std::vector<std::vector<size_t, autopas::AlignedAllocator<size_t>>> &neighbourList,
                  size_t iFrom, size_t iTo, bool newton3 = true) override {
    auto numParts = soa.getNumParticles();

    if (numParts == 0)
      ;

    double *const __restrict__ posX = soa.template begin<MoleculeMS::posX>();
    double *const __restrict__ posY = soa.template begin<MoleculeMS::posY>();
    double *const __restrict__ posZ = soa.template begin<MoleculeMS::posZ>();

    double *const __restrict__ forceX = soa.template begin<MoleculeMS::forceX>();
    double *const __restrict__ forceY = soa.template begin<MoleculeMS::forceY>();
    double *const __restrict__ forceZ = soa.template begin<MoleculeMS::forceZ>();

    int *const __restrict__ uniquetypeID = soa.template begin<MoleculeMS::uniquetypeID>();
    double *const __restrict__ sigma = soa.template begin<MoleculeMS::sigma>();
    double *const __restrict__ epsilon = soa.template begin<MoleculeMS::epsilon>();

    for (unsigned int i = iFrom; i < iTo; ++i) {
      double fxacc = 0;
      double fyacc = 0;
      double fzacc = 0;

      const size_t listSizeI = neighbourList[i].size();
      const size_t *const __restrict__ currentList = neighbourList[i].data();

#ifdef __AV512F__
      const size_t vecsize = 16;
#else
      const size_t vecsize = 12;
#endif
      size_t joff = 0;

      if (listSizeI >= vecsize) {
        alignas(64) std::array<double, vecsize> xtmp, ytmp, ztmp, sigmatmp, epsilontmp, typeIDtmp, xArr, yArr, zArr,
            fxArr, fyArr, fzArr, sigmaArr, epsilonArr, typeIDArr;

        for (size_t tmpj = 0; tmpj < vecsize; tmpj++) {
          xtmp[tmpj] = posX[i];
          ytmp[tmpj] = posY[i];
          ztmp[tmpj] = posZ[i];

          sigmatmp[tmpj] = sigma[i];
          epsilontmp[tmpj] = epsilon[i];
          typeIDtmp[tmpj] = uniquetypeID[i];
        }

        // loop over the verlet list
        for (; joff < listSizeI - vecsize + 1; joff += vecsize) {
#pragma omp simd safelen(vecsize)
          for (size_t tmpj = 0; tmpj < vecsize; tmpj++) {
            xArr[tmpj] = posX[currentList[joff + tmpj]];
            yArr[tmpj] = posY[currentList[joff + tmpj]];
            zArr[tmpj] = posZ[currentList[joff + tmpj]];

            sigmaArr[tmpj] = sigma[currentList[joff + tmpj]];
            epsilonArr[tmpj] = epsilon[currentList[joff + tmpj]];
            typeIDArr[tmpj] = uniquetypeID[currentList[joff + tmpj]];
          }

#pragma omp simd reduction(+ : fxacc, fyacc, fzacc) safelen(vecsize)
          for (size_t j = 0; j < vecsize; j++) {
            const double drx = xtmp[j] - xArr[j];
            const double dry = ytmp[j] - yArr[j];
            const double drz = ztmp[j] - zArr[j];

            const double drx2 = drx * drx;
            const double dry2 = dry * dry;
            const double drz2 = drz * drz;

            const double dr2 = drx2 + dry2 + drz2;

            if (dr2 > r_cutoff_square) continue;

            // apply mixing rules
            const double SIGMA = (sigmatmp[j] + sigmaArr[j]) / 2;

            double EPSILON = 0;
            if (epsilontmp[j] == epsilonArr[j]) {
              EPSILON = epsilonArr[j];
            } else if (epsilonArr[j] > epsilontmp[j]) {
              EPSILON = epsilon_pairs[typeIDArr[j] * (typeIDArr[j] - 1) / 2 + typeIDtmp[j]];
            } else {
              EPSILON = epsilon_pairs[typeIDtmp[j] * (typeIDtmp[j] - 1) / 2 + typeIDArr[j]];
            }

            const double invdr2 = 1. / dr2;
            const double frac2 = SIGMA * SIGMA * invdr2;
            const double frac6 = frac2 * frac2 * frac2;
            const double frac12 = frac6 * frac6;
            const double factor = 24. * EPSILON * (frac6 - 2 * frac12) * invdr2;

            const double fx = drx * factor;
            const double fy = dry * factor;
            const double fz = drz * factor;

            fxacc -= fx;
            fyacc -= fy;
            fzacc -= fz;

            fxArr[j] = fx;
            fyArr[j] = fy;
            fzArr[j] = fz;
          }

#pragma omp simd safelen(vecsize)
          for (size_t tmpj = 0; tmpj < vecsize; tmpj++) {
            const size_t j = currentList[joff + tmpj];
            forceX[j] += fxArr[tmpj];
            forceY[j] += fyArr[tmpj];
            forceZ[j] += fzArr[tmpj];
          }
        }
      }

      for (size_t jNeighIndex = joff; jNeighIndex < listSizeI; ++jNeighIndex) {
        size_t j = neighbourList[i][jNeighIndex];
        if (i == j) continue;

        const double drx = posX[i] - posX[j];
        const double dry = posY[i] - posY[j];
        const double drz = posZ[i] - posZ[j];

        const double drx2 = drx * drx;
        const double dry2 = dry * dry;
        const double drz2 = drz * drz;

        const double dr2 = drx2 + dry2 + drz2;

        if (dr2 > r_cutoff_square) continue;

        // apply mixing rules
        const double SIGMA = (sigma[i] + sigma[j]) / 2;

        double EPSILON = 0;
        if (epsilon[i] == epsilon[j]) {
          EPSILON = epsilon[i];
        } else if (epsilon[j] > epsilon[i]) {
          EPSILON = epsilon_pairs[uniquetypeID[j] * (uniquetypeID[j] - 1) / 2 + uniquetypeID[i]];
        } else {
          EPSILON = epsilon_pairs[uniquetypeID[i] * (uniquetypeID[i] - 1) / 2 + uniquetypeID[j]];
        }

        const double invdr2 = 1. / dr2;
        const double frac2 = SIGMA * SIGMA * invdr2;
        const double frac6 = frac2 * frac2 * frac2;
        const double frac12 = frac6 * frac6;
        const double factor = 24. * EPSILON * (frac6 - 2 * frac12) * invdr2;

        const double fx = drx * factor;
        const double fy = dry * factor;
        const double fz = drz * factor;

        fxacc -= fx;
        fyacc -= fy;
        fzacc -= fz;

        forceX[j] += fx;
        forceY[j] += fy;
        forceZ[j] += fz;
      }

      forceX[i] += fxacc;
      forceY[i] += fyacc;
      forceZ[i] += fzacc;
    }
  }

  /**TODO
   * @brief Copies the AoS data of the given cell in the given soa.
   *
   * @param cell Cell from where the data is loaded.
   * @param soa  Structure of arrays where the data is copied to.
   */
  AUTOPAS_FUNCTOR_SOALOADER(cell, _soa, offset,

                            _soa.resizeArrays(offset + cell.numParticles());

                            if (cell.numParticles() == 0) return;

                            unsigned long *const __restrict__ id = _soa.template begin<MoleculeMS::id>();
                            double *const __restrict__ posX = _soa.template begin<MoleculeMS::posX>();
                            double *const __restrict__ posY = _soa.template begin<MoleculeMS::posY>();
                            double *const __restrict__ posZ = _soa.template begin<MoleculeMS::posZ>();

                            double *const __restrict__ forceX = _soa.template begin<MoleculeMS::forceX>();
                            double *const __restrict__ forceY = _soa.template begin<MoleculeMS::forceY>();
                            double *const __restrict__ forceZ = _soa.template begin<MoleculeMS::forceZ>();

                            int *const __restrict__ uniquetypeID = _soa.template begin<MoleculeMS::uniquetypeID>();

                            double *const __restrict__ sigma = _soa.template begin<MoleculeMS::sigma>();

                            double *const __restrict__ epsilon = _soa.template begin<MoleculeMS::epsilon>();

                            auto it = cell.begin();

                            for (size_t i = offset; it.isValid(); ++it, ++i) {
                              id[i] = it->getID();
                              posX[i] = it->getR()[0];
                              posY[i] = it->getR()[1];
                              posZ[i] = it->getR()[2];
                              forceX[i] = it->getF()[0];
                              forceY[i] = it->getF()[1];
                              forceZ[i] = it->getF()[2];
                              uniquetypeID[i] = it->getTypeID();
                              sigma[i] = ParticleType::getSigma(uniquetypeID[i]);
                              epsilon[i] = ParticleType::getEpsilon(uniquetypeID[i]);
                            })

  /**
   * @brief Copies the data stored in the soa in the cell.
   *
   * @param cell Cell where the data should be stored.
   * @param soa  Structure of arrays from where the data is loaded.
   */
  AUTOPAS_FUNCTOR_SOAEXTRACTOR(cell, _soa, offset, if (_soa.getNumParticles() == 0) return;
                               //#ifndef NDEBUG
                               //		  unsigned long *const __restrict__ id = soa.template
                               //begin<MoleculeMS::id>(); #endif

                               double *const __restrict__ forceX = _soa.template begin<MoleculeMS::forceX>();
                               double *const __restrict__ forceY = _soa.template begin<MoleculeMS::forceY>();
                               double *const __restrict__ forceZ = _soa.template begin<MoleculeMS::forceZ>();

                               auto it = cell.begin();

                               for (unsigned long i = offset; i < _soa.getNumParticles(); ++i, ++it) {
                                 //			  assert(id[i] == it->getID());
                                 std::array<double, 3> f = {forceX[i], forceY[i], forceZ[i]};
                                 it->setF(f);
                               })

  void setEpsilonPairs(std::vector<float> pairs) { epsilon_pairs = pairs; }

 private:
  double r_cutoff;
  double r_cutoff_square;
  std::vector<float> epsilon_pairs;
};

#endif /* SRC_LENNARDJONESFUNCTOR_H_ */
