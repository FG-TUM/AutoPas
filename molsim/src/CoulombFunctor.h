/* GravityFunctor.h
 *
 *  Created on: May 28, 2018
 *      Author: raffi
 */

#ifndef SRC_COULOMBFUNCTOR_H_
#define SRC_COULOMBFUNCTOR_H_

#include "HelperFunctions.h"
#include "MoleculeMS.h"
#include "autopas/autopasIncludes.h"

#include <assert.h>
#include <array>
#include <cmath>

using namespace autopas;

const double CoulombConstant = 138935.45755;

template <class Particle, class ParticleCell, class SoAArraysType = typename Particle::SoAArraysType>
class CoulombFunctor : public Functor<Particle, ParticleCell, SoAArraysType> {
 public:
  /**@brief Constructor of the functor
   * @param cut-off radius
   */
  CoulombFunctor<Particle, ParticleCell, SoAArraysType>(double rcut) {
    r_cutoff = rcut;
    r_cutoff_square = rcut * rcut;
  }

  /**
   * @brief Functor for arrays of structures (AoS).
   *
   * This functor should calculate the forces or any other pair-wise interaction
   * between two particles.
   * This should include a cutoff check if needed!
   */
  void AoSFunctor(Particle &i, Particle &j, bool newton3 = true) override {
    ArrayMath am;

    std::array<double, 3> r = am.sub(i.getR(), j.getR());

    double rabs2 = am.dot(r, r);
    assert(!(rabs2 == 0.0));

    // save the old torque
    i.getOldA() = i.getA();
    j.getOldA() = j.getA();

    std::array<double, 3> zero = {0, 0, 0};

    // variable to store the newly computed torque
    std::array<double, 3> iTorque = zero;
    std::array<double, 3> jTorque = zero;

    // get the used properties of the particles to avoid multiple function calls
    std::vector<double> iCharge = i.getChargePart();
    std::vector<double> jCharge = j.getChargePart();

    std::vector<double> iPosX = i.getRealPosX();
    std::vector<double> iPosY = i.getRealPosY();
    std::vector<double> iPosZ = i.getRealPosZ();
    std::vector<double> jPosX = j.getRealPosX();
    std::vector<double> jPosY = j.getRealPosY();
    std::vector<double> jPosZ = j.getRealPosZ();

    if (rabs2 < r_cutoff_square) {
      for (int n = 0; n < i.getNumPart(); ++n) {
        for (int m = 0; m < j.getNumPart(); ++m) {
          double mixed_charge = iCharge[n] * jCharge[m];

          std::array<double, 3> iPos = {iPosX[n], iPosY[n], iPosZ[n]};
          std::array<double, 3> jPos = {jPosX[m], jPosY[m], jPosZ[m]};
          std::array<double, 3> rPart = am.sub(iPos, jPos);

          double rabs2Part = am.dot(rPart, rPart);
          assert(!(rabs2Part == 0.0));

          double rabsPart = sqrt(rabs2Part);

          double factor = CoulombConstant * mixed_charge / (rabsPart * rabs2Part);

          std::array<double, 3> addforce = am.mulScalar(rPart, factor);

          j.subF(addforce);
          i.addF(addforce);

          iTorque = am.add(iTorque, crossProduct(am.sub(iPos, i.getR()), addforce));
          jTorque = am.add(jTorque, crossProduct(am.sub(j.getR(), jPos), addforce));
        }
      }
    }
    i.setA(iTorque);
    j.setA(jTorque);
  }

  /**TODO
   * @brief Functor for structure of arrays (SoA)
   *
   * This functor should calculate the forces or any other pair-wise interaction
   * between all particles in soa.
   * This should include a cutoff check if needed!
   *
   * @param soa Structure of arrays
   */
  void SoAFunctor(SoA<SoAArraysType> &soa, bool newton3 = true) override {
    if (soa.getNumParticles() == 0) return;

    unsigned long *const __restrict__ ID = soa.template begin<MoleculeMS::id>();

    double *const __restrict__ posX = soa.template begin<MoleculeMS::posX>();
    double *const __restrict__ posY = soa.template begin<MoleculeMS::posY>();
    double *const __restrict__ posZ = soa.template begin<MoleculeMS::posZ>();

    double *const __restrict__ forceX = soa.template begin<MoleculeMS::forceX>();
    double *const __restrict__ forceY = soa.template begin<MoleculeMS::forceY>();
    double *const __restrict__ forceZ = soa.template begin<MoleculeMS::forceZ>();

    int *const __restrict__ uniquetypeID = soa.template begin<MoleculeMS::uniquetypeID>();

    double *const __restrict__ charge = soa.template begin<MoleculeMS::charge>();

    for (unsigned int i = 0; i < soa.getNumParticles(); ++i) {
      double fxacc = 0;
      double fyacc = 0;
      double fzacc = 0;

#pragma omp simd reduction(+ : fxacc, fyacc, fzacc)
      for (unsigned int j = i + 1; j < soa.getNumParticles(); ++j) {
        if (ID[i] == ID[j]) continue;

        const double drx = posX[i] - posX[j];
        const double dry = posY[i] - posY[j];
        const double drz = posZ[i] - posZ[j];

        const double drx2 = drx * drx;
        const double dry2 = dry * dry;
        const double drz2 = drz * drz;

        const double dr2 = drx2 + dry2 + drz2;

        if (dr2 > r_cutoff_square) continue;

        // apply mixing rules
        const double mixed_charges = charge[i] * charge[j];

        const double dr = sqrt(dr2);

        const double factor = -CoulombConstant * mixed_charges / (dr2 * dr);

        const double fx = drx * factor;
        const double fy = dry * factor;
        const double fz = drz * factor;

        fxacc -= fx;
        fyacc -= fy;
        fzacc -= fz;

        forceX[j] += fx;
        forceY[j] += fy;
        forceZ[j] += fz;
      }

      forceX[i] += fxacc;
      forceY[i] += fyacc;
      forceZ[i] += fzacc;
    }
  }

  /**
   * @brief Functor for structure of arrays (SoA)
   *
   * This functor should calculate the forces or any other pair-wise interaction
   * between all particles of soa1 and soa2.
   * This should include a cutoff check if needed!
   *
   * @param soa1 First structure of arrays.
   * @param soa2 Second structure of arrays.
   */
  void SoAFunctor(SoA<SoAArraysType> &soa1, SoA<SoAArraysType> &soa2, bool newton3 = true) override {
    if (soa1.getNumParticles() == 0 || soa2.getNumParticles() == 0) return;

    // first cell
    unsigned long *const __restrict__ ID1 = soa1.template begin<MoleculeMS::id>();

    double *const __restrict__ posX1 = soa1.template begin<MoleculeMS::posX>();
    double *const __restrict__ posY1 = soa1.template begin<MoleculeMS::posY>();
    double *const __restrict__ posZ1 = soa1.template begin<MoleculeMS::posZ>();

    double *const __restrict__ forceX1 = soa1.template begin<MoleculeMS::forceX>();
    double *const __restrict__ forceY1 = soa1.template begin<MoleculeMS::forceY>();
    double *const __restrict__ forceZ1 = soa1.template begin<MoleculeMS::forceZ>();

    int *const __restrict__ uniquetypeID1 = soa1.template begin<MoleculeMS::uniquetypeID>();

    double *const __restrict__ charge1 = soa1.template begin<MoleculeMS::charge>();

    // second cell
    unsigned long *const __restrict__ ID2 = soa2.template begin<MoleculeMS::id>();

    double *const __restrict__ posX2 = soa2.template begin<MoleculeMS::posX>();
    double *const __restrict__ posY2 = soa2.template begin<MoleculeMS::posY>();
    double *const __restrict__ posZ2 = soa2.template begin<MoleculeMS::posZ>();

    double *const __restrict__ forceX2 = soa2.template begin<MoleculeMS::forceX>();
    double *const __restrict__ forceY2 = soa2.template begin<MoleculeMS::forceY>();
    double *const __restrict__ forceZ2 = soa2.template begin<MoleculeMS::forceZ>();

    int *const __restrict__ uniquetypeID2 = soa2.template begin<MoleculeMS::uniquetypeID>();

    double *const __restrict__ charge2 = soa2.template begin<MoleculeMS::charge>();

    for (unsigned int i = 0; i < soa1.getNumParticles(); ++i) {
      double fxacc = 0;
      double fyacc = 0;
      double fzacc = 0;

#pragma omp simd reduction(+ : fxacc, fyacc, fzacc)
      for (unsigned int j = 0; j < soa2.getNumParticles(); ++j) {
        if (ID1[i] == ID2[j]) continue;

        const double drx = posX1[i] - posX2[j];
        const double dry = posY1[i] - posY2[j];
        const double drz = posZ1[i] - posZ2[j];

        const double drx2 = drx * drx;
        const double dry2 = dry * dry;
        const double drz2 = drz * drz;

        const double dr2 = drx2 + dry2 + drz2;

        if (dr2 > r_cutoff_square) continue;

        // apply mixing rules
        const double mixed_charges = charge1[i] * charge2[j];

        const double dr = sqrt(dr2);

        const double factor = -CoulombConstant * mixed_charges / (dr2 * dr);

        const double fx = drx * factor;
        const double fy = dry * factor;
        const double fz = drz * factor;

        fxacc -= fx;
        fyacc -= fy;
        fzacc -= fz;

        forceX2[j] += fx;
        forceY2[j] += fy;
        forceZ2[j] += fz;
      }

      forceX1[i] += fxacc;
      forceY1[i] += fyacc;
      forceZ1[i] += fzacc;
    }
  }
  /**
   * functor used for Verlet-lists
   */
  void SoAFunctor(SoA<SoAArraysType> &soa,
                  const std::vector<std::vector<size_t, autopas::AlignedAllocator<size_t>>> &neighbourList,
                  size_t iFrom, size_t iTo, bool newton3 = true) override {
    auto numParts = soa.getNumParticles();

    if (numParts == 0)
      ;

    double *const __restrict__ posX = soa.template begin<MoleculeMS::posX>();
    double *const __restrict__ posY = soa.template begin<MoleculeMS::posY>();
    double *const __restrict__ posZ = soa.template begin<MoleculeMS::posZ>();

    double *const __restrict__ forceX = soa.template begin<MoleculeMS::forceX>();
    double *const __restrict__ forceY = soa.template begin<MoleculeMS::forceY>();
    double *const __restrict__ forceZ = soa.template begin<MoleculeMS::forceZ>();

    int *const __restrict__ uniquetypeID = soa.template begin<MoleculeMS::uniquetypeID>();

    double *const __restrict__ charge = soa.template begin<MoleculeMS::charge>();

    for (unsigned int i = iFrom; i < iTo; ++i) {
      double fxacc = 0;
      double fyacc = 0;
      double fzacc = 0;

      const size_t listSizeI = neighbourList[i].size();
      const size_t *const __restrict__ currentList = neighbourList[i].data();

#ifdef __AV512F__
      const size_t vecsize = 16;
#else
      const size_t vecsize = 12;
#endif
      size_t joff = 0;

      if (listSizeI >= vecsize) {
        alignas(64) std::array<double, vecsize> xtmp, ytmp, ztmp, chargetmp, typeIDtmp, xArr, yArr, zArr, fxArr, fyArr,
            fzArr, chargeArr, typeIDArr;

        for (size_t tmpj = 0; tmpj < vecsize; tmpj++) {
          xtmp[tmpj] = posX[i];
          ytmp[tmpj] = posY[i];
          ztmp[tmpj] = posZ[i];

          chargetmp[tmpj] = charge[i];
          typeIDtmp[tmpj] = uniquetypeID[i];
        }

        // loop over the verlet list
        for (; joff < listSizeI - vecsize + 1; joff += vecsize) {
#pragma omp simd safelen(vecsize)
          for (size_t tmpj = 0; tmpj < vecsize; tmpj++) {
            xArr[tmpj] = posX[currentList[joff + tmpj]];
            yArr[tmpj] = posY[currentList[joff + tmpj]];
            zArr[tmpj] = posZ[currentList[joff + tmpj]];

            chargeArr[tmpj] = charge[currentList[joff + tmpj]];
            typeIDArr[tmpj] = uniquetypeID[currentList[joff + tmpj]];
          }

#pragma omp simd reduction(+ : fxacc, fyacc, fzacc) safelen(vecsize)
          for (size_t j = 0; j < vecsize; j++) {
            const double drx = xtmp[j] - xArr[j];
            const double dry = ytmp[j] - yArr[j];
            const double drz = ztmp[j] - zArr[j];

            const double drx2 = drx * drx;
            const double dry2 = dry * dry;
            const double drz2 = drz * drz;

            const double dr2 = drx2 + dry2 + drz2;

            if (dr2 > r_cutoff_square) continue;

            // apply mixing rules
            const double mixed_charges = chargetmp[j] * chargeArr[j];

            const double dr = sqrt(dr2);

            const double factor = -CoulombConstant * mixed_charges / (dr2 * dr);

            const double fx = drx * factor;
            const double fy = dry * factor;
            const double fz = drz * factor;

            fxacc -= fx;
            fyacc -= fy;
            fzacc -= fz;

            fxArr[j] = fx;
            fyArr[j] = fy;
            fzArr[j] = fz;
          }

#pragma omp simd safelen(vecsize)
          for (size_t tmpj = 0; tmpj < vecsize; tmpj++) {
            const size_t j = currentList[joff + tmpj];
            forceX[j] += fxArr[tmpj];
            forceY[j] += fyArr[tmpj];
            forceZ[j] += fzArr[tmpj];
          }
        }
      }

      for (size_t jNeighIndex = joff; jNeighIndex < listSizeI; ++jNeighIndex) {
        size_t j = neighbourList[i][jNeighIndex];
        if (i == j) continue;

        const double drx = posX[i] - posX[j];
        const double dry = posY[i] - posY[j];
        const double drz = posZ[i] - posZ[j];

        const double drx2 = drx * drx;
        const double dry2 = dry * dry;
        const double drz2 = drz * drz;

        const double dr2 = drx2 + dry2 + drz2;

        if (dr2 > r_cutoff_square) continue;

        // apply mixing rules
        const double mixed_charges = charge[i] * charge[j];

        const double dr = sqrt(dr2);

        const double factor = -CoulombConstant * mixed_charges / (dr2 * dr);

        const double fx = drx * factor;
        const double fy = dry * factor;
        const double fz = drz * factor;

        fxacc -= fx;
        fyacc -= fy;
        fzacc -= fz;

        forceX[j] += fx;
        forceY[j] += fy;
        forceZ[j] += fz;
      }

      forceX[i] += fxacc;
      forceY[i] += fyacc;
      forceZ[i] += fzacc;
    }
  }

  /**TODO
   * @brief Copies the AoS data of the given cell in the given soa.
   *
   * @param cell Cell from where the data is loaded.
   * @param soa  Structure of arrays where the data is copied to.
   */
  AUTOPAS_FUNCTOR_SOALOADER(
      cell, _soa, offset,
      // count total number of sites in the cell
      unsigned count = 0;
      for (auto it = cell.begin(); it.isValid(); ++it) { count += it->getNumPart(); } _soa.resizeArrays(offset + count);

      if (cell.numParticles() == 0) return;

      unsigned long *const __restrict__ id = _soa.template begin<MoleculeMS::id>();

      double *const __restrict__ posX = _soa.template begin<MoleculeMS::posX>();
      double *const __restrict__ posY = _soa.template begin<MoleculeMS::posY>();
      double *const __restrict__ posZ = _soa.template begin<MoleculeMS::posZ>();

      double *const __restrict__ forceX = _soa.template begin<MoleculeMS::forceX>();
      double *const __restrict__ forceY = _soa.template begin<MoleculeMS::forceY>();
      double *const __restrict__ forceZ = _soa.template begin<MoleculeMS::forceZ>();

      int *const __restrict__ uniquetypeID = _soa.template begin<MoleculeMS::uniquetypeID>();

      double *const __restrict__ charge = _soa.template begin<MoleculeMS::charge>();

      size_t i = offset;

      for (auto it = cell.begin(); it.isValid(); ++it) {
        Particle &p = *it;

        const std::vector<double> x = p.getRealPosX();
        const std::vector<double> y = p.getRealPosY();
        const std::vector<double> z = p.getRealPosZ();

        for (int j = 0; j < p.getNumPart(); ++j) {
          id[i] = p.getID();
          posX[i] = x[j];
          posY[i] = y[j];
          posZ[i] = z[j];
          forceX[i] = p.getF()[0];
          forceY[i] = p.getF()[1];
          forceZ[i] = p.getF()[2];
          uniquetypeID[i] = p.getTypeIDPart()[j];
          charge[i] = p.getChargePart()[j];

          ++i;
        }
      })

  /**
   * @brief Copies the data stored in the soa in the cell.
   *
   * @param cell Cell where the data should be stored.
   * @param soa  Structure of arrays from where the data is loaded.
   */
  AUTOPAS_FUNCTOR_SOAEXTRACTOR(cell, _soa, offset, if (_soa.getNumParticles() == 0) return;

                               //#ifndef NDEBUG
                               //		  unsigned long *const __restrict__ id = soa.template
                               // begin<MoleculeMS::id>(); #endif

                               double *const __restrict__ forceX = _soa.template begin<MoleculeMS::forceX>();
                               double *const __restrict__ forceY = _soa.template begin<MoleculeMS::forceY>();
                               double *const __restrict__ forceZ = _soa.template begin<MoleculeMS::forceZ>();

                               double *const __restrict__ posX = _soa.template begin<MoleculeMS::posX>();
                               double *const __restrict__ posY = _soa.template begin<MoleculeMS::posY>();
                               double *const __restrict__ posZ = _soa.template begin<MoleculeMS::posZ>();

                               auto it = cell.begin();

                               size_t i = offset; for (auto it = cell.begin(); it.isValid(); ++it) {
                                 auto R = it->getR();
                                 std::array<double, 3> fadd = {0, 0, 0};
                                 std::array<double, 3> torque = {0, 0, 0};

                                 for (int j = 0; j < it->getNumPart(); ++j) {
                                   //				  assert(id[i] == it->getID());

                                   // add forces
                                   fadd[0] += forceX[i];
                                   fadd[1] += forceY[i];
                                   fadd[2] += forceZ[i];

                                   // compute torques
                                   ArrayMath am;
                                   std::array<double, 3> siteR = {posX[i], posY[i], posZ[i]};
                                   std::array<double, 3> siteF = {forceX[i], forceY[i], forceZ[i]};
                                   torque = am.add(torque, crossProduct(am.sub(siteR, R), siteF));

                                   ++i;
                                 }
                                 it->getF() = fadd;
                                 it->getA() = torque;
                               })

 private:
  double r_cutoff;
  double r_cutoff_square;
};

#endif /* SRC_CoulombFunctor_H_ */
