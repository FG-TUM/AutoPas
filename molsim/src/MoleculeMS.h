/*
 * MoleculeMS.h
 *
 *  Created on: 25.05.2018
 *      Author: raffi
 */

#ifndef MOLECULEMS_H_
#define MOLECULEMS_H_

#include <array>
#include <list>
#include "MoleculeType.h"
#include "ParticleType.h"
#include "autopas/autopasIncludes.h"

using namespace autopas;

class MoleculeMS : public Particle {
 private:
  ArrayMath am;

//  /** position r of the center of mass*/
//  std::array<double, 3> _r;
//
//  /** velocity v of the center of mass */
//  std::array<double, 3> _v;
//
//  /** force f acting on the center of mass */
//  std::array<double, 3> _f;

  /** orientation of the molecule (representation as quaternions: q[0] + q[1]*i + q[2]*j + q[3]*k) */
  std::array<double, 4> _o;

  /** angular velocity (the vector is along the axis of rotation, it's absolute value gives the angular velocity in
   * degree/s) */
  std::array<double, 3> _w;

  /** angular momentum */
  std::array<double, 3> _a;

//  /** molecule id for SoA	 */
//  long unsigned _id;

  /** the force which was effective on the center of mass during the last iteration */
  std::array<double, 3> old_f;

  /** old angular momentum */
  std::array<double, 3> old_a;

  /** Molecule properties: class that stores any information such as particle positions, masses etc */
  MoleculeType* MolProp;

  /** type of the molecule. Use it for whatever you want (e.g. to seperate
   * molecules belonging to different bodies, matters, and so on)
   */

  int type;

  /**
   * unique type ID, is assigned by the algorithm
   */
  int type_id;

  /** list of pointers to all neighbouring particles */
  std::array<MoleculeMS*, 4> direct_neighbours;
  std::array<MoleculeMS*, 4> diagonal_neighbours;

 public:
  MoleculeMS(int type = 0, long unsigned ParticleID = 0, MoleculeType* properties = nullptr);

  MoleculeMS(const MoleculeMS& other);

  MoleculeMS(
      // for visualization, we need always 3 coordinates
      std::array<double, 3> x_arg, std::array<double, 3> v_arg, int type = 0, long unsigned ParticleID = 0,
      MoleculeType* properties = nullptr);

  virtual ~MoleculeMS();

  // rotate the given vector by the quaternion
  std::array<double, 3> rotate(std::array<double, 3> vec);

  // make the invert rotation associated to the quaternion
  std::array<double, 3> InvertRotate(std::array<double, 3> vec);

  std::array<double, 3>& getR();

  std::array<double, 3>& getV();

  std::array<double, 3>& getF();

  std::array<double, 4>& getO();

  std::array<double, 3>& getW();

  std::array<double, 3>& getA();

//  std::array<double, 3> setR(std::array<double, 3>& r);
//
//  std::array<double, 3> setV(std::array<double, 3>& v);
//
//  std::array<double, 3> setF(std::array<double, 3>& f);

  std::array<double, 4> setO(std::array<double, 4>& o);

  std::array<double, 3> setW(std::array<double, 3>& a);

  std::array<double, 3> setA(std::array<double, 3>& w);

  std::array<double, 3>& getOldF();

  std::array<double, 3>& getOldA();

  std::vector<double> getRealPosX();

  std::vector<double> getRealPosY();

  std::vector<double> getRealPosZ();

//  void addF(const std::array<double, 3>& f);
//
//  void subF(const std::array<double, 3>& f);

//  long unsigned getID();

  MoleculeType* getProperties();

  void setProperties(MoleculeType* props);

  double getM();
  std::array<double, 6> getI();
  std::array<double, 6> getInvI();
  double getHpart();
  double getSigma();
  double getCharge();
  double getRtruncLJ();
  bool getFixed();

  std::vector<double> getMpart();
  std::vector<double> getHpartPart();
  std::vector<double> getSigmaPart();
  std::vector<double> getChargePart();
  std::vector<double> getRtruncLJPart();
  std::vector<int> getTypePart();
  std::vector<int> getTypeIDPart();
  bool getFixedPart();

  std::array<double, 3> getDipole();
  int getType();

  int& getTypeID();

  int getNumPart();

  void actualiseNeighbours(MoleculeMS* newptr);

  bool operator==(MoleculeMS& other);

  std::string toString() override;

//  bool inBox(const std::array<double, 3> rmin, const std::array<double, 3> rmax);

  std::array<MoleculeMS*, 4>& getDirectNeighbours();

  std::array<MoleculeMS*, 4>& getDiagonalNeighbours();

  enum AttributeNamesMolecule : int {
    id,
    posX,
    posY,
    posZ,
    forceX,
    forceY,
    forceZ,
    properties,
    uniquetypeID,
    sigma,
    epsilon,
    charge
  };

  typedef autopas::utils::SoAType<size_t, double, double, double, double, double, double, MoleculeType*, int, double,
                                  double, double>::Type SoAArraysType;
};

std::ostream& operator<<(std::ostream& stream, MoleculeMS& p);

inline std::array<double, 3>& MoleculeMS::getR() { return _r; }
inline std::array<double, 3>& MoleculeMS::getV() { return _v; }
inline std::array<double, 3>& MoleculeMS::getF() { return _f; }
inline std::array<double, 4>& MoleculeMS::getO() { return _o; }
inline std::array<double, 3>& MoleculeMS::getW() { return _w; }
inline std::array<double, 3>& MoleculeMS::getA() { return _a; }

inline MoleculeType* MoleculeMS::getProperties() { return MolProp; }

inline void MoleculeMS::setProperties(MoleculeType* props) { MolProp = props; }
//inline std::array<double, 3> MoleculeMS::setR(std::array<double, 3>& r) {
//  _r = r;
//  return r;
//}
//inline std::array<double, 3> MoleculeMS::setV(std::array<double, 3>& v) {
//  _v = v;
//  return v;
//}
//inline std::array<double, 3> MoleculeMS::setF(std::array<double, 3>& f) {
//  _f = f;
//  return f;
//}
inline std::array<double, 4> MoleculeMS::setO(std::array<double, 4>& o) {
  _o = o;
  return o;
}
inline std::array<double, 3> MoleculeMS::setW(std::array<double, 3>& w) {
  _w = w;
  return w;
}
inline std::array<double, 3> MoleculeMS::setA(std::array<double, 3>& a) {
  _a = a;
  return a;
}

//inline void MoleculeMS::addF(const std::array<double, 3>& f) { _f = am.add(_f, f); }
//
//inline void MoleculeMS::subF(const std::array<double, 3>& f) { _f = am.sub(_f, f); }

inline std::array<double, 3>& MoleculeMS::getOldF() { return old_f; }
inline std::array<double, 3>& MoleculeMS::getOldA() { return old_a; }

inline std::vector<double> MoleculeMS::getHpartPart() {
  if (MolProp != nullptr) {
    return MolProp->getHpart();
  } else {
    return {ParticleType::getHpart(type_id)};
  }
}
inline std::array<double, 6> MoleculeMS::getI() {
  if (MolProp != nullptr) {
    return MolProp->getI();
  } else {
    return {0, 0, 0, 0, 0, 0};
  }
}
inline std::array<double, 6> MoleculeMS::getInvI() {
  if (MolProp != nullptr) {
    return MolProp->getInvI();
  } else {
    return {0, 0, 0, 0, 0, 0};
  }
}
inline std::vector<double> MoleculeMS::getMpart() {
  if (MolProp != nullptr) {
    return MolProp->getMasspart();
  } else {
    return {ParticleType::getM(type_id)};
  }
}
inline std::vector<double> MoleculeMS::getSigmaPart() {
  if (MolProp != nullptr) {
    return MolProp->getSigmapart();
  } else {
    return {ParticleType::getSigma(type_id)};
  }
}
inline std::vector<double> MoleculeMS::getChargePart() {
  if (MolProp != nullptr) {
    return MolProp->getChargepart();
  } else {
    return {ParticleType::getCharge(type_id)};
  }
}

inline std::vector<double> MoleculeMS::getRtruncLJPart() {
  if (MolProp != nullptr) {
    return MolProp->getRtruncLJpart();
  } else {
    return {ParticleType::getRtruncLJ(type_id)};
  }
}
inline std::vector<int> MoleculeMS::getTypePart() {
  if (MolProp != nullptr) {
    return MolProp->getTypes();
  } else {
    return {type};
  }
}
inline std::vector<int> MoleculeMS::getTypeIDPart() {
  if (MolProp != nullptr) {
    return MolProp->getTypesID();
  } else {
    return {type_id};
  }
}

inline bool MoleculeMS::getFixedPart() {
  if (MolProp != nullptr) {
    return MolProp->getFixed();
  } else {
    return {ParticleType::getFixed(type_id)};
  }
}
inline double MoleculeMS::getM() { return ParticleType::getM(type_id); }
inline double MoleculeMS::getHpart() { return ParticleType::getHpart(type_id); }
inline double MoleculeMS::getSigma() { return ParticleType::getSigma(type_id); }
inline double MoleculeMS::getCharge() { return ParticleType::getCharge(type_id); }
inline double MoleculeMS::getRtruncLJ() { return ParticleType::getRtruncLJ(type_id); }
inline bool MoleculeMS::getFixed() { return ParticleType::getFixed(type_id); }

//inline long unsigned MoleculeMS::getID() { return _id; }

inline int& MoleculeMS::getTypeID() { return type_id; }

inline std::array<MoleculeMS*, 4>& MoleculeMS::getDirectNeighbours() { return direct_neighbours; }

inline std::array<MoleculeMS*, 4>& MoleculeMS::getDiagonalNeighbours() { return diagonal_neighbours; }

inline int MoleculeMS::getNumPart() {
  if (MolProp != nullptr) {
    return MolProp->getNumPart();
  } else {
    return 1;
  }
}
inline int MoleculeMS::getType() { return type; }

//inline bool MoleculeMS::inBox(const std::array<double, 3> rmin, const std::array<double, 3> rmax) {
//  bool in = true;
//  for (int d = 0; d < 3; ++d) {
//    in &= (_r[d] >= rmin[d] and _r[d] < rmax[d]);
//  }
//  return in;
//}

#endif /* MoleculeMS_H_ */
