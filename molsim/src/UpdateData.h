/*
 * UpdateData.h
 *
 *  Created on: Jun 5, 2018
 *      Author: raffi
 */

#ifndef UPDATEDATA_H_
#define UPDATEDATA_H_

#include "HelperFunctions.h"
#include "MolSimFunctors.h"
#include "MoleculeMS.h"
#include "autopas/AutoPas.h"
#include "autopas/autopasIncludes.h"

using namespace std;
using namespace autopas;

template <class Particle>
void calculateR(Particle* p, array<int, 6> BoundCond, array<double, 3> lowerCorner, array<double, 3> upperCorner,
                double delta_t) {
  std::array<double, 3> x = p->getR();

  // calculate the position in the next step
  std::array<double, 3> new_x =
      am.add(x, am.add(am.mulScalar(p->getV(), delta_t), am.mulScalar(p->getF(), delta_t * delta_t / (2 * p->getM()))));

  assert(!std::isnan(new_x[0]) && !std::isnan(new_x[1]) && !std::isnan(new_x[2]));

  // apply periodic boundary conditions if necessary
  array<double, 3> diagonal = am.sub(upperCorner, lowerCorner);
  // to the left
  if (BoundCond[0] == boundaries::periodic && new_x[0] <= lowerCorner[0]) {
    new_x[0] += diagonal[0];
  }
  // to the upper
  if (BoundCond[1] == boundaries::periodic && new_x[2] >= upperCorner[2]) {
    new_x[2] -= diagonal[2];
  }
  // to the left
  if (BoundCond[2] == boundaries::periodic && new_x[0] >= upperCorner[0]) {
    new_x[0] -= diagonal[0];
  }
  // to the bottom
  if (BoundCond[3] == boundaries::periodic && new_x[2] <= lowerCorner[2]) {
    new_x[2] += diagonal[2];
  }
  // to the front
  if (BoundCond[4] == boundaries::periodic && new_x[1] <= lowerCorner[1]) {
    new_x[1] += diagonal[1];
  }
  // to the back
  if (BoundCond[5] == boundaries::periodic && new_x[1] >= upperCorner[1]) {
    new_x[1] -= diagonal[1];
  }

  // add particle
  p->setR(new_x);
}

template <typename Particle>
void calculateV(Particle* p, double delta_t) {
  std::array<double, 3> v = p->getV();

  // calculate the velocity in the next step
  v = am.add(v, am.mulScalar(am.add(p->getOldF(), p->getF()), delta_t / (2 * p->getM())));

  assert(!std::isnan(v[0]) && !std::isnan(v[1]) && !std::isnan(v[2]));
  p->setV(v);
}

template <typename Particle>
void calculateW(Particle* p, double delta_t) {
  // for all molecules, compute the angular velocity w from the torque A
  if (p->getProperties() != nullptr) {
    std::array<double, 3> w;
    std::array<double, 3> w_add;
    std::array<double, 3> momentum;

    // calculate the additional momentum at this timestep
    // momentum = R⁻¹*(A_new+A_old)*delta_t/2
    momentum = am.mulScalar(p->InvertRotate(am.add(p->getA(), p->getOldA())), delta_t / 2.0);
    AutoPasLogger->trace("orientation: [{6:f}, {0:f}, {1:f}, {2:f}], angular velocity: [{3:f}, {4:f}, {5:f}]",
                         p->getO()[1], p->getO()[2], p->getO()[3], p->getW()[0], p->getW()[1], p->getW()[2],
                         p->getO()[0]);

    // calculate the increment of the angular velocity
    if (p->getProperties()->getEnablePA() == false) {
      std::array<double, 6> InvI = p->getInvI();

      // w_add = I⁻¹*momentum
      w_add[0] = InvI[0] * momentum[0] + InvI[3] * momentum[1] + InvI[5] * momentum[2];
      w_add[1] = InvI[3] * momentum[0] + InvI[1] * momentum[1] + InvI[4] * momentum[2];
      w_add[2] = InvI[5] * momentum[0] + InvI[4] * momentum[1] + InvI[2] * momentum[2];
    } else {
      std::array<double, 3> InvIPA = p->getProperties()->getInvIPA();

      // rotate the momentum into the principal axis frame
      p->getProperties()->RotateBackPA(momentum);
      w_add[0] = InvIPA[0] * momentum[0];
      w_add[1] = InvIPA[1] * momentum[1];
      w_add[2] = InvIPA[2] * momentum[2];
      p->getProperties()->RotatePA(w_add);
    }
    // calculate the angular velocity in the next step
    // new_w = old_w + R*w_add
    w = am.add(p->getW(), p->rotate(w_add));

    assert(!std::isnan(w[0]) && !std::isnan(w[1]) && !std::isnan(w[2]));
    p->setW(w);

    // reset the torque
    p->getOldA() = p->getA();
    std::array<double, 3> zero = {0.0, 0.0, 0.0};
    p->setA(zero);
  }
}

template <typename Particle>
void calculateO(Particle* p, double delta_t) {
  if (p->getProperties() != nullptr) {
    // new quaternion for orientation
    std::array<double, 4> q = p->getO();

    // get the angular velocity as quaternion representation (with zero real part)
    std::array<double, 4> w = {0, p->getW()[0], p->getW()[1], p->getW()[2]};

    // qnhalf = q + delta_t/2*Q*w
    std::array<double, 4> qnhalf = am.add(q, am.mulScalar(Qw(q, w), delta_t * 0.5));

    std::array<double, 4> new_q = am.add(q, am.mulScalar(Qw(qnhalf, w), delta_t));

    // normalize the quaternion to get a versor
    std::array<double, 4> n_q = am.mulScalar(new_q, 1 / sqrt(am.dot(new_q, new_q)));
    p->setO(n_q);
  }
}

template <typename Container>
void resetF(Container* cont) {
  // for all Particles
  std::vector<MoleculeMS*> particlelistall;
  for (auto p = cont->begin(); p.isValid(); ++p) {
    particlelistall.push_back(&*p);
  }
#ifdef AUTOPAS_OPENMP
#pragma omp parallel for schedule(static)
#endif
  for (size_t i = 0; i < particlelistall.size(); ++i) {
    MoleculeMS* p = particlelistall[i];

    // save current force to old force
    p->getOldF() = p->getF();

    // reset forces to zero
    std::array<double, 3> zero = {0, 0, 0};
    p->setF(zero);
  }
}

template <class Container, class Particle>
void testEndaddForce(Container* cont, double time, ConstantFunctor<Particle>* fconst) {
  if (fconst->getForceStatus() && (fconst->getEndTime() > time)) fconst->setForceStatus(false);
}

#endif /* UPDATEDATA_H_ */
