#ifndef HELPERFUNCTIONS_H_
#define HELPERFUNCTIONS_H_

#include <cmath>
#include "MoleculeMS.h"
#include "autopas/AutoPas.h"
#include "autopas/autopasIncludes.h"
#ifdef AUTOPAS_OPENMP
#include <omp.h>
#endif

using namespace std;
using namespace autopas;

const ArrayMath am;

// define the boundary conditions
enum boundaries { outflow = 0, reflective = 1, periodic = 2 };
const double HalfZeroForceFactor = 0.561231;
const double ZeroForceFactor = 1.122462048309;
const double pi = 3.14159265;

inline std::array<double, 4> Qw(std::array<double, 4> q, std::array<double, 4> w) {
  // computes the derivative of a quaternion q with respect to an angular velocity w (dq/dt = Q*w, where Q is a 4*4
  // matrix)
  double ret0 = q[0] * w[0] - q[1] * w[1] - q[2] * w[2] - q[3] * w[3];
  double ret1 = q[1] * w[0] + q[0] * w[1] - q[3] * w[2] + q[2] * w[3];
  double ret2 = q[2] * w[0] + q[3] * w[1] + q[0] * w[2] - q[1] * w[3];
  double ret3 = q[3] * w[0] - q[2] * w[1] + q[1] * w[2] + q[0] * w[3];

  return {ret0, ret1, ret2, ret3};
}

template <class Container, class Functor>
void applyReflectiveBC(Container* cont, Functor* func, array<int, 6> BoundCond, array<double, 3> lowerCorner,
                       array<double, 3> upperCorner, double Rcut) {
  // add directly the halo force from the boundary
  // check to the left
  if (BoundCond[0] == boundaries::reflective) {
    array<double, 3> RegionLeftCorner = upperCorner;
    RegionLeftCorner[0] = lowerCorner[0] + Rcut;

    // iterate over all particles in the left region
    for (auto p = cont->getRegionIterator(lowerCorner, RegionLeftCorner); p.isValid(); ++p) {
      if (p->getR()[0] < lowerCorner[0] + HalfZeroForceFactor * p->getSigma()) {
        MoleculeMS HaloParticle(*p);
        HaloParticle.getR()[0] = 2 * lowerCorner[0] - p->getR()[0];
        func->AoSFunctor(HaloParticle, *p);
      }
    }
  }

  // check to the upper
  if (BoundCond[1] == boundaries::reflective) {
    array<double, 3> RegionUpperCorner = lowerCorner;
    RegionUpperCorner[2] = upperCorner[2] - Rcut;

    // iterate over all particles in the upper region
    for (auto p = cont->getRegionIterator(RegionUpperCorner, upperCorner); p.isValid(); ++p) {
      if (p->getR()[2] > upperCorner[2] - HalfZeroForceFactor * p->getSigma()) {
        MoleculeMS HaloParticle(*p);
        HaloParticle.getR()[2] = 2 * upperCorner[2] - p->getR()[2];
        func->AoSFunctor(HaloParticle, *p);
      }
    }
  }

  // check to the right
  if (BoundCond[2] == 1) {
    array<double, 3> RegionRightCorner = lowerCorner;
    RegionRightCorner[0] = upperCorner[0] - Rcut;

    // iterate over all particles in the right region
    for (auto p = cont->getRegionIterator(RegionRightCorner, upperCorner); p.isValid(); ++p) {
      if (p->getR()[0] > upperCorner[0] - HalfZeroForceFactor * p->getSigma()) {
        MoleculeMS HaloParticle(*p);
        HaloParticle.getR()[0] = 2 * upperCorner[0] - p->getR()[0];
        func->AoSFunctor(HaloParticle, *p);
      }
    }
  }

  // check to the bottom
  if (BoundCond[3] == boundaries::reflective) {
    array<double, 3> RegionLowerCorner = upperCorner;
    RegionLowerCorner[2] = lowerCorner[2] + Rcut;

    // iterate over all particles in the lower region
    for (auto p = cont->getRegionIterator(lowerCorner, RegionLowerCorner); p.isValid(); ++p) {
      if (p->getR()[2] < lowerCorner[2] + HalfZeroForceFactor * p->getSigma()) {
        MoleculeMS HaloParticle(*p);
        HaloParticle.getR()[2] = 2 * lowerCorner[2] - p->getR()[2];
        func->AoSFunctor(HaloParticle, *p);
      }
    }
  }

  // check to the front
  if (BoundCond[4] == 1) {
    array<double, 3> RegionFrontCorner = upperCorner;
    RegionFrontCorner[1] = lowerCorner[1] + Rcut;

    // iterate over all particles in the front region
    for (auto p = cont->getRegionIterator(lowerCorner, RegionFrontCorner); p.isValid(); ++p) {
      if (p->getR()[1] < lowerCorner[1] + HalfZeroForceFactor * p->getSigma()) {
        MoleculeMS HaloParticle(*p);
        HaloParticle.getR()[1] = 2 * lowerCorner[1] - p->getR()[1];
        func->AoSFunctor(HaloParticle, *p);
      }
    }
  }

  // check to the back
  if (BoundCond[5] == 1) {
    array<double, 3> RegionBackCorner = lowerCorner;
    RegionBackCorner[1] = upperCorner[1] - Rcut;

    // iterate over all particles in the left region
    for (auto p = cont->getRegionIterator(RegionBackCorner, upperCorner); p.isValid(); ++p) {
      if (p->getR()[1] > upperCorner[1] - HalfZeroForceFactor * p->getSigma()) {
        MoleculeMS HaloParticle(*p);
        HaloParticle.getR()[1] = 2 * upperCorner[1] - p->getR()[1];
        func->AoSFunctor(HaloParticle, *p);
      }
    }
  }
}

template <class Container, class Functor>
void computeNeighboursPeriodicBC(Container* cont, Functor* func, array<int, 6> BoundCond, array<double, 3> lowerCorner,
                                 array<double, 3> upperCorner, double Rcut) {
  // computes directly the forces between the particles at the boundary, without creating any halo particles
  // pros: efficient if there are not many particles at the boundary
  // cons: unused Linkedcells algorithm -> n² complexity
  // cons: unreadable code (each edge and corner has to be treated independently)

  // compute the distance vector between both corner
  array<double, 3> diagonal = am.sub(upperCorner, lowerCorner);

  // check along X-axis
  if (BoundCond[0] == boundaries::periodic) {
    // get particle from the left
    array<double, 3> LeftRegionCorner = upperCorner;
    LeftRegionCorner[0] = lowerCorner[0] + Rcut;

    // iterate over all particles on the border along the X-axis
    for (auto p1 = cont->getRegionIterator(lowerCorner, LeftRegionCorner); p1.isValid(); ++p1) {
      p1->getR()[0] += diagonal[0];

      // only iterate through the part of the domain Rcut around p1
      array<double, 3> Rp1 = p1->getR();
      array<double, 3> OppositeLowerCorner = {Rp1[0] - Rcut, Rp1[1] - Rcut, Rp1[2] - Rcut};
      array<double, 3> OppositeUpperCorner = {upperCorner[0], Rp1[1] + Rcut, Rp1[2] + Rcut};

      for (auto p2 = cont->getRegionIterator(OppositeLowerCorner, OppositeUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] -= diagonal[0];
    }
  }
  // check along Y-axis
  if (BoundCond[4] == boundaries::periodic) {
    // get particle from the left
    array<double, 3> LowerRegionCorner = upperCorner;
    LowerRegionCorner[1] = lowerCorner[1] + Rcut;

    // iterate over all particles on the border along the Y-axis
    for (auto p1 = cont->getRegionIterator(lowerCorner, LowerRegionCorner); p1.isValid(); ++p1) {
      p1->getR()[1] += diagonal[1];

      // only iterate through the part of the domain Rcut around p1
      array<double, 3> Rp1 = p1->getR();
      array<double, 3> OppositeLowerCorner = {Rp1[0] - Rcut, Rp1[1] - Rcut, Rp1[2] - Rcut};
      array<double, 3> OppositeUpperCorner = {Rp1[0] + Rcut, upperCorner[1], Rp1[2] + Rcut};

      for (auto p2 = cont->getRegionIterator(OppositeLowerCorner, OppositeUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[1] -= diagonal[1];
    }
  }

  // check along Z-axis
  if (BoundCond[1] == boundaries::periodic) {
    // get particle from the left
    array<double, 3> FrontRegionCorner = upperCorner;
    FrontRegionCorner[2] = lowerCorner[2] + Rcut;

    // iterate over all particles on the border along the Z-axis
    for (auto p1 = cont->getRegionIterator(lowerCorner, FrontRegionCorner); p1.isValid(); ++p1) {
      p1->getR()[2] += diagonal[2];

      // only iterate through the part of the domain Rcut around p1
      array<double, 3> Rp1 = p1->getR();
      array<double, 3> OppositeLowerCorner = {Rp1[0] - Rcut, Rp1[1] - Rcut, Rp1[2] - Rcut};
      array<double, 3> OppositeUpperCorner = {Rp1[0] + Rcut, Rp1[1] + Rcut, upperCorner[2]};

      for (auto p2 = cont->getRegionIterator(OppositeLowerCorner, OppositeUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[2] -= diagonal[2];
    }
  }

  // check for X/Y - edges
  if ((BoundCond[0] == boundaries::periodic) && (BoundCond[4] == boundaries::periodic)) {
    array<double, 3> FrontRegionCorner = lowerCorner;
    FrontRegionCorner[0] += Rcut;
    FrontRegionCorner[1] += Rcut;
    FrontRegionCorner[2] = upperCorner[2];

    for (auto p1 = cont->getRegionIterator(lowerCorner, FrontRegionCorner); p1.isValid(); ++p1) {
      p1->getR()[0] += diagonal[0];
      p1->getR()[1] += diagonal[1];

      // only iterate through the part of the domain Rcut around p1
      array<double, 3> Rp1 = p1->getR();
      array<double, 3> OppositeLowerCorner = {Rp1[0] - Rcut, Rp1[1] - Rcut, Rp1[2] - Rcut};
      array<double, 3> OppositeUpperCorner = {upperCorner[0], upperCorner[1], Rp1[2] + Rcut};

      for (auto p2 = cont->getRegionIterator(OppositeLowerCorner, OppositeUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] -= diagonal[0];
      p1->getR()[1] -= diagonal[1];
    }

    array<double, 3> FrontRegionLowerCorner = lowerCorner;
    FrontRegionLowerCorner[0] = upperCorner[0] - Rcut;
    array<double, 3> FrontRegionUpperCorner = upperCorner;
    FrontRegionUpperCorner[1] = lowerCorner[1] + Rcut;

    for (auto p1 = cont->getRegionIterator(FrontRegionLowerCorner, FrontRegionUpperCorner); p1.isValid(); ++p1) {
      p1->getR()[0] -= diagonal[0];
      p1->getR()[1] += diagonal[1];

      // only iterate through the part of the domain Rcut around p1
      array<double, 3> Rp1 = p1->getR();
      array<double, 3> OppositeLowerCorner = {lowerCorner[0], Rp1[1] - Rcut, Rp1[2] - Rcut};
      array<double, 3> OppositeUpperCorner = {Rp1[0] + Rcut, upperCorner[1], Rp1[2] + Rcut};

      for (auto p2 = cont->getRegionIterator(OppositeLowerCorner, OppositeUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] += diagonal[0];
      p1->getR()[1] -= diagonal[1];
    }
  }

  // check for X/Z - edges
  if ((BoundCond[0] == boundaries::periodic) && (BoundCond[1] == boundaries::periodic)) {
    array<double, 3> FrontRegionCorner = lowerCorner;
    FrontRegionCorner[0] += Rcut;
    FrontRegionCorner[1] = upperCorner[1];
    FrontRegionCorner[2] += Rcut;

    for (auto p1 = cont->getRegionIterator(lowerCorner, FrontRegionCorner); p1.isValid(); ++p1) {
      p1->getR()[0] += diagonal[0];
      p1->getR()[2] += diagonal[2];

      // only iterate through the part of the domain Rcut around p1
      array<double, 3> Rp1 = p1->getR();
      array<double, 3> OppositeLowerCorner = {Rp1[0] - Rcut, Rp1[1] - Rcut, Rp1[2] - Rcut};
      array<double, 3> OppositeUpperCorner = {upperCorner[0], Rp1[1] + Rcut, upperCorner[2]};

      for (auto p2 = cont->getRegionIterator(OppositeLowerCorner, OppositeUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] -= diagonal[0];
      p1->getR()[2] -= diagonal[2];
    }

    array<double, 3> FrontRegionLowerCorner = lowerCorner;
    FrontRegionLowerCorner[0] = upperCorner[0] - Rcut;
    array<double, 3> FrontRegionUpperCorner = upperCorner;
    FrontRegionUpperCorner[2] = lowerCorner[2] + Rcut;

    for (auto p1 = cont->getRegionIterator(FrontRegionLowerCorner, FrontRegionUpperCorner); p1.isValid(); ++p1) {
      p1->getR()[0] -= diagonal[0];
      p1->getR()[2] += diagonal[2];

      // only iterate through the part of the domain Rcut around p1
      array<double, 3> Rp1 = p1->getR();
      array<double, 3> OppositeLowerCorner = {lowerCorner[0], Rp1[1] - Rcut, Rp1[2] - Rcut};
      array<double, 3> OppositeUpperCorner = {Rp1[0] + Rcut, Rp1[1] + Rcut, upperCorner[2]};

      for (auto p2 = cont->getRegionIterator(OppositeLowerCorner, OppositeUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] += diagonal[0];
      p1->getR()[2] -= diagonal[2];
    }
  }

  // check for Y/Z - edges
  if ((BoundCond[1] == boundaries::periodic) && (BoundCond[4] == boundaries::periodic)) {
    array<double, 3> FrontRegionCorner = lowerCorner;
    FrontRegionCorner[0] = upperCorner[0];
    FrontRegionCorner[1] += Rcut;
    FrontRegionCorner[2] += Rcut;

    for (auto p1 = cont->getRegionIterator(lowerCorner, FrontRegionCorner); p1.isValid(); ++p1) {
      p1->getR()[1] += diagonal[1];
      p1->getR()[2] += diagonal[2];

      // only iterate through the part of the domain Rcut around p1
      array<double, 3> Rp1 = p1->getR();
      array<double, 3> OppositeLowerCorner = {Rp1[0] - Rcut, Rp1[1] - Rcut, Rp1[2] - Rcut};
      array<double, 3> OppositeUpperCorner = {Rp1[0] + Rcut, upperCorner[1], upperCorner[2]};

      for (auto p2 = cont->getRegionIterator(OppositeLowerCorner, OppositeUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[1] -= diagonal[1];
      p1->getR()[2] -= diagonal[2];
    }

    array<double, 3> FrontRegionLowerCorner = lowerCorner;
    FrontRegionLowerCorner[1] = upperCorner[1] - Rcut;
    array<double, 3> FrontRegionUpperCorner = upperCorner;
    FrontRegionUpperCorner[2] = lowerCorner[2] + Rcut;

    for (auto p1 = cont->getRegionIterator(FrontRegionLowerCorner, FrontRegionUpperCorner); p1.isValid(); ++p1) {
      p1->getR()[1] -= diagonal[1];
      p1->getR()[2] += diagonal[2];

      // only iterate through the part of the domain Rcut around p1
      array<double, 3> Rp1 = p1->getR();
      array<double, 3> OppositeLowerCorner = {Rp1[0] - Rcut, lowerCorner[1], Rp1[2] - Rcut};
      array<double, 3> OppositeUpperCorner = {Rp1[0] + Rcut, Rp1[1] + Rcut, upperCorner[2]};

      for (auto p2 = cont->getRegionIterator(OppositeLowerCorner, OppositeUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[1] += diagonal[1];
      p1->getR()[2] -= diagonal[2];
    }
  }

  // check: XYZ are all PBC (corners
  if ((BoundCond[0] == boundaries::periodic) && (BoundCond[1] == boundaries::periodic) &&
      (BoundCond[4] == boundaries::periodic)) {
    array<double, 3> RegionFrontCorner = lowerCorner;
    RegionFrontCorner[0] += Rcut;
    RegionFrontCorner[1] += Rcut;
    RegionFrontCorner[2] += Rcut;

    array<double, 3> RegionBackCorner = upperCorner;
    RegionBackCorner[0] -= Rcut;
    RegionBackCorner[1] -= Rcut;
    RegionBackCorner[2] -= Rcut;

    for (auto p1 = cont->getRegionIterator(lowerCorner, RegionFrontCorner); p1.isValid(); ++p1) {
      p1->getR()[0] += diagonal[0];
      p1->getR()[1] += diagonal[1];
      p1->getR()[2] += diagonal[2];
      for (auto p2 = cont->getRegionIterator(RegionBackCorner, upperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] -= diagonal[0];
      p1->getR()[1] -= diagonal[1];
      p1->getR()[2] -= diagonal[2];
    }

    array<double, 3> RegionFrontLowerCorner = lowerCorner;
    RegionFrontLowerCorner[0] = upperCorner[0] - Rcut;
    RegionFrontLowerCorner[2] = upperCorner[2] - Rcut;
    array<double, 3> RegionFrontUpperCorner = upperCorner;
    RegionFrontUpperCorner[1] = lowerCorner[1] + Rcut;

    array<double, 3> RegionBackLowerCorner = lowerCorner;
    RegionBackLowerCorner[1] = upperCorner[1] - Rcut;
    array<double, 3> RegionBackUpperCorner = upperCorner;
    RegionBackUpperCorner[0] = lowerCorner[0] + Rcut;
    RegionBackLowerCorner[2] = lowerCorner[2] + Rcut;

    for (auto p1 = cont->getRegionIterator(RegionFrontLowerCorner, RegionFrontUpperCorner); p1.isValid(); ++p1) {
      p1->getR()[0] -= diagonal[0];
      p1->getR()[1] += diagonal[1];
      p1->getR()[2] -= diagonal[2];
      for (auto p2 = cont->getRegionIterator(RegionBackLowerCorner, RegionBackUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] += diagonal[0];
      p1->getR()[1] -= diagonal[1];
      p1->getR()[2] += diagonal[2];
    }

    array<double, 3> RegionLeftLowerCorner = lowerCorner;
    RegionLeftLowerCorner[0] = upperCorner[0] - Rcut;
    array<double, 3> RegionLeftUpperCorner = upperCorner;
    RegionLeftUpperCorner[1] = lowerCorner[1] + Rcut;
    RegionLeftUpperCorner[2] = lowerCorner[2] + Rcut;

    array<double, 3> RegionRightLowerCorner = lowerCorner;
    RegionRightLowerCorner[1] = upperCorner[1] - Rcut;
    RegionRightLowerCorner[2] = upperCorner[2] - Rcut;
    array<double, 3> RegionRightUpperCorner = upperCorner;
    RegionRightUpperCorner[0] = lowerCorner[0] + Rcut;

    for (auto p1 = cont->getRegionIterator(RegionLeftLowerCorner, RegionLeftUpperCorner); p1.isValid(); ++p1) {
      p1->getR()[0] -= diagonal[0];
      p1->getR()[1] += diagonal[1];
      p1->getR()[2] += diagonal[2];
      for (auto p2 = cont->getRegionIterator(RegionRightLowerCorner, RegionRightUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] += diagonal[0];
      p1->getR()[1] -= diagonal[1];
      p1->getR()[2] -= diagonal[2];
    }

    array<double, 3> RegionTopLowerCorner = lowerCorner;
    RegionTopLowerCorner[2] = upperCorner[2] - Rcut;
    array<double, 3> RegionTopUpperCorner = upperCorner;
    RegionTopUpperCorner[0] = lowerCorner[0] + Rcut;
    RegionTopUpperCorner[1] = lowerCorner[1] + Rcut;

    array<double, 3> RegionBottomLowerCorner = lowerCorner;
    RegionBottomLowerCorner[0] = upperCorner[0] - Rcut;
    RegionBottomLowerCorner[1] = upperCorner[1] - Rcut;
    array<double, 3> RegionBottomUpperCorner = upperCorner;
    RegionBottomUpperCorner[2] = lowerCorner[2] + Rcut;

    for (auto p1 = cont->getRegionIterator(RegionTopLowerCorner, RegionTopUpperCorner); p1.isValid(); ++p1) {
      p1->getR()[0] += diagonal[0];
      p1->getR()[1] += diagonal[1];
      p1->getR()[2] -= diagonal[2];
      for (auto p2 = cont->getRegionIterator(RegionBottomLowerCorner, RegionBottomUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] -= diagonal[0];
      p1->getR()[1] -= diagonal[1];
      p1->getR()[2] += diagonal[2];
    }
  }
}

template <class Container, class Functor>
void computePeriodicBC(Container* cont, Functor* func, array<int, 6> BoundCond, array<double, 3> lowerCorner,
                       array<double, 3> upperCorner, double Rcut) {
  // computes directly the forces between the particles at the boundary, without creating any halo particles
  // pros: efficient if there are not many particles at the boundary
  // cons: unused Linkedcells algorithm -> n² complexity
  // cons: unreadable code (each edge and corner has to be treated independently)

  // compute the distance vector between both corner
  array<double, 3> diagonal = am.sub(upperCorner, lowerCorner);

  // check along X-axis
  if (BoundCond[0] == boundaries::periodic) {
    // get particle from the left
    array<double, 3> RegionLeftCorner = upperCorner;
    RegionLeftCorner[0] = lowerCorner[0] + Rcut;

    // get the particles from the right
    array<double, 3> RegionRightCorner = lowerCorner;
    RegionRightCorner[0] = upperCorner[0] - Rcut;

    // iterate over all particles on the border along the X-axis
    for (auto p1 = cont->getRegionIterator(lowerCorner, RegionLeftCorner); p1.isValid(); ++p1) {
      p1->getR()[0] += diagonal[0];
      for (auto p2 = cont->getRegionIterator(RegionRightCorner, upperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] -= diagonal[0];
    }
  }
  // check along Y-axis
  if (BoundCond[4] == boundaries::periodic) {
    // get particle from the left
    array<double, 3> RegionLowerCorner = upperCorner;
    RegionLowerCorner[1] = lowerCorner[1] + Rcut;

    // get the particles from the right
    array<double, 3> RegionUpperCorner = lowerCorner;
    RegionUpperCorner[1] = upperCorner[1] - Rcut;

    // iterate over all particles on the border along the X-axis
    for (auto p1 = cont->getRegionIterator(lowerCorner, RegionLowerCorner); p1.isValid(); ++p1) {
      p1->getR()[1] += diagonal[1];
      for (auto p2 = cont->getRegionIterator(RegionUpperCorner, upperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[1] -= diagonal[1];
    }
  }

  // check along Z-axis
  if (BoundCond[1] == boundaries::periodic) {
    // get particle from the left
    array<double, 3> RegionFrontCorner = upperCorner;
    RegionFrontCorner[2] = lowerCorner[2] + Rcut;

    // get the particles from the right
    array<double, 3> RegionBackCorner = lowerCorner;
    RegionBackCorner[2] = upperCorner[2] - Rcut;

    // iterate over all particles on the border along the X-axis
    for (auto p1 = cont->getRegionIterator(lowerCorner, RegionFrontCorner); p1.isValid(); ++p1) {
      p1->getR()[2] += diagonal[2];
      for (auto p2 = cont->getRegionIterator(RegionBackCorner, upperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[2] -= diagonal[2];
    }
  }

  // check for X/Y - edges
  if ((BoundCond[0] == boundaries::periodic) && (BoundCond[4] == boundaries::periodic)) {
    array<double, 3> RegionFrontCorner = lowerCorner;
    RegionFrontCorner[0] += Rcut;
    RegionFrontCorner[1] += Rcut;
    RegionFrontCorner[2] = upperCorner[2];

    array<double, 3> RegionBackCorner = upperCorner;
    RegionBackCorner[0] -= Rcut;
    RegionBackCorner[1] -= Rcut;
    RegionBackCorner[2] = lowerCorner[2];

    for (auto p1 = cont->getRegionIterator(lowerCorner, RegionFrontCorner); p1.isValid(); ++p1) {
      p1->getR()[0] += diagonal[0];
      p1->getR()[1] += diagonal[1];
      for (auto p2 = cont->getRegionIterator(RegionBackCorner, upperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] -= diagonal[0];
      p1->getR()[1] -= diagonal[1];
    }

    array<double, 3> RegionFrontLowerCorner = lowerCorner;
    RegionFrontLowerCorner[0] = upperCorner[0] - Rcut;
    array<double, 3> RegionFrontUpperCorner = upperCorner;
    RegionFrontUpperCorner[1] = lowerCorner[1] + Rcut;

    array<double, 3> RegionBackLowerCorner = lowerCorner;
    RegionBackLowerCorner[1] = upperCorner[1] - Rcut;
    array<double, 3> RegionBackUpperCorner = upperCorner;
    RegionBackUpperCorner[0] = lowerCorner[0] + Rcut;

    for (auto p1 = cont->getRegionIterator(RegionFrontLowerCorner, RegionFrontUpperCorner); p1.isValid(); ++p1) {
      p1->getR()[0] -= diagonal[0];
      p1->getR()[1] += diagonal[1];
      for (auto p2 = cont->getRegionIterator(RegionBackLowerCorner, RegionBackUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] += diagonal[0];
      p1->getR()[1] -= diagonal[1];
    }
  }

  // check for X/Z - edges
  if ((BoundCond[0] == boundaries::periodic) && (BoundCond[1] == boundaries::periodic)) {
    array<double, 3> RegionFrontCorner = lowerCorner;
    RegionFrontCorner[0] += Rcut;
    RegionFrontCorner[1] = upperCorner[1];
    RegionFrontCorner[2] += Rcut;

    array<double, 3> RegionBackCorner = upperCorner;
    RegionBackCorner[0] -= Rcut;
    RegionBackCorner[1] = lowerCorner[1];
    RegionBackCorner[2] -= Rcut;

    for (auto p1 = cont->getRegionIterator(lowerCorner, RegionFrontCorner); p1.isValid(); ++p1) {
      p1->getR()[0] += diagonal[0];
      p1->getR()[2] += diagonal[2];
      for (auto p2 = cont->getRegionIterator(RegionBackCorner, upperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] -= diagonal[0];
      p1->getR()[2] -= diagonal[2];
    }

    array<double, 3> RegionFrontLowerCorner = lowerCorner;
    RegionFrontLowerCorner[0] = upperCorner[0] - Rcut;
    array<double, 3> RegionFrontUpperCorner = upperCorner;
    RegionFrontUpperCorner[2] = lowerCorner[2] + Rcut;

    array<double, 3> RegionBackLowerCorner = lowerCorner;
    RegionBackLowerCorner[2] = upperCorner[2] - Rcut;
    array<double, 3> RegionBackUpperCorner = upperCorner;
    RegionBackUpperCorner[0] = lowerCorner[0] + Rcut;

    for (auto p1 = cont->getRegionIterator(RegionFrontLowerCorner, RegionFrontUpperCorner); p1.isValid(); ++p1) {
      p1->getR()[0] -= diagonal[0];
      p1->getR()[2] += diagonal[2];
      for (auto p2 = cont->getRegionIterator(RegionBackLowerCorner, RegionBackUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] += diagonal[0];
      p1->getR()[2] -= diagonal[2];
    }
  }

  // check for Y/Z - edges
  if ((BoundCond[1] == boundaries::periodic) && (BoundCond[4] == boundaries::periodic)) {
    array<double, 3> RegionFrontCorner = lowerCorner;
    RegionFrontCorner[0] = upperCorner[0];
    RegionFrontCorner[1] += Rcut;
    RegionFrontCorner[2] += Rcut;

    array<double, 3> RegionBackCorner = upperCorner;
    RegionBackCorner[0] = lowerCorner[0];
    RegionBackCorner[1] -= Rcut;
    RegionBackCorner[2] -= Rcut;

    for (auto p1 = cont->getRegionIterator(lowerCorner, RegionFrontCorner); p1.isValid(); ++p1) {
      p1->getR()[1] += diagonal[1];
      p1->getR()[2] += diagonal[2];
      for (auto p2 = cont->getRegionIterator(RegionBackCorner, upperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[1] -= diagonal[1];
      p1->getR()[2] -= diagonal[2];
    }

    array<double, 3> RegionFrontLowerCorner = lowerCorner;
    RegionFrontLowerCorner[1] = upperCorner[1] - Rcut;
    array<double, 3> RegionFrontUpperCorner = upperCorner;
    RegionFrontUpperCorner[2] = lowerCorner[2] + Rcut;

    array<double, 3> RegionBackLowerCorner = lowerCorner;
    RegionBackLowerCorner[2] = upperCorner[2] - Rcut;
    array<double, 3> RegionBackUpperCorner = upperCorner;
    RegionBackUpperCorner[1] = lowerCorner[1] + Rcut;

    for (auto p1 = cont->getRegionIterator(RegionFrontLowerCorner, RegionFrontUpperCorner); p1.isValid(); ++p1) {
      p1->getR()[1] -= diagonal[1];
      p1->getR()[2] += diagonal[2];
      for (auto p2 = cont->getRegionIterator(RegionBackLowerCorner, RegionBackUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[1] += diagonal[1];
      p1->getR()[2] -= diagonal[2];
    }
  }

  // check: XYZ are all PBC
  if ((BoundCond[0] == boundaries::periodic) && (BoundCond[1] == boundaries::periodic) &&
      (BoundCond[4] == boundaries::periodic)) {
    array<double, 3> RegionFrontCorner = lowerCorner;
    RegionFrontCorner[0] += Rcut;
    RegionFrontCorner[1] += Rcut;
    RegionFrontCorner[2] += Rcut;

    array<double, 3> RegionBackCorner = upperCorner;
    RegionBackCorner[0] -= Rcut;
    RegionBackCorner[1] -= Rcut;
    RegionBackCorner[2] -= Rcut;

    for (auto p1 = cont->getRegionIterator(lowerCorner, RegionFrontCorner); p1.isValid(); ++p1) {
      p1->getR()[0] += diagonal[0];
      p1->getR()[1] += diagonal[1];
      p1->getR()[2] += diagonal[2];
      for (auto p2 = cont->getRegionIterator(RegionBackCorner, upperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] -= diagonal[0];
      p1->getR()[1] -= diagonal[1];
      p1->getR()[2] -= diagonal[2];
    }

    array<double, 3> RegionFrontLowerCorner = lowerCorner;
    RegionFrontLowerCorner[0] = upperCorner[0] - Rcut;
    RegionFrontLowerCorner[2] = upperCorner[2] - Rcut;
    array<double, 3> RegionFrontUpperCorner = upperCorner;
    RegionFrontUpperCorner[1] = lowerCorner[1] + Rcut;

    array<double, 3> RegionBackLowerCorner = lowerCorner;
    RegionBackLowerCorner[1] = upperCorner[1] - Rcut;
    array<double, 3> RegionBackUpperCorner = upperCorner;
    RegionBackUpperCorner[0] = lowerCorner[0] + Rcut;
    RegionBackLowerCorner[2] = lowerCorner[2] + Rcut;

    for (auto p1 = cont->getRegionIterator(RegionFrontLowerCorner, RegionFrontUpperCorner); p1.isValid(); ++p1) {
      p1->getR()[0] -= diagonal[0];
      p1->getR()[1] += diagonal[1];
      p1->getR()[2] -= diagonal[2];
      for (auto p2 = cont->getRegionIterator(RegionBackLowerCorner, RegionBackUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] += diagonal[0];
      p1->getR()[1] -= diagonal[1];
      p1->getR()[2] += diagonal[2];
    }

    array<double, 3> RegionLeftLowerCorner = lowerCorner;
    RegionLeftLowerCorner[0] = upperCorner[0] - Rcut;
    array<double, 3> RegionLeftUpperCorner = upperCorner;
    RegionLeftUpperCorner[1] = lowerCorner[1] + Rcut;
    RegionLeftUpperCorner[2] = lowerCorner[2] + Rcut;

    array<double, 3> RegionRightLowerCorner = lowerCorner;
    RegionRightLowerCorner[1] = upperCorner[1] - Rcut;
    RegionRightLowerCorner[2] = upperCorner[2] - Rcut;
    array<double, 3> RegionRightUpperCorner = upperCorner;
    RegionRightUpperCorner[0] = lowerCorner[0] + Rcut;

    for (auto p1 = cont->getRegionIterator(RegionLeftLowerCorner, RegionLeftUpperCorner); p1.isValid(); ++p1) {
      p1->getR()[0] -= diagonal[0];
      p1->getR()[1] += diagonal[1];
      p1->getR()[2] += diagonal[2];
      for (auto p2 = cont->getRegionIterator(RegionRightLowerCorner, RegionRightUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] += diagonal[0];
      p1->getR()[1] -= diagonal[1];
      p1->getR()[2] -= diagonal[2];
    }

    array<double, 3> RegionTopLowerCorner = lowerCorner;
    RegionTopLowerCorner[2] = upperCorner[2] - Rcut;
    array<double, 3> RegionTopUpperCorner = upperCorner;
    RegionTopUpperCorner[0] = lowerCorner[0] + Rcut;
    RegionTopUpperCorner[1] = lowerCorner[1] + Rcut;

    array<double, 3> RegionBottomLowerCorner = lowerCorner;
    RegionBottomLowerCorner[0] = upperCorner[0] - Rcut;
    RegionBottomLowerCorner[1] = upperCorner[1] - Rcut;
    array<double, 3> RegionBottomUpperCorner = upperCorner;
    RegionBottomUpperCorner[2] = lowerCorner[2] + Rcut;

    for (auto p1 = cont->getRegionIterator(RegionTopLowerCorner, RegionTopUpperCorner); p1.isValid(); ++p1) {
      p1->getR()[0] += diagonal[0];
      p1->getR()[1] += diagonal[1];
      p1->getR()[2] -= diagonal[2];
      for (auto p2 = cont->getRegionIterator(RegionBottomLowerCorner, RegionBottomUpperCorner); p2.isValid(); ++p2) {
        func->AoSFunctor(*p1, *p2, true);
      }
      p1->getR()[0] -= diagonal[0];
      p1->getR()[1] -= diagonal[1];
      p1->getR()[2] += diagonal[2];
    }
  }
}

template <class Container>
void applyPeriodicBC(Container* cont, array<int, 6> BoundCond, array<double, 3> lowerCorner,
                     array<double, 3> upperCorner, double Rcut) {
  /*creates halo particles which are added to the pairwise iterator
  pros: fast force computation
  cons: need to create and delete many new particles at each iteration */

  // compute the distance vector between both corner
  array<double, 3> diagonal = am.sub(upperCorner, lowerCorner);

  int NumThreads = 1;
#ifdef AUTOPAS_OPENMP
  NumThreads = omp_get_max_threads();
#endif

  vector<vector<shared_ptr<MoleculeMS>>> halolist;
  halolist.resize(NumThreads);

  // vector to fetch all particles near the boundary
  vector<MoleculeMS*> partlist;
  array<size_t, 5> regionsplit;

  vector<MoleculeMS*> partlist0;
  vector<MoleculeMS*> partlist1;
  vector<MoleculeMS*> partlist2;
  vector<MoleculeMS*> partlist3;
  vector<MoleculeMS*> partlist4;
  vector<MoleculeMS*> partlist5;

  for (auto p = cont->begin(); p.isValid(); ++p) {
    // check along X-axis
    if (BoundCond[0] == boundaries::periodic) {
      // copy particles from left to right
      array<double, 3> RegionLeftCorner = upperCorner;
      RegionLeftCorner[0] = lowerCorner[0] + Rcut;
      // iterate over all particles in the left region
      if (inBox(p->getR(), lowerCorner, RegionLeftCorner)) partlist0.push_back(&*p);

      // copy particles from right to left
      array<double, 3> RegionRightCorner = lowerCorner;
      RegionRightCorner[0] = upperCorner[0] - Rcut;
      // iterate over all particles in the right region
      if (inBox(p->getR(), RegionRightCorner, upperCorner)) partlist1.push_back(&*p);
      continue;
    }

    // check along Y-axis
    if (BoundCond[4] == boundaries::periodic) {
      // copy particles from low to up
      array<double, 3> RegionLowerCorner = upperCorner;
      RegionLowerCorner[1] = lowerCorner[1] + Rcut;
      RegionLowerCorner[0] += Rcut;  // add right-lower corner

      array<double, 3> SubLowerCorner = lowerCorner;
      SubLowerCorner[0] -= Rcut;  // add left-lower corner

      // iterate over all particles in the lower region
      if (inBox(p->getR(), SubLowerCorner, RegionLowerCorner)) partlist2.push_back(&*p);
      // copy particles from up to low
      array<double, 3> RegionUpperCorner = lowerCorner;
      RegionUpperCorner[1] = upperCorner[1] - Rcut;
      RegionUpperCorner[0] -= Rcut;  // add upper-left corner

      array<double, 3> SupUpperCorner = upperCorner;
      SupUpperCorner[0] += Rcut;  // add upper-right corner
      // iterate over all particles in the upper region
      if (inBox(p->getR(), RegionUpperCorner, SupUpperCorner)) partlist3.push_back(&*p);
      continue;
    }

    // check along Z-axis
    if (BoundCond[1] == boundaries::periodic) {
      // copy particles from front to back
      array<double, 3> RegionFrontCorner = upperCorner;
      RegionFrontCorner[2] = lowerCorner[2] + Rcut;
      RegionFrontCorner[0] += Rcut;
      RegionFrontCorner[1] += Rcut;

      array<double, 3> SubLowerCorner = lowerCorner;
      SubLowerCorner[0] -= Rcut;
      SubLowerCorner[1] -= Rcut;

      // iterate over all particles in the front region
      if (inBox(p->getR(), SubLowerCorner, RegionFrontCorner)) partlist4.push_back(&*p);

      // copy particles from back to front
      array<double, 3> RegionBackCorner = lowerCorner;
      RegionBackCorner[2] = upperCorner[2] - Rcut;
      RegionBackCorner[0] -= Rcut;
      RegionBackCorner[1] -= Rcut;

      array<double, 3> SupUpperCorner = upperCorner;
      SupUpperCorner[0] += Rcut;
      SupUpperCorner[1] += Rcut;

      // iterate over all particles in the left region
      if (inBox(p->getR(), RegionBackCorner, SupUpperCorner)) partlist5.push_back(&*p);
    }
  }

  // get the index which separes each region
  regionsplit[0] = partlist0.size();
  regionsplit[1] = regionsplit[0] + partlist1.size();
  regionsplit[2] = regionsplit[1] + partlist2.size();
  regionsplit[3] = regionsplit[2] + partlist3.size();
  regionsplit[4] = regionsplit[3] + partlist4.size();

  // compose the particle vector
  partlist.reserve(partlist0.size() + partlist1.size() + partlist2.size() + partlist3.size() + partlist4.size() +
                   partlist5.size());
  partlist = partlist0;
  partlist.insert(partlist.end(), partlist1.begin(), partlist1.end());
  partlist.insert(partlist.end(), partlist2.begin(), partlist2.end());
  partlist.insert(partlist.end(), partlist3.begin(), partlist3.end());
  partlist.insert(partlist.end(), partlist4.begin(), partlist4.end());
  partlist.insert(partlist.end(), partlist5.begin(), partlist5.end());

#ifdef AUTOPAS_OPENMP
#pragma omp parallel for schedule(static)
#endif
  for (size_t i = 0; i < partlist.size(); ++i) {
    if (i < regionsplit[0]) {
      shared_ptr<MoleculeMS> HaloParticle = make_shared<MoleculeMS>(*partlist[i]);
      HaloParticle->getR()[0] += diagonal[0];
#ifdef AUTOPAS_OPENMP
      halolist[omp_get_thread_num()].push_back(HaloParticle);
#else
      halolist[0].push_back(HaloParticle);
#endif
    } else if (i < regionsplit[1]) {
      shared_ptr<MoleculeMS> HaloParticle = make_shared<MoleculeMS>(*partlist[i]);
      HaloParticle->getR()[0] -= diagonal[0];
#ifdef AUTOPAS_OPENMP
      halolist[omp_get_thread_num()].push_back(HaloParticle);
#else
      halolist[0].push_back(HaloParticle);
#endif

    } else if (i < regionsplit[2]) {
      shared_ptr<MoleculeMS> HaloParticle = make_shared<MoleculeMS>(*partlist[i]);
      HaloParticle->getR()[1] += diagonal[1];
#ifdef AUTOPAS_OPENMP
      halolist[omp_get_thread_num()].push_back(HaloParticle);
#else
      halolist[0].push_back(HaloParticle);
#endif

    } else if (i < regionsplit[3]) {
      shared_ptr<MoleculeMS> HaloParticle = make_shared<MoleculeMS>(*partlist[i]);
      HaloParticle->getR()[1] -= diagonal[1];
#ifdef AUTOPAS_OPENMP
      halolist[omp_get_thread_num()].push_back(HaloParticle);
#else
      halolist[0].push_back(HaloParticle);
#endif

    } else if (i < regionsplit[4]) {
      shared_ptr<MoleculeMS> HaloParticle = make_shared<MoleculeMS>(*partlist[i]);
      HaloParticle->getR()[2] += diagonal[2];
#ifdef AUTOPAS_OPENMP
      halolist[omp_get_thread_num()].push_back(HaloParticle);
#else
      halolist[0].push_back(HaloParticle);
#endif

    } else {
      shared_ptr<MoleculeMS> HaloParticle = make_shared<MoleculeMS>(*partlist[i]);
      HaloParticle->getR()[2] -= diagonal[2];
#ifdef AUTOPAS_OPENMP
      halolist[omp_get_thread_num()].push_back(HaloParticle);
#else
      halolist[0].push_back(HaloParticle);
#endif
    }
  }
  for (auto list : halolist) {
    for (auto p : list) {
      cont->getContainer()->addHaloParticle(*p);
    }
  }
}
inline double QuaternionRotationX(std::array<double, 3> p, std::array<double, 4> q) {
  // computes the rotation of a given point in 3D according to the quaternion q and returns its X-coordinate
  double Qdot = am.dot(q, q);
  double s = 0;
  if (Qdot != 0.0) {
    s = 1 / Qdot;
  }
  // apply the rotation matrix derived from the quaternion
  return (1 - 2 * s * (q[2] * q[2] + q[3] * q[3])) * p[0] + 2 * s * (q[1] * q[2] - q[0] * q[3]) * p[1] +
         2 * s * (q[1] * q[3] + q[0] * q[2]) * p[2];
}

inline double QuaternionRotationY(std::array<double, 3> p, std::array<double, 4> q) {
  // computes the rotation of a given point in 3D according to the quaternion q and returns its Y-coordinate
  double Qdot = am.dot(q, q);
  double s = 0;
  if (Qdot != 0.0) {
    s = 1 / Qdot;
  }
  // apply the rotation matrix derived from the quaternion
  return 2 * s * (q[1] * q[2] + q[3] * q[0]) * p[0] + (1 - 2 * s * (q[1] * q[1] + q[3] * q[3])) * p[1] +
         2 * s * (q[2] * q[3] - q[1] * q[0]) * p[2];
}

inline double QuaternionRotationZ(std::array<double, 3> p, std::array<double, 4> q) {
  // computes the rotation of a given point in 3D according to the quaternion q and returns its Z-coordinate
  double Qdot = am.dot(q, q);
  double s = 0;
  if (Qdot != 0.0) {
    s = 1 / Qdot;
  }
  // apply the rotation matrix derived from the quaternion
  return 2 * s * (q[1] * q[3] - q[2] - q[0]) * p[0] + 2 * s * (q[2] * q[3] + q[1] * q[0]) * p[1] +
         (1 - 2 * s * (q[1] * q[1] + q[2] * q[2])) * p[2];
}

template <class T>
std::array<T, 3> crossProduct(std::array<T, 3> a, std::array<T, 3> b) {
  std::array<T, 3> ret;

  ret[0] = a[1] * b[2] - a[2] * b[1];
  ret[1] = a[2] * b[0] - a[0] * b[2];
  ret[2] = a[0] * b[1] - a[1] * b[0];
  return ret;
}

#endif
