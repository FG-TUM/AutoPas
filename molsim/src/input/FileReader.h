/*
 * FileReader.h
 *
 *  Created on: 23.02.2010
 *      Author: eckhardw
 */

#ifndef FILE_READER_H_
#define FILE_READER_H_

#include "../MoleculeMS.h"

#include "../ContainerProperties.h"
#include "../GenerateContainer.h"
#include "../MolSimFunctors.h"
#include "../ParticleType.h"
#include "autopas/AutoPas.h"
#include "autopas/autopasIncludes.h"
#include "particle_input.h"

#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <list>
#include <sstream>

using namespace autopas;
using namespace std;

class FileReader {
 public:
  FileReader() {}
  virtual ~FileReader() {}

  /**
   * Fill a particle container
   * with particles from a xml-file
   * @param particles the container to fill
   * @param filename the path to the xml-file containing information to generate the particles
   * @param Rcutoff distance to set the truncation distance of the LJ-calculation
   **/

  template <class Container, class Particle>
  bool readFile(Container* AutoPasObject, const char* filename, double Rcutoff, std::map<int, MoleculeType>& MolMap,
                ConstantFunctor<Particle>* fconstant, MembraneFunctor<Particle>* fmembrane) {
    // check if file exists
    struct stat buffer;
    if (stat(filename, &buffer) != 0) {
      AutoPasLogger->error("File {0:s} not found!", filename);
      return false;
    }

    unique_ptr<input_t> pinput;
    try {
      pinput = particle_input(filename, xml_schema::flags::dont_validate);
    } catch (const std::exception& e) {
      AutoPasLogger->error("Caught exception during reading types!\n{}", e.what());
      return false;
    }

    long unsigned index = 0;
    try {
      readTypes(pinput, Rcutoff);
    } catch (const std::exception& e) {
      AutoPasLogger->error("Caught exception during reading types!\n{}", e.what());
      return false;
    }
    try {
      getMoleculeTypes(MolMap, pinput);
    } catch (const std::exception& e) {
      AutoPasLogger->error("Caught exception during reading molecules!\n{}", e.what());
      return false;
    }
    try {
      readSingleParticle(AutoPasObject, pinput, index, MolMap);
    } catch (const std::exception& e) {
      AutoPasLogger->error("Caught exception during reading SingleParticle!\n{}", e.what());
      return false;
    }
    try {
      readCuboid(AutoPasObject, pinput, index, MolMap, fconstant);
    } catch (const std::exception& e) {
      AutoPasLogger->error("Caught exception during reading Cuboid!\n{}", e.what());
      return false;
    }
    try {
      readMembrane(AutoPasObject, pinput, index, MolMap, fconstant, fmembrane);
    } catch (const std::exception& e) {
      AutoPasLogger->error("Caught exception during reading Membrane!\n{}", e.what());
      return false;
    }
    try {
      readSphere(AutoPasObject, pinput, index, MolMap, fconstant);
    } catch (const std::exception& e) {
      AutoPasLogger->error("Caught exception during reading Sphere!\n{}", e.what());
      return false;
    }
    return true;
  }

 private:
  /**
   * Parse a vector_t element into an array
   * @param src vector_t xml-element
   * @param dest output array
   **/
  void parseVector(vector_t src, std::array<double, 3>& dest) {
    dest[0] = src.x();
    dest[1] = src.y();
    dest[2] = src.z();
  }

  /**
   * Parse a int vector_t element into an array
   * @param src vector_t xml-element
   * @param dest output array
   **/
  void parseIntVector(int_vector_t src, std::array<int, 3>& dest) {
    dest[0] = src.x();
    dest[1] = src.y();
    dest[2] = src.z();
  }

  /**
   * Read types of particles from a xml node
   * @param pinput xml node containting particletype_t elements
   * @param Rcutoff the potential RtruncLJ
   **/
  void readTypes(const unique_ptr<input_t>& pinput, double Rcutoff) {
    int id = 0;
    double mass;
    double epsilon;
    double sigma;
    double charge;
    double RtruncLJ;
    bool fixed;

    for (input_t::types_input_const_iterator i = pinput->types_input().begin(); i != pinput->types_input().end(); ++i) {
      particletype_t type = *i;
      id = type.id();
      mass = type.mass();
      epsilon = type.epsilon();
      sigma = type.sigma();
      charge = type.charge();
      if (type.RtruncLJ().present()) {
        RtruncLJ = type.RtruncLJ().get();
      } else {
        RtruncLJ = Rcutoff;
      }

      if (type.fixed().present()) {
        fixed = type.fixed().get();
      } else {
        fixed = false;
      }

      ParticleType::setType(id, mass, epsilon, sigma, charge, RtruncLJ, fixed);
    }

    AutoPasLogger->info("Read {0:d} different particle types.", pinput->types_input().size());
  }

  void getMoleculeTypes(std::map<int, MoleculeType>& MolMap, const unique_ptr<input_t>& pinput) {
    for (input_t::molecules_const_iterator i = pinput->molecules().begin(); i != pinput->molecules().end(); ++i) {
      int type = i->type();
      // create the instance of the properties of the molecule, type is the unique ID of this molecule type
      MoleculeType props(type, false);

      // prepare global variables of the molecule (will be accessed through ParticleType), useful if the molecule is
      // considered as a whole
      double mass = 0;
      double epsilon = 1;
      double sigma = 0;
      double RtruncLJ = 0;
      double charge = 0;
      double partial_charge = 0;
      bool usePA = false;

      // read the different sites
      std::array<double, 3> s = {0, 0, 0};

      for (molecules_t::sites_const_iterator j = i->sites().begin(); j != i->sites().end(); ++j) {
        parseVector(j->coord(), s);

        // type of the current particle
        int typepart = j->typepart();
        int partID = ParticleType::getID(typepart);
        mass += ParticleType::getM(partID);
        epsilon *= ParticleType::getEpsilon(partID);
        sigma += ParticleType::getSigma(partID);
        charge += ParticleType::getCharge(partID);
        RtruncLJ = max(RtruncLJ, ParticleType::getRtruncLJ(partID));

        if (j->partial_charge().present()) partial_charge = j->partial_charge().get();

        props.addParticle(s, typepart, partial_charge);
      }
      props.computeDipole();

      // set use of principal axis
      if (i->usePA().present()) {
        usePA = i->usePA().get();
      } else {
        if (props.getIPA()[0] * props.getIPA()[1] * props.getIPA()[2] < 1e-8) usePA = true;
      }
      props.getEnablePA() = usePA;

      // test whether the mass center has been correctly entered
      std::array<double, 3> zero = {0, 0, 0};
      std::array<double, 3> MassCenter =
          computeMassCenter(props.getPosX(), props.getPosY(), props.getPosZ(), props.getMasspart());
      if (MassCenter == zero) {
        AutoPasLogger->info("mass center for molecule type {0:d} has been correctly specified", props.getID());
      } else {
        AutoPasLogger->warn("wrong mass center for molecule type {0:d} !", props.getID());
        AutoPasLogger->warn("angular momentums and subsequent rotations will be computed erroneuously", 0);
        AutoPasLogger->info("offset each site of the type {0:d} by [{1:f}, {2:f}, {3:f}]", props.getID(),
                            -MassCenter[0], -MassCenter[1], -MassCenter[2]);
      }

      // add the molecule properties to ParticleType
      sigma /= props.getNumPart();
      epsilon = pow(epsilon, 1.0 / props.getNumPart());
      ParticleType::setType(type, mass, epsilon, sigma, charge, RtruncLJ, props.getFixed());
      MolMap.insert(std::pair<int, MoleculeType>(type, props));
    }
    return;
  }

  /**
   * Read single particles from a xml node
   * @param AutoPasObject the container to add the particles
   * @param pinput xml node containing single_t-elements
   **/
  template <class Container>
  long unsigned readSingleParticle(Container* AutoPasObject, const unique_ptr<input_t>& pinput, long unsigned index,
                                   std::map<int, MoleculeType>& MolMap) {
    AutoPasLogger->debug("Read {0:d} single particles", pinput->single_input().size());

    std::array<double, 3> x = {0, 0, 0};
    std::array<double, 3> f = {0, 0, 0};
    std::array<double, 3> v = {1, 1, 1};
    int id = 0;

    for (input_t::single_input_const_iterator i = pinput->single_input().begin(); i != pinput->single_input().end();
         ++i) {
      single_t single_particle = *i;
      parseVector(single_particle.coord(), x);
      parseVector(single_particle.velocity(), v);
      id = single_particle.type();

      MoleculeType* props = nullptr;
      MoleculeMS m(x, v, id, index);

      // add the molecule properties if the considered particle is actually a molecule
      if (MolMap.find(id) != MolMap.end()) {
        props = &MolMap[id];
      }

      m.setProperties(props);

      if (single_particle.force().present()) {
        parseVector(single_particle.force().get(), f);
        m.setF(f);
      }
      ++index;

      AutoPasObject->addParticle(m);
    }
    return index;
  }

  /**
   * Read cuboids of particles from a xml node
   * @param particles the container to add the particles
   * @param pinput xml node containing cuboid_t-elements
   **/
  template <class Container, class Particle>
  long unsigned readCuboid(Container* AutoPasObject, const unique_ptr<input_t>& pinput, long unsigned index,
                           std::map<int, MoleculeType>& MolMap, ConstantFunctor<Particle>* fconstant) {
    AutoPasLogger->info("Read {0:d} cuboids", pinput->cuboid_input().size());

    std::array<double, 3> x = {0, 0, 0};
    std::array<double, 3> v = {1, 1, 1};
    std::array<int, 3> dim = {0, 0, 0};
    double t_end = 0;
    std::array<double, 3> f = {0, 0, 0};
    std::list<std::array<int, 3>> coord_force;
    double h = 1;
    int id = 0;

    for (input_t::cuboid_input_const_iterator i = pinput->cuboid_input().begin(); i != pinput->cuboid_input().end();
         ++i) {
      cuboid_t cuboid_particle = *i;
      parseVector(cuboid_particle.coord(), x);
      parseVector(cuboid_particle.velocity(), v);
      parseIntVector(cuboid_particle.dimension(), dim);
      h = cuboid_particle.mesh();
      id = cuboid_particle.type();

      if (cuboid_particle.force().present()) {
        parseVector(cuboid_particle.force().get(), f);
      }
      if (cuboid_particle.t_end_force().present()) {
        t_end = cuboid_particle.t_end_force().get();
        fconstant->setForceStatus(true);
      }

      for (membrane_t::coord_force_const_iterator j = cuboid_particle.coord_force().begin();
           j != cuboid_particle.coord_force().end(); ++j) {
        int_vector_t coord_f = *j;
        std::array<int, 3> fcoord = {0, 0, 0};
        parseIntVector(coord_f, fcoord);
        coord_force.push_back(fcoord);
      }

      MoleculeType* moleculeprops = nullptr;
      // add the molecule properties if the considered particle is actually a molecule
      if (MolMap.find(id) != MolMap.end()) {
        moleculeprops = &MolMap[id];
      }

      index += generateCuboid(AutoPasObject, moleculeprops, id, t_end, f, coord_force, x, dim, h, v, fconstant, index);
    }
    return index;
  }

  /**
   * Read membrane of particles from a xml node
   * @param particles the container to add the particles
   * @param pinput xml node containing membrane_t-elements
   **/
  template <class Container, class Particle>
  long unsigned readMembrane(Container* AutoPasObject, const unique_ptr<input_t>& pinput, long unsigned index,
                             std::map<int, MoleculeType>& MolMap, ConstantFunctor<Particle>* fconstant,
                             MembraneFunctor<Particle>* fmembrane) {
    AutoPasLogger->debug("Read {0:d} membranes", pinput->membrane_input().size());

    double k = 1;
    double r0 = 0;
    double t_end = 0;
    std::array<double, 3> f = {0, 0, 0};
    std::list<std::array<int, 3>> coord_force;
    std::array<double, 3> x = {0, 0, 0};
    std::array<double, 3> v = {1, 1, 1};
    std::array<int, 3> dim = {0, 0, 0};
    double h = 1;
    int id = 0;

    for (input_t::membrane_input_const_iterator i = pinput->membrane_input().begin();
         i != pinput->membrane_input().end(); ++i) {
      membrane_t membrane_particle = *i;
      k = membrane_particle.stiffness();
      r0 = membrane_particle.r_zero();
      if (membrane_particle.force().present()) {
        parseVector(membrane_particle.force().get(), f);
      }
      if (membrane_particle.t_end_force().present()) {
        t_end = membrane_particle.t_end_force().get();
        fconstant->setForceStatus(true);
      }

      for (membrane_t::coord_force_const_iterator j = membrane_particle.coord_force().begin();
           j != membrane_particle.coord_force().end(); ++j) {
        int_vector_t coord_f = *j;
        std::array<int, 3> fcoord = {0, 0, 0};
        parseIntVector(coord_f, fcoord);
        coord_force.push_back(fcoord);
      }
      parseVector(membrane_particle.coord(), x);
      parseVector(membrane_particle.velocity(), v);
      parseIntVector(membrane_particle.dimension(), dim);
      h = membrane_particle.mesh();
      id = membrane_particle.type();

      MoleculeType* moleculeprops = nullptr;
      // add the molecule properties if the considered particle is actually a molecule
      if (MolMap.find(id) != MolMap.end()) {
        moleculeprops = &MolMap[id];
      }

      index += generateMembrane(AutoPasObject, moleculeprops, id, k, r0, t_end, f, coord_force, x, dim, h, v, fconstant,
                                fmembrane, index);
    }
    return index;
  }

  /**
   * Read sphere of particles from a xml node
   * @param particles the container to add the particles
   * @param pinput xml node containting sphere_t-elements
   **/

  template <class Container, class Particle>
  long unsigned readSphere(Container* AutoPasObject, const unique_ptr<input_t>& pinput, long unsigned index,
                           std::map<int, MoleculeType>& MolMap, ConstantFunctor<Particle>* fconstant) {
    AutoPasLogger->debug("Read {0:d} spheres", pinput->sphere_input().size());

    std::array<double, 3> x = {0, 0, 0};
    std::array<double, 3> v = {1, 1, 1};
    double t_end = 10000;
    std::array<double, 3> f = {0, 0, 0};
    std::list<std::array<int, 3>> coord_force;
    int r = 1;
    double h = 1;
    int id = 0;

    for (input_t::sphere_input_const_iterator i = pinput->sphere_input().begin(); i != pinput->sphere_input().end();
         ++i) {
      sphere_t sphere_particle = *i;
      parseVector(sphere_particle.coord(), x);
      parseVector(sphere_particle.velocity(), v);
      r = sphere_particle.radius();
      h = sphere_particle.mesh();
      id = sphere_particle.type();

      if (sphere_particle.force().present()) {
        parseVector(sphere_particle.force().get(), f);
      }
      if (sphere_particle.t_end_force().present()) {
        t_end = sphere_particle.t_end_force().get();
        fconstant->setForceStatus(true);
      }

      for (membrane_t::coord_force_const_iterator j = sphere_particle.coord_force().begin();
           j != sphere_particle.coord_force().end(); ++j) {
        int_vector_t coord_f = *j;
        std::array<int, 3> fcoord = {0, 0, 0};
        parseIntVector(coord_f, fcoord);
        coord_force.push_back(fcoord);
      }

      MoleculeType* moleculeprops = nullptr;
      // add the molecule properties if the considered particle is actually a molecule
      if (MolMap.find(id) != MolMap.end()) {
        moleculeprops = &MolMap[id];
      }

      index += generateSphere(AutoPasObject, moleculeprops, id, t_end, f, coord_force, x, r, h, v, fconstant, index);
    }
    return index;
  }
  /**
   * computes the mass center from a given set of molecules with their respective masses
   * @param x vector containing the x-coordinates of the particles
   * @param y vector containing the y-coordinates of the particles
   * @param z vector containing the z-coordinates of the particles
   * @param m vector containing the masses
   * @return coordinates of the mass center
   */
  std::array<double, 3> computeMassCenter(std::vector<double> x, std::vector<double> y, std::vector<double> z,
                                          std::vector<double> m) {
    double mtotal = 0;
    double center_x = 0;
    double center_y = 0;
    double center_z = 0;

    for (size_t i = 0; i < x.size(); ++i) {
      center_x += x[i] * m[i];
      center_y += y[i] * m[i];
      center_z += z[i] * m[i];

      mtotal += m[i];
    }
    return {center_x / mtotal, center_y / mtotal, center_z / mtotal};
  }
};

#endif /* FILE_READER_H_ */
