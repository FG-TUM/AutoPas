/*
 * SingleFunctor.h
 *
 *  Created on: Jul 1, 2018
 *      Author: raffi
 */

#ifndef SRC_SINGLEFUNCTOR_H_
#define SRC_SINGLEFUNCTOR_H_

#include "MoleculeMS.h"
#include "autopas/autopasIncludes.h"

#include <assert.h>
#include <array>
#include <cmath>

template <class Particle>
class SingleFunctor {
 public:
  SingleFunctor<Particle>() {}

  ~SingleFunctor<Particle>() {}

  virtual void Functor(Particle& p) {}
};

#endif /* SINGLEFUNCTOR_H_ */
