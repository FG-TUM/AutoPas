#include "ContainerProperties.h"
#include "GenerateContainer.h"
#include "input/FileReader.h"
#include "outputWriter/CSVWriter.h"
#include "outputWriter/VTKWriter.h"
#include "outputWriter/XMLWriter.h"
#include "outputWriter/XYZWriter.h"
//#include "tests/BoundaryTest.h"
//#include "tests/LJFunctorTest.h"
//#include "tests/MoleculeTest.h"
//#include "tests/MoleculeFunctorTest.h"
//#include "tests/RotationTest.h"
//#include "tests/SoATest.h"
#include "Eigenvalues.h"
#include "HelperFunctions.h"
#include "MolSimFunctors.h"
#include "MoleculeMS.h"
#include "UpdateData.h"

#include <sys/time.h>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <list>
#include <memory>
#include <typeinfo>
//#include <cppunit/ui/text/TestRunner.h>
#include "setting/setting.h"

#include "autopas/AutoPas.h"
#include "autopas/autopasIncludes.h"

#include <omp.h>
#include <chrono>

using namespace std;
using namespace autopas;

/************************ forward declaration *************************/

template <typename autopasClass>
void plotParticles(int iteration, autopasClass *partcont);
template <typename autopasClass>
void plotParticlesXML(autopasClass *partcont, string filename);
template <typename autopasClass>
void plotParticlesProfile(autopasClass *partcont, int bucketNum, float sizeX, int iteration);

double start_time = 0;
double end_time = 1000;
double delta_t = 0.014;

enum ContainerStructure { directsum, linkedcells, verletlists };
enum ForceComputation { noforces, lennardjones, gravity, lennardjonesmolecules, membrane, coulomb };
enum PeriodicBCMethod { directComputation, directNeighboursComputation, HaloParticles };

string outputname = "output/MD_vtk";
string profileFile = "";

#define PROFILE_Y_INTERVAL 10000

/** \mainpage

\section req_sec Requirements

- xerces-c
- cppunit


\section run the simulation on the SUPERMUC2 Linux-Cluster

 - log in to to one login node (e.g. use ssh [username]@lxlogin5.lrz.de)
 - load the modules intel, gcc, git and cmake
 - clone the gitlab repository from the repository https://gitlab.lrz.de/ga78jey/AutoPas.git/
 - change to directory AutoPas/molsim
 - rename CMakeLists.txtCLUSTER to CMakeLists.txt
 - run cmake -D CMAKE_CXX_COMPILER=icpc .. to specify the intel compiler
 - disable BUILD_TEST and change other personal settings using ccmake ..
 - change to directory molsim
 - compile with make


\section args_sec Running Arguments

```
export OMP_NUM_THREADS=8
./MolSim filename
```
Run the simulation with configurations defined in 'filename'.
Set the environment variable **OMP_NUM_THREADS** to define the number of threads used.

```
./MolSim -test [TestSuite]
```
Run the testsuites. A specific suite can be passed. Otherwise all suites will be processed.


\subsection cfile_ssec Configuration File

Configuration files are xml file which specify needed setting.
These start with a **setting**-element which contains follwing property-elements:
- **outputname** - Base name of the output files
- **endfile** - Optional: save last particle states into a new xml input-file
- **inputfiles** - File containing information to create particles
- **frequency** - Write frequency of the output files
- **profileFile** - Optional: Write profile of y-velocity into this file
- **profileBucketsX** - Optional: Number of divisions in x-direction to profile y-velocity
- **delta_t** - The time step between iterations
- **t_end** - Time to simulate
- **sigma** - Sigma for Lennard-Jones calculation
- **epsilon** - Epsilon for Lennard-Jones calculation
- **b_factor** - Factor for Maxwell-Boltzmann Distribution
- **g_grav_x** - Acceleration due to gravity in x-direction
- **g_grav_y** - Acceleration due to gravity in y-direction
- **g_grav_z** - Acceleration due to gravity in z-direction
- **domainX** - x-Dimension of domain
- **domainY** - y-Dimension of domain
- **domainZ** - z-Dimension of domain
- **r_cutoff** - Maximum cutoff radius
- **CellType** - optional: Defines the main iteration algorithm: 0 is DirectSum, 1 is Linked Cells (default), 2 is
Verlet lists
- **Traversal** - optional: Defines the traversal of the linked cells: 0 is for sequential traversal, 1 is for C08
traversal (default), 2 is for sliced traversal
- **ForceComputationMethod** - optional: Defines the used functor: 0 is no interactions, 1 is Lennard-Jones potential
(default), 2 is Gravity, 3 is a Lennard-Jones potential for molecules, 4 is for membranes based on LJ for particles, 5
uses Coulomb interactions
- **PeriodicComputationType** -optional:  Defines the used method to compute the interactions at periodic boundaries: 0
is for a direct computation, with the inconvenience of a O(n²) complexity, 1 is similar to 0, except that only the
interactions with particles located at less than the cut off radius in normal directions from the original particle are
considered and 2 is for the instantiation of Halo particles at each iteration, which will however be covered by the
linked cells traversal (default)
- **Vectorisation** - optional: uses SoA if true and AoS if false (default)
- **bc_left** - Boundary condition at x=0: 1 is reflecting boundaries, 2 is periodic boundaries, else it is an outflow
- **bc_lower** - Boundary condition at y=0: 1 is reflecting boundaries, 2 is periodic boundaries, else it is an outflow
- **bc_upper** - Boundary condition at y=domainY: 1 is reflecting boundaries, 2 is periodic boundaries, else it is an
outflow
- **bc_right** - Boundary condition at x=domainX: 1 is reflecting boundaries, 2 is periodic boundaries, else it is an
outflow
- **bc_front** - Boundary condition at z=0: 1 is reflecting boundaries, 2 is periodic boundaries, else it is an outflow
- **bc_back** - Boundary condition at z=domainZ: 1 is reflecting boundaries, 2 is periodic boundaries, else it is an
outflow
- **thermostat** - Optional: Defines a thermostat that keeps the simulation at given temperature. This will override
**b_factor**!
  - **initial** - inital temperature
  - **timestep** - number of timesteps after which the thermostat is applied
  - **ignoreY** - Optional: true if the thermostat does not affect the y-component of the velocity
  - **heating** - Optional: defines a temperature change over time
      - **target** - target temperature
      - **temperature_step** - step size in which the temperature should be changed
      - **timestep** - number of timesteps after which the temperature is changed

Example file: setting.xml

\subsection ifile_ssec Input File
Input files are xml files which specify the particles in given simulation.
These start with a particle_input-element which can contain following elements with subelements:
- **types_input** - Defines the properties of a type
  - **id** - The unique identifier of the type
  - **mass** - The mass of a particle of this type
  - **sigma** - The sigma used for the Lennard-Jones formula
  - **epsilon** - The epsilon used for the Lennard-Jones formula
  - **charge** - charge to compute electrostatic and dipole interactions
  - **fixed** - Optional: true if particle cannot be moved
  - **RtruncLJ** - Optional: distance at which the Lennard-Jones force calculation should be truncated, if not
specified, it will be set to Rcutoff
- **molecules** - Optional: define the molecules, if molecules shall be used
  - **type** - give a unique ID to this molecule type , it has to be different from the type_id used for particles
  - *usePA** - define whether the angular velocity shall be calculated with the 3x3 inertia tensor (false) or in the
principal axis coordinate system
  - **sites** - define the particles composing the molecule,  at least 1 is required
    - **typepart** - type of the particle. The type refers to an ID defined beforehand in types_input
    - **coord** - position of this particle relative to the mass center
    - **partial_charge** - Optional: The partial charge of a site in a molecule can be derived from the different
electronegativities of the different elements constituting the molecule. For one given site, it is computed by the sum
over all bonds of: pc = G - N - B * X/(X + Y), where G is the atomic number, N the number of unbounded electrons, B the
number of electrons in the considered bond and X and Y the respective electronegativity of the considered element and of
the bounded element. If not specified, the algorithm assumes its value to be zero and therefore no dipole-dipole
interactions will occur.
- **single_input** - Create a single particle
  - **coord** - Position of particle
  - **force** - Optional: Current force enacting on the particle
  - **velocity** - Initial velocity of the partilce
  - **type** - id of the type of the particle
- **cuboid_input** - Create a cuboid of particles
  - **force** - Optional: force which acts on certain molecules
  - **t_end_force** - Optional: time when the optional force stops to act
  - **coord_force** - Optional: (any amount) mesh coordinates of the particles where the force acts
  - **coord** - Position of lower left front corner of the cuboid
  - **dimension** - Number of particles in each dimension
  - **mesh** - Spacing between each particle
  - **velocity** - Initial mean velocity of each particle
  - **type** - id of the type of the particle
- **sphere_input** - Create a sphere of particles
  - **force** - Optional: force which acts on certain molecules
  - **t_end_force** - Optional: time when the optional force stops to act
  - **coord_force** - Optional: (any amount) mesh coordinates of the particles where the force acts
  - **coord** - Center of the sphere
  - **radius** - Number of particles as radius
  - **mesh** - Spacing between each particle
  - **velocity** - Initial mean velocity of each particle
  - **type** - id of the type of the particle
- **membrane_input** - Create a membrane of particles
  - **stiffness** - stiffness k of the membrane
  - **r_zero** - average bond length of a molecule pair
  - **coord** - Position of lower left front corner of the cuboid
  - **dimension** - Number of particles in each dimension
  - **mesh** - Spacing between each particle
  - **velocity** - Initial mean velocity of each particle
  - **type** - id of the type of the particle
  - **force** - Optional: force which acts on certain molecules
  - **t_end_force** - Optional: time when the optional force stops to act
  - **coord_force** - Optional: (any amount) mesh coordinates of the particles where the force acts


Example file: example.xml

\section info_unit Information concerning the units
All lengths and positions shall be given in Angstrom (1 A = 10⁻¹⁰ m)
time is given in picoseconds, (1 ps = 10⁻⁹ s)
Subsequently, velocities are represented by A/ps.
Masses are specified in atomic mass units (1 u = 1.660 539 040(20) * 10⁻²⁷ kg)
Forces in the functors will thus be computed in u*A*ps⁻² which corresponds approximately to 1.661 * 10⁻¹³ N.

In the Lennard-Jones potential, sigma represents a distance, therefore it is given in Angstrom (e.g. in Argon sigma
= 3.4 A) and epsilon represents an energy, which will be given in u*A²*ps⁻² according to our unit system and 1 u*A²*ps⁻²
= 1.661 * 10⁻²³ J (e.g. in Argon epsilon = 1.65 * 10⁻²¹ J = 99.3 u*A²*ps⁻²) reference for water:
www.researchgate.net/figure/Lennard-Jones-paramenters-used-in-montmorillonite-with-SPC-E-water_tbl1_274480551


Charges and partial charges in the atoms of polarised molecules shall be given in elementary charges (1 e = 1.602 176
6208 (20) * 10⁻¹⁹ C) in Coulomb's Law, the Coulomb constant in the vacuum is therefore given as kc = 8.987 551 787 * 10⁹
N*m²*C⁻² = 138935.45755 u*A³*ps⁻²*e⁻²


*/

/*******************************MAIN************************************/

int main(int argc, char *argsv[]) {
  if ((argc > 1) && (string(argsv[1]) == "-test")) {  // arguments: -test [TestSuite]
    string testsuite = "";
    if (argc > 2) {
      testsuite = argsv[2];
    }
    std::cerr << "Tests were removed!" << std::endl;
    //		CppUnit::TextUi::TestRunner runner;
    //		runner.addTest(BoundaryTest::suite());
    //		runner.addTest(LJFunctorTest::suite());
    //		runner.addTest(MoleculeTest::suite());
    //		runner.addTest(MoleculeFunctorTest::suite());
    //		runner.addTest(RotationTest::suite());
    //		runner.addTest(SoATest::suite());
    //		runner.run(testsuite);
    return 0;
  } else if (argc != 2) {  // if invalid arguments
    Logger::create(std::cout);
    AutoPasLogger->warn("Erroneous program call! ", 0);
    AutoPasLogger->info("./molsym xml-file", 0);
    //    AutoPasLogger->info("./molsym -test [TestSuite]", 0);
    Logger::unregister();
    return 1;
  }
  // set main class AutoPasMS

  typedef FullParticleCell<MoleculeMS> CellType;

  // this also initializes the logger
  auto *AutoPasMS = new AutoPas<MoleculeMS, CellType>();

  // get map to store molecule types
  map<int, MoleculeType> MolMap;

  AutoPasLogger->set_level(spdlog::level::debug);

  AutoPasLogger->info("Hello from MolSim for PSE!", 0);

#ifdef AUTOPAS_OPENMP
  AutoPasLogger->info("OpenMP parallelization is enabled with {0:d} threads.", omp_get_max_threads());
#endif

  /*******************read from input file************************+***/
  AutoPasLogger->info("Read setting-file...", 0);
  unique_ptr<setting_t> s(setting(argsv[1], xml_schema::flags::dont_validate));
  delta_t = s->delta_t();
  end_time = s->t_end();

  float b_factor = s->b_factor();

  float domainX = s->domainX();
  float domainY = s->domainY();
  float domainZ = s->domainZ();
  double r_cutoff = s->r_cutoff();

  AutoPasLogger->debug("boundaries: outflow: {}, reflective: {}, periodic: {}", boundaries::outflow,
                       boundaries::reflective, boundaries::periodic);
  AutoPasLogger->debug("Container : DirectSum: {}, LinkedCells: {}, VerletLists: {}",
                       autopas::ContainerOptions::directSum, autopas::ContainerOptions::linkedCells,
                       autopas::ContainerOptions::verletLists);
  AutoPasLogger->debug("Traversals: c08: {}, sliced: {}", autopas::TraversalOptions::c08,
                       autopas::TraversalOptions::sliced);

  std::vector<autopas::ContainerOptions> containerOptions;
  for (auto c : s->Container()) {
    transform(c.begin(), c.end(), c.begin(), ::tolower);
    if (c.find("dir") != string::npos) {
      containerOptions.push_back(autopas::ContainerOptions::directSum);
    }
    if (c.find("link") != string::npos) {
      containerOptions.push_back(autopas::ContainerOptions::linkedCells);
    }
    if (c.find("ver") != string::npos) {
      containerOptions.push_back(autopas::ContainerOptions::verletLists);
    }
  }

  vector<int> ForceComputationMethod;
  for (auto i = s->ForceComputationMethod().begin(); i != s->ForceComputationMethod().end(); ++i) {
    ForceComputationMethod.push_back(*i);
  }
  if (ForceComputationMethod.size() == 0) ForceComputationMethod.push_back(ForceComputation::lennardjones);

  int PeriodicBoundaryMethod = PeriodicBCMethod::HaloParticles;
  if (s->PeriodicComputationType().present()) PeriodicBoundaryMethod = s->PeriodicComputationType().get();

  DataLayoutOption DataStructure = autopas::DataLayoutOption::aos;
  if (s->Vectorisation().present()) {
    if (s->Vectorisation().get()) DataStructure = autopas::DataLayoutOption::soa;
  }

  // set the boundary conditions
  std::array<int, 6> BoundCond;

  BoundCond[0] = s->bc_left();
  BoundCond[1] = s->bc_upper();
  BoundCond[2] = s->bc_right();
  BoundCond[3] = s->bc_lower();
  BoundCond[4] = s->bc_front();
  BoundCond[5] = s->bc_back();

  if ((BoundCond[0] == boundaries::periodic) != (BoundCond[2] == boundaries::periodic) ||
      (BoundCond[1] == boundaries::periodic) != (BoundCond[3] == boundaries::periodic) ||
      (BoundCond[4] == boundaries::periodic) != (BoundCond[5] == boundaries::periodic)) {
    AutoPasLogger->warn("Error: Opposite sides must both be set to periodic", 0);
    return 1;
  }

  int frequency = s->frequency();

  outputname = s->outputname();
  string endfile = "";
  if (s->endfile().present()) {
    endfile = s->endfile().get();
  }

  int profileBucketsX = 0;
  if (s->profileFile().present()) {
    profileFile = s->profileFile().get();
    AutoPasLogger->info("Parse y-velocity profile into {0:s}", profileFile);

    if (s->profileBucketsX().present()) {
      profileBucketsX = s->profileBucketsX().get();
    }
  }

  float temperature = nan("");
  int T_timestep = 0;
  float TT_target = nan("");
  bool T_ignoreY = false;
  float TT_Tstep = nan("");
  int TT_timestep = 0;
  if (s->thermostat().present()) {
    thermo_t thermo = s->thermostat().get();
    temperature = thermo.initial();
    T_timestep = thermo.timestep();

    b_factor = sqrt(temperature);

    if (thermo.ignoreY().present()) {
      T_ignoreY = thermo.ignoreY().get();
    }

    if (thermo.heating().present()) {
      thermo_target_t thermo_target = thermo.heating().get();
      TT_target = thermo_target.target();
      TT_Tstep = thermo_target.temperature_step();
      TT_timestep = thermo_target.timestep();
    }
  }

  std::array<double, 3> BoxMin = {0.0, 0.0, 0.0};
  std::array<double, 3> BoxMax = {domainX, domainY, domainZ};

  std::vector<autopas::TraversalOptions> traversalOptions;
  for (auto t : s->Traversal()) {
    transform(t.begin(), t.end(), t.begin(), ::tolower);
    if (t.find("c08") != string::npos) {
      traversalOptions.push_back(autopas::TraversalOptions::c08);
    }
    if (t.find("sli") != string::npos) {
      traversalOptions.push_back(autopas::TraversalOptions::sliced);
    }
  }

  // chose the underlying data structure
  AutoPasMS->init(BoxMin, BoxMax, r_cutoff, 0.3 * r_cutoff, 20, containerOptions, traversalOptions);
  AutoPasMS->init(BoxMin, BoxMax, r_cutoff, 0.3 * r_cutoff, 20, containerOptions, traversalOptions);
  AutoPasMS->init(BoxMin, BoxMax, r_cutoff, 0.3 * r_cutoff, 20, containerOptions, traversalOptions);

  // Single functor declarations
  GlobalGravityFunctor<MoleculeMS> *fggrav = new GlobalGravityFunctor<MoleculeMS>();
  ConstantFunctor<MoleculeMS> *fconst = new ConstantFunctor<MoleculeMS>();
  MembraneFunctor<MoleculeMS> *fmembrane = new MembraneFunctor<MoleculeMS>();

  // Pairwise functor declarations
  GravityFunctor<MoleculeMS, CellType> fgravity(r_cutoff);
  LennardJonesFunctor<MoleculeMS, CellType> fLJ(r_cutoff);
  LennardJonesMoleculesFunctor<MoleculeMS, CellType> fLJmol(r_cutoff);
  NoForcesFunctor<MoleculeMS, CellType> fnoforces(r_cutoff);
  CoulombFunctor<MoleculeMS, CellType> fCoulomb(r_cutoff);

  int NumFunctors = ForceComputationMethod.size();
  vector<Functor<MoleculeMS, CellType> *> func;

  for (int fcm : ForceComputationMethod) {
    switch (fcm) {
      case (ForceComputation::gravity):
        func.push_back(&fgravity);
        break;

      case (ForceComputation::noforces):
        func.push_back(&fnoforces);
        break;

      case (ForceComputation::lennardjonesmolecules):
        func.push_back(&fLJmol);
        break;

      case (ForceComputation::membrane):
        func.push_back(&fLJ);
        break;

      case (ForceComputation::coulomb):
        func.push_back(&fCoulomb);
        break;

      default:
        func.push_back(&fLJ);
    }
  }
  assert(func.size() == ForceComputationMethod.size());

  AutoPasLogger->info("Read input files:", 0);

  FileReader fileReader;

  for (setting_t::inputfiles_const_iterator i(s->inputfiles().begin()); i != s->inputfiles().end(); ++i) {
    const char *filename = (*i).c_str();
    AutoPasLogger->info(" read... {0:s}", filename);
    if (not fileReader.readFile(AutoPasMS, filename, r_cutoff, MolMap, fconst, fmembrane)) return -1;
  }

  std::array<double, 3> g;
  g[0] = s->g_grav_x();
  g[1] = s->g_grav_y();
  g[2] = s->g_grav_z();

  fggrav->SetParameters(g);

  unsigned long NumParticles = 0;

  for (auto p = AutoPasMS->begin(); p.isValid(); ++p) {
    // count particles
    ++NumParticles;

    // apply brownian motion
    MaxwellBoltzmannDistribution(*p, b_factor, 3);
  }

  AutoPasLogger->info("particles Total {0:d}", NumParticles);

  // prepare main loop
  double current_time = start_time;

  int iteration = 0;

  // prepare thermostat
  if (!isnan(temperature)) setTemperature(AutoPasMS, temperature, T_ignoreY);

  // plot the particles once
  if (frequency != 0) plotParticles(iteration, AutoPasMS);

  // set the epsilon pairs for soa
  if (DataStructure == autopas::DataLayoutOption::soa) {
    std::vector<float> epsilon_pairs = ParticleType::getEpsilonPairs();
    fLJ.setEpsilonPairs(epsilon_pairs);
    fLJmol.setEpsilonPairs(epsilon_pairs);
  }

  // time measurement
  timeval start_timeofday;
  timeval end_timeofday;
  std::chrono::high_resolution_clock::time_point startForceCalc, endForceCalc;
  long totalTimeForceCalc = 0;
  timeval end_forcecalc;

  if (AutoPasMS->getContainer()->getContainerType() == autopas::ContainerOptions::linkedCells) {
    auto cellsPerDim =
        ((autopas::LinkedCells<MoleculeMS, autopas::FullParticleCell<MoleculeMS>> *)(AutoPasMS->getContainer()))
            ->getCellBlock()
            .getCellsPerDimensionWithHalo();
    AutoPasLogger->info("Cells per dimension : {} {} {} ", cellsPerDim[0], cellsPerDim[1], cellsPerDim[2]);
  }

  auto expectedNumIterations = (size_t)std::ceil((end_time - start_time) / delta_t);
  AutoPasLogger->info("Expected number of iterations: {}", expectedNumIterations);
  AutoPasLogger->info("Number of OpenMP Threads: {}", autopas::autopas_get_max_threads());
  /********************************** MAIN LOOP *************************************/
  while (current_time < end_time) {
    // start timer after the first iteration
    if (iteration == 1) gettimeofday(&start_timeofday, 0);

    AutoPasLogger->trace("begin - container update", 0);
    // update container (necessary for LinkedCells)
    if (AutoPasMS->getContainer()->isContainerUpdateNeeded()) AutoPasMS->getContainer()->updateContainer();

    AutoPasLogger->trace("container update - resetF", 0);
    // reset f
    resetF(AutoPasMS);

    AutoPasLogger->trace("resetF - single functors", 0);

    // apply the single particle functors
    std::vector<MoleculeMS *> particlelistall;
    for (auto p = AutoPasMS->begin(); p.isValid(); ++p) {
      particlelistall.push_back(&*p);
    }
#ifdef AUTOPAS_OPENMP
#pragma omp parallel for schedule(guided)
#endif
    for (size_t i = 0; i < particlelistall.size(); ++i) {
      MoleculeMS *p = particlelistall[i];
      fggrav->Functor(*p);
      fconst->Functor(*p);
      fmembrane->Functor(*p);
    }

    // apply eventual boundary conditions
    AutoPasLogger->trace("single functors - reflective BC", 0);
    applyReflectiveBC(AutoPasMS, &fLJ, BoundCond, BoxMin, BoxMax, r_cutoff);
    AutoPasLogger->trace("reflective BC - periodic BC", 0);

    switch (PeriodicBoundaryMethod) {
      case PeriodicBCMethod::directComputation:
        for (int i = 0; i < NumFunctors; ++i) {
          computePeriodicBC(AutoPasMS, func[i], BoundCond, BoxMin, BoxMax, r_cutoff);
        }
        break;
      case PeriodicBCMethod::directNeighboursComputation:
        for (int i = 0; i < NumFunctors; ++i) {
          computeNeighboursPeriodicBC(AutoPasMS, func[i], BoundCond, BoxMin, BoxMax, r_cutoff);
        }
        break;
      default:
        applyPeriodicBC(AutoPasMS, BoundCond, BoxMin, BoxMax, r_cutoff);
    }

    AutoPasLogger->trace("periodic BC - pairwise iteration", 0);

    // iterate through all functors
    startForceCalc = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < NumFunctors; ++i) {
      if (auto functor = dynamic_cast<GravityFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> *>(func[i]))
        AutoPasMS->iteratePairwise(functor, DataStructure);
      else if (auto functor = dynamic_cast<NoForcesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> *>(func[i]))
        AutoPasMS->iteratePairwise(functor, DataStructure);
      else if (auto functor =
                   dynamic_cast<LennardJonesMoleculesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> *>(func[i]))
        AutoPasMS->iteratePairwise(functor, DataStructure);
      else if (auto functor = dynamic_cast<LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> *>(func[i]))
        AutoPasMS->iteratePairwise(functor, DataStructure);
      else if (auto functor = dynamic_cast<CoulombFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> *>(func[i]))
        AutoPasMS->iteratePairwise(functor, DataStructure);
      else
        AutoPasLogger->error("Cannot cast unknown functor!");
    }
    endForceCalc = std::chrono::high_resolution_clock::now();

    // if container is linked cells print number of empty cells
    if (AutoPasLogger->level() == spdlog::level::debug &&
        AutoPasMS->getContainer()->getContainerType() == autopas::ContainerOptions::linkedCells) {
      size_t numEmptyCells = 0;
      LinkedCells<MoleculeMS, FullParticleCell<MoleculeMS>> *lcContainer =
          dynamic_cast<LinkedCells<MoleculeMS, FullParticleCell<MoleculeMS>> *>(AutoPasMS->getContainer());
      for (auto &&cell : lcContainer->getCells()) {
        if (not cell.isNotEmpty()) ++numEmptyCells;
      }
      AutoPasLogger->debug("Number of empty cells: {}", numEmptyCells);
    }

    AutoPasLogger->trace("pairwise iteration - delete halo", 0);

    // delete eventually created halo particles
    AutoPasMS->getContainer()->deleteHaloParticles();

    AutoPasLogger->trace("delete halo - partial integrations", 0);

    std::vector<MoleculeMS *> particlelist;
    for (auto p = AutoPasMS->begin(); p.isValid(); ++p) {
      if (p->getFixed()) {
        continue;
      }
      particlelist.push_back(&*p);
    }
#ifdef AUTOPAS_OPENMP
#pragma omp parallel for schedule(guided)
#endif
    for (size_t i = 0; i < particlelist.size(); ++i) {
      MoleculeMS *p = particlelist[i];

      // calculate new v
      calculateV(p, delta_t);

      // calculate new x
      calculateR(p, BoundCond, BoxMin, BoxMax, delta_t);

      // calculate molecule specific interactions
      calculateW(p, delta_t);
      calculateO(p, delta_t);
    }
    AutoPasLogger->trace("partial integrations - test end force", 0);

    // test end of force up
    testEndaddForce(AutoPasMS, current_time, fconst);
    AutoPasLogger->trace("test end force - thermostat", 0);

    // adjust velocities according to thermostat
    if (!isnan(TT_target) && temperature != TT_target) {
      if (TT_timestep > 0 && iteration % TT_timestep == 0) {
        if (TT_target - temperature < TT_Tstep) {
          temperature = TT_target;
        } else {
          temperature += TT_Tstep;
        }
      }
    }

    if (!isnan(temperature)) {
      if (iteration % T_timestep == 0) {
        setTemperature(AutoPasMS, temperature, T_ignoreY);
      }
    }
    AutoPasLogger->trace("thermostat - end", 0);

    if (profileBucketsX > 0 && iteration % PROFILE_Y_INTERVAL == 0) {
      plotParticlesProfile(AutoPasMS, profileBucketsX, domainX, iteration);
    }

    iteration++;
    AutoPasLogger->trace("Iteration {0:d} at time {1:f} finished.", iteration, current_time);
    if ((frequency > 0) && (iteration % frequency == 0)) {
      AutoPasLogger->info("Plot iteration {0:d}", iteration);
      plotParticles(iteration, AutoPasMS);
    }

    current_time += delta_t;
    auto timeForceCalc = chrono::duration_cast<chrono::milliseconds>(endForceCalc - startForceCalc).count();
    AutoPasLogger->info("Force calculation in iteration {} took: {} ms", iteration ,timeForceCalc);
    totalTimeForceCalc += timeForceCalc;
  }

  gettimeofday(&end_timeofday, 0);
  long long int computation_sec = end_timeofday.tv_sec - start_timeofday.tv_sec;
  long long int computation_usec = 1000000 * computation_sec + end_timeofday.tv_usec - start_timeofday.tv_usec;

  --iteration;  // since the first is ignored by the timer

  unsigned long NumParticlesEnd = 0;
  for (auto p = AutoPasMS->begin(); p.isValid(); ++p, ++NumParticlesEnd)
    ;
  // get Molecule update rate per second
  double MUPS = 1000000.0 * (iteration * NumParticlesEnd) / computation_usec;

  // compute flops
  FlopCounterFunctor<MoleculeMS, CellType> flopCounter(AutoPasMS->getContainer()->getCutoff());
  AutoPasMS->iteratePairwise(&flopCounter, DataStructure);
  auto flops = flopCounter.getFlops(18ul) * iteration;
  AutoPasLogger->info("{0:d} particles remaining at the end", NumParticlesEnd);
  AutoPasLogger->info("Molecule update rate per second: {0:f}", MUPS);
  AutoPasLogger->info("Flop/s: {0:f}", 1000000 * flops / computation_usec);
  AutoPasLogger->info("Runtime: {0:f} s", (computation_usec / 1000000.0));
  AutoPasLogger->info("Avg Force Calculation time: {0:f} ms", (totalTimeForceCalc / (double)(iteration + 1)));

  if (endfile.compare("")) {
    plotParticlesXML(AutoPasMS, endfile);
  }

  AutoPasLogger->info("output written. Terminating...", 0);
  return 1;
}

/**
 * plot the y-profile to a csv file
 */
template <typename Container>
void plotParticlesProfile(Container *particles, int bucketNum, float sizeX, int iteration) {
  outputWriter::CSVWriter writer(bucketNum, sizeX);
  writer.plotParticles(particles, profileFile, iteration);
}

/**
 * plot the particles to a vtk-file
 */
template <typename Container>
void plotParticles(int iteration, Container *particles) {
  outputWriter::VTKWriter writer;
  long unsigned i = 0;
  for (auto pp = particles->begin(); pp.isValid(); ++pp) {
    int add = 1;
    if (pp->getProperties() != nullptr) {
      add *= pp->getNumPart();
    }
    i += add;
  }
  writer.initializeOutput(i);
  auto iterator = particles->begin();
  while (iterator.isValid()) {
    MoleculeMS &p = *iterator;

    writer.plotParticle(p);
    ++iterator;
  }

  writer.writeFile(outputname, iteration);
}
/**
 * plot the particles to a xml-file
 */
template <class Container>
void plotParticlesXML(Container *particles, string filename) {
  outputWriter::XMLWriter writer;
  writer.initializeOutput();

  auto iterator = particles->begin();
  while (iterator.isValid()) {
    MoleculeMS &p = *iterator;

    writer.plotParticle(p);

    ++iterator;
  }
  writer.writeFile(filename);
}
