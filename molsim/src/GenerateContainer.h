#ifndef SRC_GENERATECONTAINER_H_
#define SRC_GENERATECONTAINER_H_

#include "HelperFunctions.h"
#include "MolSimFunctors.h"
#include "MoleculeMS.h"
#include "MoleculeType.h"
#include "autopas/autopasIncludes.h"

#include <assert.h>
#include <omp.h>
#include <array>
#include <cmath>
#include <list>
#include <vector>
#include "MaxwellBoltzmannDistribution.h"

using namespace autopas;

/**
 * generates a cuboid of particles
 * @param Container to which the particle shall be put
 * @param type type of each particle
 * @param t_end time when the force up should disappear
 * @param force force acting on certain particles in the membrane
 * @param force_coord list of mesh coordinates of the particles supporting an external force
 * @param pos coordinates of lower left front-side corner
 * @param dim number of particles in each dimension
 * @param h mesh width
 * @Param properties of the container
 * @param v velocity of each particle
 */
template <class autopasClass, class Particle>
unsigned long generateCuboid(autopasClass* autopasMS, MoleculeType* moleculeprops, int type, double t_end,
                             std::array<double, 3> force, std::list<std::array<int, 3>> coord_force,
                             std::array<double, 3> pos, std::array<int, 3> dim, double h, std::array<double, 3> v,
                             ConstantFunctor<Particle>* fconstant, unsigned long index) {
  fconstant->setEndTime(t_end);

  // for each step in each dimension
  for (int x = 0; x < dim[0]; x++) {
    for (int y = 0; y < dim[1]; y++) {
      for (int z = 0; z < dim[2]; z++) {
        // create a Molecule and add to container
        MoleculeMS p = MoleculeMS(type, index);

        std::array<double, 3> r = {pos[0] + x * h, pos[1] + y * h, pos[2] + z * h};
        p.setR(r);

        p.setV(v);

        p.setProperties(moleculeprops);

        // add external force on certain Molecules
        for (std::list<std::array<int, 3>>::iterator c = coord_force.begin(); c != coord_force.end(); ++c) {
          std::array<int, 3> meshcoord = {x, y, z};
          if (*c == meshcoord) {
            std::pair<unsigned long, std::array<double, 3>> thepair = std::make_pair(index, force);
            fconstant->addParticle(thepair);
          }
        }

        autopasMS->addParticle(p);

        ++index;
      }
    }
  }
  return index;
}

/**
 * generates a cuboid of membrane particles
 * @param autopasClass to which the particle shall be put
 * @param properties of the container
 * @param type type of each particle
 * @param stiffness stiffness k of the membrane
 * @param r_zero average bond length of a molecule pair
 * @param t_end time when the force up should disappear
 * @param force force acting on certain particles in the membrane
 * @param force_coord list of mesh coordinates of the particles supporting an external force
 * @param pos coordinates of lower left front-side corner
 * @param dim number of particles in each dimension
 * @param h mesh width
 * @Param properties of the container
 * @param v velocity of each particle
 */

template <class autopasClass, class Particle>
unsigned long generateMembrane(autopasClass* autopasMS, MoleculeType* moleculeprops, int type, double stiffness,
                               double r_zero, double t_end, std::array<double, 3> force,
                               std::list<std::array<int, 3>> coord_force, std::array<double, 3> pos,
                               std::array<int, 3> dim, double h, std::array<double, 3> v,
                               ConstantFunctor<Particle>* fconstant, MembraneFunctor<Particle>* fmembrane,
                               unsigned long index) {
  fconstant->setEndTime(t_end);
  fmembrane->SetParameters(stiffness, r_zero, true);

  // for each step in each dimension
  for (int z = 0; z < dim[2]; z++) {
    int i = 0;

    std::vector<MoleculeMS*> PartIndex;
    for (int y = 0; y < dim[1]; y++) {
      for (int x = 0; x < dim[0]; x++) {
        // get actual mesh coordinates in vector
        std::array<int, 3> meshcoord = {x, y, z};

        // create a Molecule and add to container
        MoleculeMS p = MoleculeMS(type, index);

        std::array<double, 3> r = {pos[0] + x * h, pos[1] + y * h, pos[2] + z * h};
        p.setR(r);

        p.setV(v);

        p.setProperties(moleculeprops);

        // add external force on certain Molecules
        for (std::list<std::array<int, 3>>::iterator c = coord_force.begin(); c != coord_force.end(); ++c) {
          if (*c == meshcoord) {
            std::pair<unsigned long, std::array<double, 3>> thepair = std::make_pair(index, force);
            fconstant->addParticle(thepair);
          }
        }

        // add to the container
        autopasMS->addParticle(p);
        ++index;

        PartIndex.push_back(&p);

        if (PartIndex[i] == nullptr) {
          continue;
          i++;
        }
        MoleculeMS& actual = *PartIndex[i];

        // complete neighbour vectors
        // all except the lower row
        if (i >= dim[0]) {
          actual.getDirectNeighbours()[3] = PartIndex[i - dim[0]];
          actual.getDirectNeighbours()[3]->getDirectNeighbours()[1] = &actual;

          // ignore the left side
          if (i % dim[0] != 0) {
            actual.getDirectNeighbours()[0] = PartIndex[i - 1];
            actual.getDiagonalNeighbours()[3] = PartIndex[i - dim[0] - 1];
            actual.getDirectNeighbours()[0]->getDirectNeighbours()[2] = &actual;
            actual.getDiagonalNeighbours()[3]->getDiagonalNeighbours()[1] = &actual;
          }
          // ignore the right side
          if ((i + 1) % dim[0] != 0) {
            actual.getDiagonalNeighbours()[2] = PartIndex[i - dim[0] + 1];
            actual.getDiagonalNeighbours()[2]->getDiagonalNeighbours()[0] = &actual;
          }
        }
        // fill the lower row
        else if (i > 0) {
          actual.getDirectNeighbours()[0] = PartIndex[i - 1];
          actual.getDirectNeighbours()[0]->getDirectNeighbours()[2] = &actual;
        }

        ++i;
      }
    }
  }
  return index;
}

/**
 * generates a sphere of particles
 * @param Container to which the particle shall be put
 * @param properties of the container
 * @param type type of each particle
 * @param t_end time when the force up should disappear
 * @param force force acting on certain particles in the membrane
 * @param force_coord list of mesh coordinates of the particles supporting an external force
 * @param pos coordinates of the center of sphere
 * @param r number of particles as radius
 * @param h mesh width
 * @Param properties of the container
 * @param m mass of each particle
 * @param v velocity of each particle
 */

template <typename autopasClass, class Particle>
unsigned long generateSphere(autopasClass* autopasMS, MoleculeType* moleculeprops, int type, double t_end,
                             std::array<double, 3> force, std::list<std::array<int, 3>> coord_force,
                             std::array<double, 3> pos, int r, double h, std::array<double, 3> v,
                             ConstantFunctor<Particle>* fconstant, unsigned long index, bool twoD = false) {
  int r_sqr = r * r;

  fconstant->setEndTime(t_end);

  // for each step in each dimension
  for (int x = -r; x <= r; x++) {
    int x_sqr = x * x;
    // TODO: read twoD from input file
    // if the sphere is 2D only place 1 particle in y direction
    for (int y = (twoD ? -r : r); y <= r; y++) {
      int y_sqr = y * y;
      for (int z = -r; z <= r; z++) {
        int z_sqr = z * z;

        if (x_sqr + y_sqr + z_sqr < r_sqr) {
          // create a Molecule and add to container
          MoleculeMS p = MoleculeMS(type, index);

          std::array<double, 3> r = {pos[0] + x * h, pos[1] + y * h, pos[2] + z * h};
          p.setR(r);

          p.setV(v);

          p.setProperties(moleculeprops);

          // add external force on certain Molecules
          for (std::list<std::array<int, 3>>::iterator c = coord_force.begin(); c != coord_force.end(); ++c) {
            std::array<int, 3> meshcoord = {x, y, z};
            if (*c == meshcoord) {
              std::pair<unsigned long, std::array<double, 3>> thepair = std::make_pair(index, force);
              fconstant->addParticle(thepair);
            }
          }

          autopasMS->addParticle(p);

          ++index;
        }
      }
    }
  }
  return index;
}

/**
 * add a thermostat which regulate the temperature to T
 * @param Container to which the thermostat shall be applied
 * @param T new temperature
 * @param ignoreY true if y-component is ignored
 */

template <typename autopasClass>
void setTemperature(autopasClass* autopasMS, float T_target, bool ignoreY) {
  if (ignoreY) {
    double T = 0;
    int count = 0;

    // get sum of velocity squared in xz and count non-fix particles
    for (auto pp = autopasMS->begin(); pp.isValid(); ++pp) {
      MoleculeMS& p = *pp;

      if (p.getFixed()) {
        continue;
      }

      std::array<double, 3> v = p.getV();
      double v_xz_sqr = v[0] * v[0] + v[2] * v[2];

      T += p.getM() * v_xz_sqr;
      count++;
    }

    T /= 3 * count;
    double beta = sqrt(T_target / T);

    // scale xz-velocity of all non-fix Molecules
    for (auto pp = autopasMS->begin(); pp.isValid(); ++pp) {
      MoleculeMS& p = *pp;

      if (p.getFixed()) {
        continue;
      }

      std::array<double, 3> v = p.getV();
      v[0] *= beta;
      v[2] *= beta;
      p.setV(v);
    }
  } else {
    double T = 0;
    double Trot = 0;
    int count = 0;

    // get sum of velocity squared and count non-fix Molecules
    for (auto pp = autopasMS->begin(); pp.isValid(); ++pp) {
      MoleculeMS& p = *pp;
      if (p.getFixed()) {
        continue;
      }

      std::array<double, 3> vel = p.getV();
      std::array<double, 3> w = p.getW();
      std::array<double, 6> I = {0, 0, 0, 0, 0, 0};
      if (p.getProperties() != nullptr) I = p.getProperties()->getI();
      double Vnormsqr = vel[0] * vel[0] + vel[1] * vel[1] + vel[2] * vel[2];
      Trot += I[0] * w[0] * w[0] + I[1] * w[1] * w[1] + I[2] * w[2] * w[2] + I[3] * w[0] * w[1] * 2 +
              I[4] * w[1] * w[2] * 2 + I[5] * w[0] * w[2] * 2;
      T += p.getM() * Vnormsqr;
      count++;
    }

    T /= 3 * count;
    Trot /= 3 * count;
    double beta = 0;
    if (T != 0) beta = sqrt(T_target / T);

    double betarot = 0;
    if (Trot != 0) betarot = sqrt(T_target / Trot);

    //		AutoPasLogger->info("Trot: {0:f}, T= {1:f}", Trot, T);
    //		AutoPasLogger->info("betarot: {0:f}, beta: {1:f}", betarot, beta);

    // scale velocity of all non-fix Molecules
    for (auto pp = autopasMS->begin(); pp.isValid(); ++pp) {
      MoleculeMS& p = *pp;

      if (p.getFixed()) {
        continue;
      }

      p.getV() = am.mulScalar(p.getV(), beta);
      p.getW() = am.mulScalar(p.getW(), betarot);
    }
  }
}

#endif
