/*
 * GlobalGravityFunctor.h
 *
 *  Created on: Jul 1, 2018
 *      Author: raffi
 */
#ifndef SRC_GLOBALGRAVITYFUNCTOR_H_
#define SRC_GLOBALGRAVITYFUNCTOR_H_

#include "MoleculeMS.h"
#include "SingleFunctor.h"
#include "autopas/autopasIncludes.h"

#include <assert.h>
#include <array>
#include <cmath>

using namespace autopas;

template <class Particle>
class GlobalGravityFunctor : SingleFunctor<Particle> {
 public:
  GlobalGravityFunctor<Particle>() { ggrav = {0., 0., 0.}; }

  ~GlobalGravityFunctor<Particle>() {}

  void Functor(Particle& p) override { p.addF(ArrayMath::mulScalar(ggrav, p.getM())); }

  void SetParameters(std::array<double, 3> g) { ggrav = g; }

 private:
  std::array<double, 3> ggrav;
};

#endif /* GRAVITYFUNCTOR_H_ */
