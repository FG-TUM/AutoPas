/*
 * Eigenvalues.h
 *
 *  Created on: Jun 9, 2018
 *      Author: raffi
 */

#ifndef SRC_EIGENVALUES_H_
#define SRC_EIGENVALUES_H_

#include <cmath>
#include "HelperFunctions.h"
#include "MoleculeMS.h"
#include "autopas/AutoPas.h"
#include "autopas/autopasIncludes.h"

using namespace std;

/*
 * computes the eigenvalues of a real symmetric 3*3 matrix
 * @param I considered matrix of the form: (I[0] I[3] I[5], I[3] I[1] I[4], I[5] I[4] I[2])
 * @return array containing the three computed eigenvalues
 */
inline std::array<double, 3> getEigenValues(std::array<double, 6> I) {
  // initialize output
  double eig0 = 0;
  double eig1 = 0;
  double eig2 = 0;

  double phi = 0;

  // test whether the matrix is diagonal
  if ((I[3] == 0) && (I[4] == 0) && (I[5] == 0)) {
    eig0 = I[0];
    eig1 = I[1];
    eig2 = I[2];
  } else {
    double trace = I[0] + I[1] + I[2];
    double m = trace / 3;  // mean value of the diagonal elements
    double p1 = I[3] * I[3] + I[4] * I[4] + I[5] * I[5];
    double p2 = (I[0] - m) * (I[0] - m) + (I[1] - m) * (I[1] - m) + (I[2] - m) * (I[2] - m) + 2 * p1;

    double p = sqrt(p2 / 6.0);
    std::array<double, 6> mI = {m, m, m, 0, 0, 0};  // identity matrix multiplied by m

    std::array<double, 6> B = am.mulScalar(am.sub(I, mI), 1.0 / p);

    double detB =
        B[0] * B[1] * B[2] + 2 * B[3] * B[4] * B[5] - B[0] * B[4] * B[4] - B[1] * B[5] * B[5] - B[2] * B[3] * B[3];
    double r = detB / 2;

    if (r <= -1) {
      phi = pi / 3;
    } else if (r >= 1) {
      phi = 0;
    } else {
      phi = acos(r) / 3;
    }

    eig0 = m + 2 * p * cos(phi);
    eig1 = m + 2 * p * cos(phi + 2.0 * pi / 3);
    eig2 = trace - eig0 - eig1;  // since the sum of all eigenvalues equals the trace
  }

  return {eig0, eig1, eig2};
}

/*
 * computes the eigenvectors of a given symmetric 3*3 matrix knowing its eigenvalues
 * @param eig eigenvalues of the matrix
 * @param A considered matrix of the form: (A[0] A[3] A[5], A[3] A[1] A[4], A[5] A[4] A[2])
 * @return the matrix containing the eigenvalues in the form (0 3 6
 *                                                            1 4 7
 *                                                            2 5 8)
 */
inline std::array<double, 9> getEigenVectors(std::array<double, 3> eig, std::array<double, 6> A) {
  array<double, 3> eigvec0;
  array<double, 3> eigvec1;
  array<double, 3> eigvec2;

  array<double, 6> identity = {1, 1, 1, 0, 0, 0};

  // A0 corresponds to the difference between the initial matrix A and the identity matrix scaled to the first
  // eigenvalue A1 and A2 are build similarly
  array<double, 6> A0 = am.sub(A, am.mulScalar(identity, eig[0]));
  array<double, 6> A1 = am.sub(A, am.mulScalar(identity, eig[1]));
  //	array<double, 6> A2 = am.sub(A, am.mulScalar(identity, eig[2]));

  if (((abs(eig[0] - eig[1]) < 1e-7) and (abs(eig[1] - eig[2]) < 1e-7)) or
      ((abs(A[3]) < 1e-7) and (A[4] < 1e-7) and (abs(A[5]) < 1e-7))) {
    // one eigenvalue with multiplicity 3 or if A is already diagonal
    eigvec0 = {1, 0, 0};
    eigvec1 = {0, 1, 0};
    eigvec2 = {0, 0, 1};

  } else if (abs(eig[0] - eig[1]) < 1e-7) {
    // two different eigenvalues with multiplicity 2 and 1

    array<double, 3> v = {A0[0], A0[3], A0[5]};
    // find one non-zero column of A - lambda*I
    if (am.dot(v, v) < 1e-8) {
      v = {A0[3], A0[1], A0[4]};

      if (am.dot(v, v) < 1e-8) v = {A0[5], A0[4], A0[2]};
    }

    // choose one vector u not parallel to v
    array<double, 3> u = {0, 1, 0};
    if (am.dot(crossProduct(u, v), crossProduct(u, v)) < 1e-8) u = {1, 0, 0};

    eigvec0 = crossProduct(u, v);
    eigvec1 = crossProduct(eigvec0, v);
    eigvec2 = crossProduct(eigvec0, eigvec1);

    // normalize the eigenvectors
    eigvec0 = am.mulScalar(eigvec0, 1.0 / sqrt(am.dot(eigvec0, eigvec0)));
    eigvec1 = am.mulScalar(eigvec1, 1.0 / sqrt(am.dot(eigvec1, eigvec1)));
    eigvec2 = am.mulScalar(eigvec2, 1.0 / sqrt(am.dot(eigvec2, eigvec2)));

  } else if (abs(eig[0] - eig[2]) < 1e-7) {
    // two different eigenvalues with multiplicity 2 and 1

    array<double, 3> v = {A0[0], A0[3], A0[5]};
    // find one non-zero column of A - lambda*I
    if (am.dot(v, v) < 1e-8) {
      v = {A0[3], A0[1], A0[4]};

      if (am.dot(v, v) < 1e-8) v = {A0[5], A0[4], A0[2]};
    }

    // choose one vector u not parallel to v
    array<double, 3> u = {0, 1, 0};
    if (am.dot(crossProduct(u, v), crossProduct(u, v)) < 1e-8) u = {1, 0, 0};

    eigvec0 = crossProduct(u, v);
    eigvec2 = crossProduct(eigvec0, v);
    eigvec1 = crossProduct(eigvec0, eigvec2);

    // normalize the eigenvectors
    eigvec0 = am.mulScalar(eigvec0, 1.0 / sqrt(am.dot(eigvec0, eigvec0)));
    eigvec1 = am.mulScalar(eigvec1, 1.0 / sqrt(am.dot(eigvec1, eigvec1)));
    eigvec2 = am.mulScalar(eigvec2, 1.0 / sqrt(am.dot(eigvec2, eigvec2)));

  } else if (abs(eig[1] - eig[2]) < 1e-7) {
    // two different eigenvalues with multiplicity 2 and 1

    array<double, 3> v = {A1[0], A1[3], A1[5]};
    // find one non-zero column of A - lambda*I
    if (am.dot(v, v) < 1e-8) {
      v = {A1[3], A1[1], A1[4]};

      if (am.dot(v, v) < 1e-8) {
        v = {A1[5], A1[4], A1[2]};
      }
    }

    // choose one vector u not parallel to v
    array<double, 3> u = {0, 1, 0};
    if (am.dot(crossProduct(u, v), crossProduct(u, v)) < 1e-8) u = {1, 0, 0};

    eigvec1 = crossProduct(u, v);
    eigvec2 = crossProduct(eigvec1, v);
    eigvec0 = crossProduct(eigvec1, eigvec2);

    // normalize the eigenvectors
    eigvec0 = am.mulScalar(eigvec0, 1.0 / sqrt(am.dot(eigvec0, eigvec0)));
    eigvec1 = am.mulScalar(eigvec1, 1.0 / sqrt(am.dot(eigvec1, eigvec1)));
    eigvec2 = am.mulScalar(eigvec2, 1.0 / sqrt(am.dot(eigvec2, eigvec2)));

  } else {
    // 3 different eigenvectors of multiplicity 1 each

    // first eigenvector
    array<double, 3> v = {A0[0], A0[3], A0[5]};  // first column of A0
    array<double, 3> u = {A0[3], A0[1], A0[4]};  // second column of A0

    // check if the two vectors are linearly independent
    if (am.dot(crossProduct(u, v), crossProduct(u, v)) < 1e-8)
      u = {A0[5], A0[4], A0[2]};  // if u and v are parallel, set u to the third column of A0

    eigvec0 = crossProduct(u, v);

    // second eigenvector
    v = {A1[0], A1[3], A1[5]};
    u = {A1[3], A1[1], A1[4]};

    // check if the two vectors are linearly independent
    if (am.dot(crossProduct(u, v), crossProduct(u, v)) < 1e-8) u = {A1[5], A1[4], A1[2]};

    eigvec1 = crossProduct(u, v);

    // third eigenvector
    eigvec2 = crossProduct(eigvec0, eigvec1);

    // normalize the eigenvectors
    eigvec0 = am.mulScalar(eigvec0, 1.0 / sqrt(am.dot(eigvec0, eigvec0)));
    eigvec1 = am.mulScalar(eigvec1, 1.0 / sqrt(am.dot(eigvec1, eigvec1)));
    eigvec2 = am.mulScalar(eigvec2, 1.0 / sqrt(am.dot(eigvec2, eigvec2)));
  }

  return {eigvec0[0], eigvec0[1], eigvec0[2], eigvec1[0], eigvec1[1], eigvec1[2], eigvec2[0], eigvec2[1], eigvec2[2]};
}

#endif /* MOLSIM_SRC_EIGENVALUES_H_ */
