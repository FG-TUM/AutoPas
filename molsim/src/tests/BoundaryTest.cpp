/*
 * LinkedContainerTest.cpp
 *
 *  Created on: Dec 04, 2017
 *      Author: jan
 */

#include "BoundaryTest.h"
#include <iostream>
#include "../UpdateData.h"

using namespace autopas;

const double CutOff = 5.0;

BoundaryTest::BoundaryTest() {
  // TODO Auto-generated constructor stub
}

BoundaryTest::~BoundaryTest() {
  // TODO Auto-generated destructor stub
}
void BoundaryTest::setUp() {
  double mass = 1.0;
  double sigma = 1.0;
  double epsilon = 3.0;
  double charge = 0;
  bool fixed = false;
  ParticleType::setType(0, mass, epsilon, sigma, charge, CutOff, false);
  ParticleType::setType(1, mass, epsilon, sigma, charge, CutOff, false);
}

void BoundaryTest::applyPBCTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();

  AutoPasLogger->info("Start Tests: ", 0);

  setUp();

  std::array<int, 6> BoundCond = {2, 2, 2, 2, 2, 2};
  std::array<double, 3> BoxMin = {0.0, 0.0, 0.0};
  std::array<double, 3> BoxMax = {100.0, 100.0, 100.0};
  AutoPasObject->init(BoxMax, CutOff, linkedCells);
  MoleculeMS testPart1(0);
  testPart1.getR()[0] = 0.5;
  testPart1.getR()[1] = 50;
  testPart1.getR()[2] = 50;
  testPart1.setID(0);
  AutoPasObject->addParticle(testPart1);

  MoleculeMS testPart2(0);
  testPart2.getR()[0] = 99.5;
  testPart2.getR()[1] = 50;
  testPart2.getR()[2] = 50;
  testPart2.setID(1);
  AutoPasObject->addParticle(testPart2);

  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);

  applyPeriodicBC(AutoPasObject, BoundCond, BoxMin, BoxMax, CutOff);
  AutoPasObject->iteratePairwise(&fLJ, aos);
  AutoPasObject->getContainer()->deleteHaloParticles();

  auto it = AutoPasObject->begin();
  MoleculeMS p1 = *it;
  MoleculeMS p2 = *(++it);

  std::array<double, 3> deltaF = am.add(p1.getF(), p2.getF());

  CPPUNIT_ASSERT(am.dot(p1.getF(), p1.getF()) > 0);  // tsst whether the force in one particle is not zero
  CPPUNIT_ASSERT(am.dot(deltaF, deltaF) < 1e-12);    // test whether the force difference is next to zero
  CPPUNIT_ASSERT(abs(p1.getF()[1]) < 1e-12);         // test whether the other directions remain unaffected
  CPPUNIT_ASSERT(p1.getF()[0] * p2.getF()[0] < 0);   // test whether the force of both particles have inverse sign
}
void BoundaryTest::applyEdgePBCTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();

  std::array<int, 6> BoundCond = {2, 2, 2, 2, 2, 2};
  std::array<double, 3> BoxMin = {0.0, 0.0, 0.0};
  std::array<double, 3> BoxMax = {100.0, 100.0, 100.0};
  AutoPasObject->init(BoxMax, CutOff, linkedCells);

  // test for particles at an edge
  MoleculeMS testPart1(0);
  testPart1.getR()[0] = 0.5;
  testPart1.getR()[1] = 0.5;
  testPart1.getR()[2] = 50;
  testPart1.setID(0);
  AutoPasObject->addParticle(testPart1);

  MoleculeMS testPart2(0);
  testPart2.getR()[0] = 99.5;
  testPart2.getR()[1] = 99.5;
  testPart2.getR()[2] = 50;
  testPart2.setID(1);
  AutoPasObject->addParticle(testPart2);

  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);

  applyPeriodicBC(AutoPasObject, BoundCond, BoxMin, BoxMax, CutOff);
  AutoPasObject->iteratePairwise(&fLJ, aos);
  AutoPasObject->getContainer()->deleteHaloParticles();

  auto it = AutoPasObject->begin();
  MoleculeMS p1 = *it;
  MoleculeMS p2 = *(++it);
}

void BoundaryTest::applyCornerPBCTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();

  std::array<int, 6> BoundCond = {2, 2, 2, 2, 2, 2};
  std::array<double, 3> BoxMin = {0.0, 0.0, 0.0};
  std::array<double, 3> BoxMax = {100.0, 100.0, 100.0};
  AutoPasObject->init(BoxMax, CutOff, linkedCells);

  // test for particles at a corner
  MoleculeMS testPart3(0);
  testPart3.getR()[0] = 0.5;
  testPart3.getR()[1] = 0.5;
  testPart3.getR()[2] = 0.5;
  testPart3.setID(2);
  AutoPasObject->addParticle(testPart3);

  MoleculeMS testPart4(0);
  testPart4.getR()[0] = 99.5;
  testPart4.getR()[1] = 99.5;
  testPart4.getR()[2] = 99.5;
  testPart4.setID(3);
  AutoPasObject->addParticle(testPart4);

  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);

  applyPeriodicBC(AutoPasObject, BoundCond, BoxMin, BoxMax, CutOff);
  AutoPasObject->iteratePairwise(&fLJ, aos);
  AutoPasObject->getContainer()->deleteHaloParticles();

  auto it = AutoPasObject->begin();
  MoleculeMS p1 = *it;
  MoleculeMS p2 = *(++it);

  std::array<double, 3> deltaF = am.add(p1.getF(), p2.getF());

  CPPUNIT_ASSERT(am.dot(deltaF, deltaF) < 1e-12);  // test whether the force difference is next to zero
}

void BoundaryTest::computePBCTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();

  std::array<int, 6> BoundCond = {2, 2, 2, 2, 2, 2};
  std::array<double, 3> BoxMin = {0.0, 0.0, 0.0};
  std::array<double, 3> BoxMax = {100.0, 100.0, 100.0};
  AutoPasObject->init(BoxMax, CutOff, linkedCells);
  MoleculeMS testPart1(0);
  testPart1.getR()[0] = 0.5;
  testPart1.getR()[1] = 50;
  testPart1.getR()[2] = 50;
  testPart1.setID(0);
  AutoPasObject->addParticle(testPart1);

  MoleculeMS testPart2(0);
  testPart2.getR()[0] = 99.5;
  testPart2.getR()[1] = 50;
  testPart2.getR()[2] = 50;
  testPart2.setID(1);
  AutoPasObject->addParticle(testPart2);

  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);

  computePeriodicBC(AutoPasObject, &fLJ, BoundCond, BoxMin, BoxMax, CutOff);

  auto it = AutoPasObject->begin();
  MoleculeMS p1 = *it;
  MoleculeMS p2 = *(++it);

  std::array<double, 3> deltaF = am.add(p1.getF(), p2.getF());

  CPPUNIT_ASSERT(am.dot(p1.getF(), p1.getF()) > 0);  // tsst whether the force in one particle is not zero
  CPPUNIT_ASSERT(am.dot(deltaF, deltaF) < 1e-12);    // test whether the force difference is next to zero
  CPPUNIT_ASSERT(abs(p1.getF()[1]) < 1e-12);         // test whether the other directions remain unaffected
  CPPUNIT_ASSERT(p1.getF()[0] * p2.getF()[0] < 0);   // test whether the force of both particles have inverse sign
}
void BoundaryTest::computeEdgePBCTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();

  std::array<int, 6> BoundCond = {2, 2, 2, 2, 2, 2};
  std::array<double, 3> BoxMin = {0.0, 0.0, 0.0};
  std::array<double, 3> BoxMax = {100.0, 100.0, 100.0};
  AutoPasObject->init(BoxMax, CutOff, linkedCells);

  // test for particles at an edge
  MoleculeMS testPart1(0);
  testPart1.getR()[0] = 0.5;
  testPart1.getR()[1] = 0.5;
  testPart1.getR()[2] = 50;
  testPart1.setID(0);
  AutoPasObject->addParticle(testPart1);

  MoleculeMS testPart2(0);
  testPart2.getR()[0] = 99.5;
  testPart2.getR()[1] = 99.5;
  testPart2.getR()[2] = 50;
  testPart2.setID(1);
  AutoPasObject->addParticle(testPart2);

  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);

  computePeriodicBC(AutoPasObject, &fLJ, BoundCond, BoxMin, BoxMax, CutOff);

  auto it = AutoPasObject->begin();
  MoleculeMS p1 = *it;
  MoleculeMS p2 = *(++it);

  std::array<double, 3> deltaF = am.add(p1.getF(), p2.getF());

  CPPUNIT_ASSERT(am.dot(deltaF, deltaF) < 1e-12);  // test whether the force difference is next to zero
}

void BoundaryTest::computeCornerPBCTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();

  std::array<int, 6> BoundCond = {2, 2, 2, 2, 2, 2};
  std::array<double, 3> BoxMin = {0.0, 0.0, 0.0};
  std::array<double, 3> BoxMax = {100.0, 100.0, 100.0};
  AutoPasObject->init(BoxMax, CutOff, linkedCells);

  // test for particles at a corner
  MoleculeMS testPart3(0);
  testPart3.getR()[0] = 0.5;
  testPart3.getR()[1] = 0.5;
  testPart3.getR()[2] = 0.5;
  testPart3.setID(2);
  AutoPasObject->addParticle(testPart3);

  MoleculeMS testPart4(0);
  testPart4.getR()[0] = 99.5;
  testPart4.getR()[1] = 99.5;
  testPart4.getR()[2] = 99.5;
  testPart4.setID(3);
  AutoPasObject->addParticle(testPart4);

  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);

  computePeriodicBC(AutoPasObject, &fLJ, BoundCond, BoxMin, BoxMax, CutOff);

  auto it = AutoPasObject->begin();
  MoleculeMS p1 = *it;
  MoleculeMS p2 = *(++it);

  std::array<double, 3> deltaF = am.add(p1.getF(), p2.getF());

  CPPUNIT_ASSERT(am.dot(deltaF, deltaF) < 1e-12);  // test whether the force difference is next to zero
}

void BoundaryTest::crossPBCTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();

  // create one particle that travels from position {1, 50, 50} to position {99, 50, 50} in 200 iterations
  std::array<int, 6> BoundCond = {2, 2, 2, 2, 2, 2};
  std::array<double, 3> BoxMin = {0.0, 0.0, 0.0};
  std::array<double, 3> BoxMax = {100.0, 100.0, 100.0};

  AutoPasObject->init(BoxMax, CutOff, linkedCells);
  MoleculeMS testPart(0);
  testPart.getR()[0] = 1;
  testPart.getR()[1] = 50;
  testPart.getR()[2] = 50;
  testPart.getV()[0] = -1;
  testPart.setID(0);
  AutoPasObject->addParticle(testPart);

  double delta_t = 0.01;
  for (double time = 0; time <= 2; time += delta_t) {
    calculateR(&testPart, BoundCond, BoxMin, BoxMax, delta_t);
  }
  MoleculeMS p = testPart;

  // Particle should have arrived at position {99, 50, 50}
  CPPUNIT_ASSERT(abs(p.getR()[0] - 99.0) < 1e-12);
  CPPUNIT_ASSERT(abs(p.getR()[1] - 50.0) < 1e-12);
  CPPUNIT_ASSERT(abs(p.getR()[2] - 50.0) < 1e-12);
}

void BoundaryTest::crossEdgePBCTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();

  // create one particle that travels from position {1, 1, 50} to position {99, 99, 50} in 200 iterations
  std::array<int, 6> BoundCond = {2, 2, 2, 2, 2, 2};
  std::array<double, 3> BoxMin = {0.0, 0.0, 0.0};
  std::array<double, 3> BoxMax = {100.0, 100.0, 100.0};

  AutoPasObject->init(BoxMax, CutOff, linkedCells);
  MoleculeMS testPart(0);
  testPart.getR()[0] = 1;
  testPart.getR()[1] = 1;
  testPart.getR()[2] = 50;
  testPart.getV()[0] = -1;
  testPart.getV()[1] = -1;
  testPart.setID(0);
  AutoPasObject->addParticle(testPart);

  // LOOP
  double delta_t = 0.01;
  for (double time = 0; time <= 2; time += delta_t) {
    calculateR(&testPart, BoundCond, BoxMin, BoxMax, delta_t);
  }
}
void BoundaryTest::crossCornerPBCTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();

  std::array<int, 6> BoundCond = {2, 2, 2, 2, 2, 2};
  std::array<double, 3> BoxMin = {0.0, 0.0, 0.0};
  std::array<double, 3> BoxMax = {100.0, 100.0, 100.0};

  AutoPasObject->init(BoxMax, CutOff, linkedCells);

  // create one particle that travels from position {1, 1, 1} to position {99, 99, 99} in 200 iterations
  MoleculeMS testPart2(0);
  testPart2.getR()[0] = 1;
  testPart2.getR()[1] = 1;
  testPart2.getR()[2] = 1;
  testPart2.getV()[0] = -1;
  testPart2.getV()[1] = -1;
  testPart2.getV()[2] = -1;
  testPart2.setID(1);
  AutoPasObject->addParticle(testPart2);

  double delta_t = 0.01;
  for (double time = 0; time <= 2; time += delta_t) {
    for (auto trt = AutoPasObject->begin(); trt.isValid(); ++trt) {
      calculateR(&*trt, BoundCond, BoxMin, BoxMax, delta_t);
    }
  }
  MoleculeMS p = *(AutoPasObject->begin());

  // Particle should have arrived at position {99, 99, 99}
  CPPUNIT_ASSERT(abs(p.getR()[0] - 99.0) < 1e-12);
  CPPUNIT_ASSERT(abs(p.getR()[1] - 99.0) < 1e-12);
  CPPUNIT_ASSERT(abs(p.getR()[2] - 99.0) < 1e-12);
}

void BoundaryTest::orthogonalRBCTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();

  // create one particle that travels from position {1, 1, 50} to position {99, 99, 50} in 200 iterations
  std::array<int, 6> BoundCond = {1, 1, 1, 1, 1, 1};
  std::array<double, 3> BoxMin = {0.0, 0.0, 0.0};
  std::array<double, 3> BoxMax = {100.0, 100.0, 100.0};

  AutoPasObject->init(BoxMax, CutOff, linkedCells);
  MoleculeMS testPart(0);
  testPart.getR()[0] = 1;
  testPart.getR()[1] = 50;
  testPart.getR()[2] = 50;
  testPart.getV()[0] = -1;
  testPart.setID(0);
  AutoPasObject->addParticle(testPart);

  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);

  double delta_t = 0.0001;
  for (double time = 0; time < 1; time += delta_t) {
    resetF(AutoPasObject);
    applyReflectiveBC(AutoPasObject, &fLJ, BoundCond, BoxMin, BoxMax, CutOff);
    calculateR(&testPart, BoundCond, BoxMin, BoxMax, delta_t);
    calculateV(&testPart, delta_t);
  }
  MoleculeMS p = testPart;
  CPPUNIT_ASSERT(abs(p.getV()[0] - 1) <
                 1e-2);  // test whether the velocity in X-direction is the opposite of the original velocity
  CPPUNIT_ASSERT(abs(p.getR()[1] - 50) < 1e-12);  // check whether the particle remains on the same path
  CPPUNIT_ASSERT(abs(p.getR()[2] - 50) < 1e-12);  // same as above
}

void BoundaryTest::diagonalRBCTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();

  // create one particle that travels from position {1, 1, 50} to position {99, 99, 50} in 200 iterations
  std::array<int, 6> BoundCond = {1, 1, 1, 1, 1, 1};
  std::array<double, 3> BoxMin = {0.0, 0.0, 0.0};
  std::array<double, 3> BoxMax = {100.0, 100.0, 100.0};

  AutoPasObject->init(BoxMax, CutOff, linkedCells);
  MoleculeMS testPart(0);
  testPart.getR()[0] = 1;
  testPart.getR()[1] = 50;
  testPart.getR()[2] = 50;
  testPart.getV()[0] = -1;
  testPart.getV()[1] = -1;
  testPart.getV()[2] = -1;
  testPart.setID(0);
  AutoPasObject->addParticle(testPart);

  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);

  double delta_t = 0.0001;
  for (double time = 0; time < 1; time += delta_t) {
    resetF(AutoPasObject);
    applyReflectiveBC(AutoPasObject, &fLJ, BoundCond, BoxMin, BoxMax, CutOff);
    for (auto trt = AutoPasObject->begin(); trt.isValid(); ++trt) {
      calculateR(&*trt, BoundCond, BoxMin, BoxMax, delta_t);
      calculateV(&*trt, delta_t);
    }
  }
  MoleculeMS p = *AutoPasObject->begin();
  CPPUNIT_ASSERT(abs(p.getV()[0] - 1) <
                 1e-2);  // test whether the velocity in X-direction is the opposite of the original velocity
  CPPUNIT_ASSERT(abs(p.getV()[2] + 1) < 5e-12);  // test whether the velocity in Y-direction remains the same
  CPPUNIT_ASSERT(abs(p.getV()[2] + 1) < 5e-12);  // test whether the velocity in Z-direction remains the same

  // check whether the particle remains on the same path in the Y/Z plane
  CPPUNIT_ASSERT(abs((p.getR()[1] - 50) - (p.getR()[2] - 50)) < 1e-12);
}
