/*
 * MoleculeTest.h
 *
 *  Created on: May 27, 2018
 *      Author: raffi
 */

#ifndef SRC_MOLECULETEST_H_
#define SRC_MOLECULETEST_H_

#include <cppunit/TestResult.h>
#include <cppunit/extensions/HelperMacros.h>
#include "../HelperFunctions.h"
#include "../MoleculeMS.h"
#include "../MoleculeType.h"
#include "../ParticleType.h"

#include <cmath>
#include "autopas/AutoPas.h"
#include "autopas/autopasIncludes.h"

/**
 * @brief TestSuite for LinkedContainer
 */
class MoleculeTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(MoleculeTest);
  CPPUNIT_TEST(addParticleTest);
  CPPUNIT_TEST(addMultipleParticleTest);
  CPPUNIT_TEST(RotationXAxisTest);
  CPPUNIT_TEST(RotationYAxisTest);
  CPPUNIT_TEST(RotationZAxisTest);
  CPPUNIT_TEST(DipoleSymmetricTest);
  CPPUNIT_TEST(DipoleAsymmetricTest);
  CPPUNIT_TEST(DipoleOrientedTest);
  CPPUNIT_TEST_SUITE_END();

 public:
  void setUp() override;
  /**
   * Tests if a particle is correctly added to the molecule
   */
  void addParticleTest();

  /**
   * Tests if two particles are correctly added to the molecule
   */
  void addMultipleParticleTest();

  /**
   * Tests if the positions are correctly computed after a rotation along the Y-axis
   */
  void RotationYAxisTest();
  /**
   * Tests if the positions are correctly computed after a rotation along the X-axis
   */
  void RotationXAxisTest();
  /**
   * Tests if the positions are correctly computed after a rotation along the Z-axis
   */
  void RotationZAxisTest();
  /**
   * Tests if dipoles are correctly setted for a symmetric molecule
   */
  void DipoleSymmetricTest();
  /**
   * Tests if dipoles are correctly setted for an asymmetric molecule
   */
  void DipoleAsymmetricTest();
  /**
   * Tests if dipoles are rotated setted for an asymmetric molecule
   */
  void DipoleOrientedTest();

  MoleculeTest();
  virtual ~MoleculeTest();
};
#endif /* SRC_BOUNDARYTEST_H_ */
