/*
 * LJFunctorTest.h
 *
 *  Created on: May 23, 2018
 *      Author: raffi
 */

#ifndef SRC_LJFUNCTORTEST_H_
#define SRC_LJFUNCTORTEST_H_

#include <cppunit/TestResult.h>
#include <cppunit/extensions/HelperMacros.h>
#include "../MolSimFunctors.h"

#include <cmath>
#include "autopas/AutoPas.h"
#include "autopas/autopasIncludes.h"

/**
 * @brief TestSuite for LinkedContainer
 */
class LJFunctorTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(LJFunctorTest);
  CPPUNIT_TEST(AttractionForceTest);
  CPPUNIT_TEST(ZeroForceTest);
  CPPUNIT_TEST(RepulsionForceTest);
  CPPUNIT_TEST(OutOfCutOffRadiusTest);
  CPPUNIT_TEST(AttractionForceDiagonalTest);
  CPPUNIT_TEST(ZeroForceDiagonalTest);
  CPPUNIT_TEST(RepulsionForceDiagonalTest);
  CPPUNIT_TEST(OutOfCutOffRadiusDiagonalTest);
  CPPUNIT_TEST_SUITE_END();

 public:
  void setUp() override;
  /**
   * Tests if the forces are correctly computed in the case of attraction between two particles aligned along the X-axis
   */
  void AttractionForceTest();

  /**
   * Tests if the forces are correctly for two particles at the no-force-distance aligned along the X-axis
   */
  void ZeroForceTest();

  /**
   * Tests if the forces are correctly computed in the case of repulsion between two particles aligned along the X-axis
   */
  void RepulsionForceTest();

  /**
   * Tests if no forces are computed in the case of two particles further away than the cut-off radius aligned along the
   * X-axis
   */
  void OutOfCutOffRadiusTest();

  /**
   * Tests if the forces are correctly computed in the case of attraction between two particles aligned along the line
   * parameterized by {t, t, t}, t real
   */
  void AttractionForceDiagonalTest();

  /**
   * Tests if the forces are correctly for two particles at the no-force-distance aligned along the line parameterized
   * by {t, t, t}, t real
   */
  void ZeroForceDiagonalTest();

  /**
   * Tests if the forces are correctly computed in the case of repulsion between two particles aligned along the line
   * parameterized by {t, t, t}, t real
   */
  void RepulsionForceDiagonalTest();

  /**
   * Tests if no forces are computed in the case of two particles further away than the cut-off radius aligned along the
   * line parameterized by {t, t, t}, t real
   */
  void OutOfCutOffRadiusDiagonalTest();
};

#endif /* MOLSIM_SRC_LJFUNCTORTEST_H_ */
