/*
 * LJFunctorTest.cpp
 *
 *  Created on: May 23, 2018
 *      Author: raffi
 */

#include "LJFunctorTest.h"

using namespace autopas;
using namespace std;

const double CutOff = 5.0;

void LJFunctorTest::setUp() {
  double mass = 1.0;
  double sigma = 1.0;
  double charge = 0;
  double epsilon = 3.0;
  bool fixed = false;
  ParticleType::setType(0, mass, epsilon, sigma, charge, CutOff, false);
  ParticleType::setType(1, mass, epsilon, sigma, charge, CutOff, false);
}

void LJFunctorTest::AttractionForceTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // we need an AutoPas instance to avoid a segmentation
                                                                // fault caused by the logger

  setUp();

  // set two particles
  MoleculeMS p1(0);
  p1.getR()[0] = 1.0;
  p1.getR()[1] = 1.0;
  p1.getR()[2] = 1.0;
  p1.setID(1);

  MoleculeMS p2(0);
  p2.getR()[0] = 3.0;
  p2.getR()[1] = 1.0;
  p2.getR()[2] = 1.0;
  p2.setID(2);

  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);

  // those two particles should attract themselves as their relative distance is 2, which is greater than the zero-force
  // distance for two particles with sigma = 1

  fLJ.AoSFunctor(p1, p2, true);

  CPPUNIT_ASSERT(am.dot(am.add(p1.getF(), p2.getF()), am.add(p1.getF(), p2.getF())) <
                 1e-12);  // check whether the forces show in opposite directions

  // check the three components of the force
  CPPUNIT_ASSERT(abs(p1.getF()[0] - 0.544921875) < 1e-12);
  CPPUNIT_ASSERT(abs(p1.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs(p1.getF()[2]) < 1e-12);
}

void LJFunctorTest::ZeroForceTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // we need an AutoPas instance to avoid a segmentation
                                                                // fault caused by the logger
  // set two particles
  MoleculeMS p1(0);
  p1.getR()[0] = 1.0;
  p1.getR()[1] = 1.0;
  p1.getR()[2] = 1.0;
  p1.setID(1);

  MoleculeMS p2(0);
  p2.getR()[0] = 1.0 + ZeroForceFactor;
  p2.getR()[1] = 1.0;
  p2.getR()[2] = 1.0;
  p2.setID(2);

  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);

  // those two particles should not interact as their relative is equal to the zero-force distance for two particles
  // with sigma = 1

  fLJ.AoSFunctor(p1, p2, true);

  CPPUNIT_ASSERT(am.dot(am.add(p1.getF(), p2.getF()), am.add(p1.getF(), p2.getF())) <
                 1e-12);  // check whether the forces show in opposite directions

  // check the three components of the force
  CPPUNIT_ASSERT(abs(p1.getF()[0]) < 1e-10);
  CPPUNIT_ASSERT(abs(p1.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs(p1.getF()[2]) < 1e-12);
}

void LJFunctorTest::RepulsionForceTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // we need an AutoPas instance to avoid a segmentation
                                                                // fault caused by the logger

  setUp();

  // set two particles
  MoleculeMS p1(0);
  p1.getR()[0] = 1.0;
  p1.getR()[1] = 1.0;
  p1.getR()[2] = 1.0;
  p1.setID(1);

  MoleculeMS p2(0);
  p2.getR()[0] = 2.0;
  p2.getR()[1] = 1.0;
  p2.getR()[2] = 1.0;
  p2.setID(2);

  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);

  // those two particles should attract themselves as their relative distance is 1, which is smaller than the zero-force
  // distance for two particles with sigma = 1

  fLJ.AoSFunctor(p1, p2, true);

  CPPUNIT_ASSERT(am.dot(am.add(p1.getF(), p2.getF()), am.add(p1.getF(), p2.getF())) <
                 1e-12);  // check whether the forces show in opposite directions

  // check the three components of the force
  CPPUNIT_ASSERT(abs(p1.getF()[0] + 72.0) < 1e-12);
  CPPUNIT_ASSERT(abs(p1.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs(p1.getF()[2]) < 1e-12);
}

void LJFunctorTest::OutOfCutOffRadiusTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // we need an AutoPas instance to avoid a segmentation
                                                                // fault caused by the logger

  setUp();

  // set two particles
  MoleculeMS p1(0);
  p1.getR()[0] = 1.0;
  p1.getR()[1] = 1.0;
  p1.getR()[2] = 1.0;
  p1.setID(1);

  MoleculeMS p2(0);
  p2.getR()[0] = 7.0;
  p2.getR()[1] = 1.0;
  p2.getR()[2] = 1.0;
  p2.setID(2);

  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);

  // those two particles should not interact as their relative distance is 6, which is greater than the cut-off distance
  fLJ.AoSFunctor(p1, p2, true);

  CPPUNIT_ASSERT(am.dot(am.add(p1.getF(), p2.getF()), am.add(p1.getF(), p2.getF())) <
                 1e-12);  // check whether the forces show in opposite directions

  // check the three components of the force
  CPPUNIT_ASSERT(abs(p1.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs(p1.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs(p1.getF()[2]) < 1e-12);
}

void LJFunctorTest::AttractionForceDiagonalTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // we need an AutoPas instance to avoid a segmentation
                                                                // fault caused by the logger

  setUp();

  // set two particles
  MoleculeMS p1(0);
  p1.getR()[0] = 1.0;
  p1.getR()[1] = 1.0;
  p1.getR()[2] = 1.0;
  p1.setID(1);

  MoleculeMS p2(0);
  p2.getR()[0] = 3.0;
  p2.getR()[1] = 2.5;
  p2.getR()[2] = 2.0;
  p2.setID(2);

  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);

  // those two particles should attract themselves as their relative distance is sqrt(7.25), which is greater than the
  // zero-force distance for two particles with sigma = 1

  fLJ.AoSFunctor(p1, p2, true);

  CPPUNIT_ASSERT(am.dot(am.add(p1.getF(), p2.getF()), am.add(p1.getF(), p2.getF())) <
                 1e-12);  // check whether the forces show in opposite directions

  // check the three components of the force
  CPPUNIT_ASSERT(abs(p1.getF()[0] - 0.051847183595) < 1e-12);
  CPPUNIT_ASSERT(abs(p1.getF()[1] - 0.038885387697) < 1e-12);
  CPPUNIT_ASSERT(abs(p1.getF()[2] - 0.025923591797) < 1e-12);
}

void LJFunctorTest::ZeroForceDiagonalTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // we need an AutoPas instance to avoid a segmentation
                                                                // fault caused by the logger
  // set two particles
  MoleculeMS p1(0);
  p1.getR()[0] = 1.0;
  p1.getR()[1] = 1.0;
  p1.getR()[2] = 1.0;
  p1.setID(1);

  MoleculeMS p2(0);
  p2.getR()[0] = 1.0 + ZeroForceFactor / sqrt(6.0);
  p2.getR()[1] = 1.0 + ZeroForceFactor / sqrt(6.0);
  p2.getR()[2] = 1.0 + ZeroForceFactor / sqrt(3.0 / 2.0);
  p2.setID(2);

  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);

  // those two particles should not interact as their relative is equal to the zero-force distance for two particles
  // with sigma = 1

  fLJ.AoSFunctor(p1, p2, true);

  CPPUNIT_ASSERT(am.dot(am.add(p1.getF(), p2.getF()), am.add(p1.getF(), p2.getF())) <
                 1e-12);  // check whether the forces show in opposite directions

  // check the three components of the force
  CPPUNIT_ASSERT(abs(p1.getF()[0]) < 1e-10);
  CPPUNIT_ASSERT(abs(p1.getF()[1]) < 1e-10);
  CPPUNIT_ASSERT(abs(p1.getF()[2]) < 1e-10);
}

void LJFunctorTest::RepulsionForceDiagonalTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // we need an AutoPas instance to avoid a segmentation
                                                                // fault caused by the logger

  setUp();

  // set two particles
  MoleculeMS p1(0);
  p1.getR()[0] = 1.0;
  p1.getR()[1] = 1.0;
  p1.getR()[2] = 1.0;
  p1.setID(1);

  MoleculeMS p2(0);
  p2.getR()[0] = 1.7;
  p2.getR()[1] = 1.5;
  p2.getR()[2] = 1.5;
  p2.setID(2);

  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);

  // those two particles should attract themselves as their relative distance is 0.99, which is smaller than the
  // zero-force distance for two particles with sigma = 1

  fLJ.AoSFunctor(p1, p2, true);

  CPPUNIT_ASSERT(am.dot(am.add(p1.getF(), p2.getF()), am.add(p1.getF(), p2.getF())) <
                 1e-12);  // check whether the forces show in opposite directions

  // check the three components of the force
  CPPUNIT_ASSERT(abs(p1.getF()[0] + 55.6794977053) < 1e-10);
  CPPUNIT_ASSERT(abs(p1.getF()[1] + 39.7710697895) < 1e-10);
  CPPUNIT_ASSERT(abs(p1.getF()[2] + 39.7710697895) < 1e-10);
}

void LJFunctorTest::OutOfCutOffRadiusDiagonalTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // we need an AutoPas instance to avoid a segmentation
                                                                // fault caused by the logger

  setUp();

  // set two particles
  MoleculeMS p1(0);
  p1.getR()[0] = 1.0;
  p1.getR()[1] = 1.0;
  p1.getR()[2] = 1.0;
  p1.setID(1);

  MoleculeMS p2(0);
  p2.getR()[0] = 4.0;
  p2.getR()[1] = 5.0;
  p2.getR()[2] = 6.0;
  p2.setID(2);

  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);

  // those two particles should not interact as their relative distance is 5*sqrt(2), which is greater than the cut-off
  // distance
  fLJ.AoSFunctor(p1, p2, true);

  CPPUNIT_ASSERT(am.dot(am.add(p1.getF(), p2.getF()), am.add(p1.getF(), p2.getF())) <
                 1e-12);  // check whether the forces show in opposite directions

  // check the three components of the force
  CPPUNIT_ASSERT(abs(p1.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs(p1.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs(p1.getF()[2]) < 1e-12);
}
