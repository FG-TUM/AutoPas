/*
 * ParticleContainerTest.h
 *
 *  Created on: Nov 13, 2017
 *      Author: raffi
 */

#ifndef SRC_BOUNDARYTEST_H_
#define SRC_BOUNDARYTEST_H_

#include <cppunit/TestResult.h>
#include <cppunit/extensions/HelperMacros.h>
#include "../HelperFunctions.h"
#include "../MolSimFunctors.h"
#include "../MoleculeMS.h"

#include <cmath>
#include "autopas/AutoPas.h"
#include "autopas/autopasIncludes.h"

/**
 * @brief TestSuite for LinkedContainer
 */
class BoundaryTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(BoundaryTest);
  CPPUNIT_TEST(applyPBCTest);
  CPPUNIT_TEST(applyEdgePBCTest);
  CPPUNIT_TEST(applyCornerPBCTest);
  CPPUNIT_TEST(computePBCTest);
  CPPUNIT_TEST(computeEdgePBCTest);
  CPPUNIT_TEST(computeCornerPBCTest);
  CPPUNIT_TEST(crossPBCTest);
  CPPUNIT_TEST(crossEdgePBCTest);
  CPPUNIT_TEST(crossCornerPBCTest);
  CPPUNIT_TEST(orthogonalRBCTest);
  CPPUNIT_TEST(diagonalRBCTest);
  CPPUNIT_TEST_SUITE_END();

 public:
  void setUp() override;
  /**
   * Tests if the forces are correctly transmitted through the boundary
   */
  void applyPBCTest();

  /**
   * Tests if the forces are correctly transmitted through the boundary at the edges
   */
  void applyEdgePBCTest();

  /**
   * Tests if the forces are correctly transmitted through the boundary at the corners
   */
  void applyCornerPBCTest();

  /**
   * identical as applyPBCTest, but using the direct computation method (no pairwise iteration)
   */
  void computePBCTest();

  /**
   * identical as applyEdgePBCTest, but using the direct computation method (no pairwise iteration)
   */
  void computeEdgePBCTest();

  /**
   * identical as applyCornerPBCTest, but using the direct computation method (no pairwise iteration)
   */
  void computeCornerPBCTest();

  /**
   * Tests whether a particle correctly crosses the boundary or not
   */
  void crossPBCTest();

  /**
   * Tests whether a particle correctly crosses the boundary at edges or not
   */
  void crossEdgePBCTest();

  /**
   * Tests whether a particle correctly crosses the boundary at corners or not
   */
  void crossCornerPBCTest();

  /**
   * Tests whether a particle arriving orthogonally to a reflective boundary is correctly reflected
   */
  void orthogonalRBCTest();
  /**
   * Tests whether a particle arriving diagonally to a reflective boundary is correctly reflected
   */
  void diagonalRBCTest();

  BoundaryTest();
  virtual ~BoundaryTest();
};
#endif /* SRC_BOUNDARYTEST_H_ */
