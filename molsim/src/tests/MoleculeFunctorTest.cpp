/*
 * MoleculeFunctorTest.cpp
 *
 *  Created on: May 27, 2018
 *      Author: raffi
 */

#include "MoleculeFunctorTest.h"
#include <iostream>
#include "../UpdateData.h"

using namespace autopas;

const double mass1 = 1.0;
const double sigma1 = 3.0;
const double epsilon1 = 3.0;
const double charge1 = 0;
const double mass2 = 2.0;
const double sigma2 = 2.0;
const double epsilon2 = 5.0;
const double charge2 = 0;
const double mass3 = 3.0;
const double sigma3 = 3.0;
const double epsilon3 = 1.0;
const double charge3 = 1;
const double mass4 = 5.0;
const double sigma4 = 1.0;
const double charge4 = -2.0;
const double epsilon4 = 3.0;
const double CutOff = 5.0;

MoleculeFunctorTest::MoleculeFunctorTest() {
  // TODO Auto-generated constructor stub
}

MoleculeFunctorTest::~MoleculeFunctorTest() {
  // TODO Auto-generated destructor stub
}
void MoleculeFunctorTest::setUp() {
  ParticleType::setType(4, mass1, epsilon1, sigma1, charge1, CutOff, false);
  ParticleType::setType(5, mass2, epsilon2, sigma2, charge2, CutOff, false);
  ParticleType::setType(6, mass3, epsilon3, sigma3, charge3, CutOff, false);
  ParticleType::setType(7, mass4, epsilon4, sigma4, charge4, CutOff, false);
}

void MoleculeFunctorTest::addParticleTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  int ID = 0;

  // create molecule properties
  MoleculeType molprop(ID);

  // create molecule
  MoleculeMS mol1({1, 1, 1}, {0, 0, 0}, ID, 0, &molprop);

  std::array<double, 3> PartPos = {0, 0, 0};
  molprop.addParticle(PartPos, 4, 0);

  CPPUNIT_ASSERT(mol1.getNumPart() == 1);  // test whether there is 1 particle in the molecule

  // test the position of the only particle in the molecule
  CPPUNIT_ASSERT(abs(mol1.getRealPosX()[0] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol1.getRealPosY()[0] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol1.getRealPosZ()[0] - 1.0) < 1e-12);

  // test the particle's properties
  CPPUNIT_ASSERT(abs(mol1.getSigmaPart()[0] - sigma1) < 1e-12);
  CPPUNIT_ASSERT(abs(mol1.getMpart()[0] - mass1) < 1e-12);
  CPPUNIT_ASSERT(abs(mol1.getTypePart()[0] - 4) < 1e-12);
}

void MoleculeFunctorTest::addMultipleParticleTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  int ID = 0;

  // create molecule properties
  MoleculeType molprop(ID);

  // create molecule
  MoleculeMS mol({1, 1, 1}, {0, 0, 0}, ID, 0, &molprop);

  std::array<double, 3> PartPos1 = {0, 0, 0};
  std::array<double, 3> PartPos2 = {0, 1, 1};

  molprop.addParticle(PartPos1, 4, 0);
  molprop.addParticle(PartPos2, 5, 0);

  CPPUNIT_ASSERT(mol.getNumPart() == 2);  // test whether there is 1 particle in the molecule

  // test the position of the first particle in the molecule
  CPPUNIT_ASSERT(abs(mol.getRealPosX()[0] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosY()[0] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosZ()[0] - 1.0) < 1e-12);

  // test the particles' properties
  CPPUNIT_ASSERT(abs(mol.getSigmaPart()[0] - sigma1) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getMpart()[0] - mass1) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getTypePart()[0] - 4) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getSigmaPart()[1] - sigma2) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getMpart()[1] - mass2) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getTypePart()[1] - 5) < 1e-12);

  // test the position of the second particle in the molecule
  CPPUNIT_ASSERT(abs(mol.getRealPosX()[1] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosY()[1] - 2.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosZ()[1] - 2.0) < 1e-12);
}
