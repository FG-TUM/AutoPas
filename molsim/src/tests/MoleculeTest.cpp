/*
 * MoleculeTest.cpp
 *
 *  Created on: May 27, 2018
 *      Author: raffi
 */

#include "MoleculeTest.h"
#include <iostream>

using namespace autopas;

const double mass1 = 1.0;
const double sigma1 = 3.0;
const double charge1 = -1.0;
const double epsilon1 = 3.0;
const double mass2 = 2.0;
const double sigma2 = 2.0;
const double charge2 = 2.0;
const double epsilon2 = 5.0;
const double CutOff = 5.0;

MoleculeTest::MoleculeTest() {
  // TODO Auto-generated constructor stub
}

MoleculeTest::~MoleculeTest() {
  // TODO Auto-generated destructor stub
}
void MoleculeTest::setUp() {
  ParticleType::setType(2, mass1, epsilon1, sigma1, charge1, CutOff, false);
  ParticleType::setType(3, mass2, epsilon2, sigma2, charge2, CutOff, false);
}

void MoleculeTest::addParticleTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  int ID = 0;

  // create molecule properties
  MoleculeType molprop(ID);

  // create molecule
  MoleculeMS mol({1, 1, 1}, {0, 0, 0}, ID, 0, &molprop);

  std::array<double, 3> PartPos = {0, 0, 0};
  molprop.addParticle(PartPos, 2, 0);

  CPPUNIT_ASSERT(mol.getNumPart() == 1);  // test whether there is 1 particle in the molecule

  // test the position of the only particle in the molecule
  CPPUNIT_ASSERT(abs(mol.getRealPosX()[0] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosY()[0] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosZ()[0] - 1.0) < 1e-12);

  // test the particle's properties
  CPPUNIT_ASSERT(abs(mol.getSigmaPart()[0] - sigma1) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getMpart()[0] - mass1) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getTypePart()[0] - 2) < 1e-12);
}

void MoleculeTest::addMultipleParticleTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  int ID = 0;

  // create molecule properties
  MoleculeType molprop(ID);

  // create molecule
  MoleculeMS mol({1, 1, 1}, {0, 0, 0}, ID, 0, &molprop);

  std::array<double, 3> PartPos1 = {0, 0, 0};
  std::array<double, 3> PartPos2 = {0, 1, 1};

  molprop.addParticle(PartPos1, 2, 0);
  molprop.addParticle(PartPos2, 3, 0);

  CPPUNIT_ASSERT(mol.getNumPart() == 2);  // test whether there is 1 particle in the molecule

  // test the position of the first particle in the molecule
  CPPUNIT_ASSERT(abs(mol.getRealPosX()[0] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosY()[0] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosZ()[0] - 1.0) < 1e-12);

  // test the particles' properties
  CPPUNIT_ASSERT(abs(mol.getSigmaPart()[0] - sigma1) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getMpart()[0] - mass1) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getTypePart()[0] - 2) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getSigmaPart()[1] - sigma2) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getMpart()[1] - mass2) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getTypePart()[1] - 3) < 1e-12);

  // test the position of the second particle in the molecule
  CPPUNIT_ASSERT(abs(mol.getRealPosX()[1] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosY()[1] - 2.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosZ()[1] - 2.0) < 1e-12);
}

void MoleculeTest::RotationYAxisTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  int ID = 0;
  // create molecule properties
  MoleculeType molprop(ID);

  // create molecule
  MoleculeMS mol({1, 1, 1}, {0, 0, 0}, ID, 0, &molprop);

  std::array<double, 3> PartPos1 = {0, 0, 0};
  std::array<double, 3> PartPos2 = {0, 1, 0};

  molprop.addParticle(PartPos1, 2, 0);
  molprop.addParticle(PartPos2, 3, 0);

  // rotation along the Y-axis should not affect the particles
  std::array<double, 4> o1 = {1.0, 0.0, 1.0, 0.0};
  mol.setO(o1);

  // test the position of the first particle in the molecule
  CPPUNIT_ASSERT(abs(mol.getRealPosX()[0] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosY()[0] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosZ()[0] - 1.0) < 1e-12);

  // test the position of the second particle in the molecule
  CPPUNIT_ASSERT(abs(mol.getRealPosX()[1] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosY()[1] - 2.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosZ()[1] - 1.0) < 1e-12);
}
void MoleculeTest::RotationXAxisTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  int ID = 0;
  // create molecule properties
  MoleculeType molprop(ID);

  // create molecule
  MoleculeMS mol({1, 1, 1}, {0, 0, 0}, ID, 0, &molprop);

  std::array<double, 3> PartPos1 = {0, 0, 0};
  std::array<double, 3> PartPos2 = {0, 1, 0};

  molprop.addParticle(PartPos1, 2, 0);
  molprop.addParticle(PartPos2, 3, 0);

  // rotation along the Y-axis should place the particle on the Z-axis
  std::array<double, 4> o1 = {1.0, 1.0, 0.0, 0.0};
  mol.setO(o1);

  // test the position of the first particle in the molecule
  CPPUNIT_ASSERT(abs(mol.getRealPosX()[0] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosY()[0] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosZ()[0] - 1.0) < 1e-12);

  // test the position of the second particle in the molecule
  CPPUNIT_ASSERT(abs(mol.getRealPosX()[1] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosY()[1] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosZ()[1] - 2.0) < 1e-12);
}
void MoleculeTest::RotationZAxisTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  int ID = 101;
  // create molecule properties
  MoleculeType molprop(ID);

  // create molecule
  MoleculeMS mol({1, 1, 1}, {0, 0, 0}, ID, 0, &molprop);

  std::array<double, 3> PartPos1 = {0, 0, 0};
  std::array<double, 3> PartPos2 = {0, 1, 0};

  molprop.addParticle(PartPos1, 2, 0);
  molprop.addParticle(PartPos2, 3, 0);

  // rotation along the Y-axis should place the particle on the X-axis
  std::array<double, 4> o1 = {1.0, 0.0, 0.0, 1.0};
  mol.setO(o1);

  // test the position of the first particle in the molecule
  CPPUNIT_ASSERT(abs(mol.getRealPosX()[0] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosY()[0] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosZ()[0] - 1.0) < 1e-12);

  // test the position of the second particle in the molecule
  CPPUNIT_ASSERT(abs(mol.getRealPosX()[1] - 0.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosY()[1] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getRealPosZ()[1] - 1.0) < 1e-12);
}

void MoleculeTest::DipoleSymmetricTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  int ID = 102;
  // create molecule properties
  MoleculeType molprop(ID);

  // create molecule
  MoleculeMS mol({1, 1, 1}, {0, 0, 0}, ID, 0, &molprop);

  std::array<double, 3> PartPos1 = {0, -1, 0};
  std::array<double, 3> PartPos2 = {0, 0, 0};
  std::array<double, 3> PartPos3 = {0, 1, 0};

  molprop.addParticle(PartPos1, 2, 0);
  molprop.addParticle(PartPos2, 3, 0);
  molprop.addParticle(PartPos3, 2, 0);

  molprop.computeDipole();

  // verify the charges
  CPPUNIT_ASSERT(abs(mol.getChargePart()[0] - charge1) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getChargePart()[1] - charge2) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getChargePart()[2] - charge1) < 1e-12);

  // verify the dipole
  CPPUNIT_ASSERT(abs(mol.getDipole()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getDipole()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getDipole()[2]) < 1e-12);
}

void MoleculeTest::DipoleAsymmetricTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  int ID = 102;
  // create molecule properties
  MoleculeType molprop(ID);

  // create molecule
  MoleculeMS mol({1, 1, 1}, {0, 0, 0}, ID, 0, &molprop);

  std::array<double, 3> PartPos1 = {1, -1, 0};
  std::array<double, 3> PartPos2 = {0, 1, 0};
  std::array<double, 3> PartPos3 = {-1, -1, 0};

  molprop.addParticle(PartPos1, 2, 0);
  molprop.addParticle(PartPos2, 3, 0);
  molprop.addParticle(PartPos3, 2, 0);

  molprop.computeDipole();

  // verify the charges
  CPPUNIT_ASSERT(abs(mol.getChargePart()[0] - charge1) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getChargePart()[1] - charge2) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getChargePart()[2] - charge1) < 1e-12);

  // verify the dipole
  CPPUNIT_ASSERT(abs(mol.getDipole()[0] - 0.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getDipole()[1] - 12.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getDipole()[2] - 0.0) < 1e-12);
}

void MoleculeTest::DipoleOrientedTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  int ID = 102;
  // create molecule properties
  MoleculeType molprop(ID);

  // create molecule
  MoleculeMS mol({1, 1, 1}, {0, 0, 0}, ID, 0, &molprop);

  std::array<double, 3> PartPos1 = {1, -1, 0};
  std::array<double, 3> PartPos2 = {0, 1, 0};
  std::array<double, 3> PartPos3 = {-1, -1, 0};

  molprop.addParticle(PartPos1, 2, 0);
  molprop.addParticle(PartPos2, 3, 0);
  molprop.addParticle(PartPos3, 2, 0);

  molprop.computeDipole();

  // rotate by 120° along the (1, 1, 1) axis (permute axis)
  std::array<double, 4> orientation = {0.5, 0.5, 0.5, 0.5};
  mol.setO(orientation);

  // verify the charges
  CPPUNIT_ASSERT(abs(mol.getChargePart()[0] - charge1) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getChargePart()[1] - charge2) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getChargePart()[2] - charge1) < 1e-12);

  // verify the dipole
  CPPUNIT_ASSERT(abs(mol.getDipole()[0] - 0.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getDipole()[1] - 0.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getDipole()[2] - 12.0) < 1e-12);

  // rotate by -120° along the (1, 1, 1) axis (permute axis)
  orientation = {0.5, -0.5, -0.5, -0.5};
  mol.setO(orientation);

  CPPUNIT_ASSERT(abs(mol.getDipole()[0] - 12.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getDipole()[1] - 0.0) < 1e-12);
  CPPUNIT_ASSERT(abs(mol.getDipole()[2] - 0.0) < 1e-12);
}
