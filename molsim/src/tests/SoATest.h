/*
 * SoATest.h
 *
 *  Created on: June 25, 2018
 *      Author: raffi
 */

#ifndef SRC_SOATEST_H_
#define SRC_SOATEST_H_

#include <cppunit/TestResult.h>
#include <cppunit/extensions/HelperMacros.h>
#include "../HelperFunctions.h"
#include "../MolSimFunctors.h"
#include "../MoleculeMS.h"
#include "../MoleculeType.h"
#include "../ParticleType.h"

#include <cmath>
#include "autopas/AutoPas.h"
#include "autopas/autopasIncludes.h"

/**
 * @brief TestSuite for SoA
 */
class SoATest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(SoATest);
  CPPUNIT_TEST(SoAGravitySameCellTest);
  CPPUNIT_TEST(SoAGravityDifferentCellTest);
  CPPUNIT_TEST(SoALJSameCellTest);
  CPPUNIT_TEST(SoALJDifferentCellTest);
  CPPUNIT_TEST(SoALJMolSameCellTest);
  CPPUNIT_TEST(SoALJMolDifferentCellTest);
  CPPUNIT_TEST(SoACoulombSameCellTest);
  CPPUNIT_TEST(SoACoulombDifferentCellTest);
  CPPUNIT_TEST_SUITE_END();

 public:
  void setUp() override;
  /**
   * Tests if two particles in two cells interact through gravity the same way in SoA ad AoS
   */
  void SoAGravityDifferentCellTest();

  /**
   * Tests if two particles in one cell interact through gravity the same way in SoA ad AoS
   */
  void SoAGravitySameCellTest();

  /**
   * Tests if two particles in two cells interact through lennard-jones the same way in SoA ad AoS
   */
  void SoALJDifferentCellTest();

  /**
   * Tests if two particles in one cell interact through lennard-jones the same way in SoA ad AoS
   */
  void SoALJSameCellTest();
  /**
   * Tests if two particles in two cells interact through lennard-jones for molecules the same way in SoA ad AoS
   */
  void SoALJMolDifferentCellTest();

  /**
   * Tests if two particles in one cell interact through lennard-jones for molecules the same way in SoA ad AoS
   */
  void SoALJMolSameCellTest();
  /**
   * Tests if two particles in two cells interact through Coulomb's law for molecules the same way in SoA ad AoS
   */
  void SoACoulombDifferentCellTest();

  /**
   * Tests if two particles in one cell interact through Coulomb's law the same way in SoA ad AoS
   */
  void SoACoulombSameCellTest();

  SoATest();
  virtual ~SoATest();
};
#endif /* SRC_BOUNDARYTEST_H_ */
