/*
 * RotationTest.h
 *
 *  Created on: May 27, 2018
 *      Author: raffi
 */

#ifndef SRC_ROTATIONTEST_H_
#define SRC_ROTATIONTEST_H_

#include <cppunit/TestResult.h>
#include <cppunit/extensions/HelperMacros.h>
#include "../Eigenvalues.h"
#include "../HelperFunctions.h"
#include "../MoleculeMS.h"
#include "../MoleculeType.h"
#include "../ParticleType.h"
#include "../UpdateData.h"

#include <cmath>
#include "autopas/AutoPas.h"
#include "autopas/autopasIncludes.h"

/**
 * @brief TestSuite for LinkedContainer
 */
class RotationTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(RotationTest);
  CPPUNIT_TEST(InertiaZeroTest);
  CPPUNIT_TEST(InertiaTest);
  CPPUNIT_TEST(CalculateWTest);
  CPPUNIT_TEST(EigenDiagonalTest);
  CPPUNIT_TEST(EigenMultiplicity3Test);
  CPPUNIT_TEST(EigenMultiplicity2XZTest);
  CPPUNIT_TEST(EigenMultiplicity2YZTest);
  CPPUNIT_TEST(EigenMultiplicity1Test);
  CPPUNIT_TEST(EigenRang2Test);
  CPPUNIT_TEST(EigenRang1Test);
  CPPUNIT_TEST(PARotationTest);
  CPPUNIT_TEST(InertiaPATest);
  CPPUNIT_TEST_SUITE_END();

 public:
  void setUp() override;
  /*
   * tests if the inertia matrix and its inverse are correctly computed
   */
  void InertiaZeroTest();
  /*
   * tests if the inertia matrix and its inverse are correctly computed
   */
  void InertiaTest();

  /*
   * tests if the angular velocity is correctly computed from the torque
   */
  void CalculateWTest();

  /*
   * tests if all eigenvalues are on the diagonal for a diagonal matrix
   */
  void EigenDiagonalTest();

  /*
   * tests if all the eigenvalues are 1 for a matrix with multiplicity 3
   */
  void EigenMultiplicity3Test();

  /*
   * tests if the eigenvalues and vectors are correctly computed if the second an the third eigenvalue are identical
   */
  void EigenMultiplicity2XZTest();

  /*
   * tests if the eigenvalues and vectors are correctly computed if the second an the third eigenvalue are identical
   */
  void EigenMultiplicity2YZTest();

  /*
   * tests if the eigenvalues and vectors are crrectly computed if all three eigenvalues are non-zero and different
   */
  void EigenMultiplicity1Test();
  /*
   * tests if the eigenvalues and vectors are correctly computed if one eigenvalue is zero
   */
  void EigenRang2Test();
  /*
   * tests if the eigenvalues and vectors are correctly computed if 2 eigenvalues are zero
   */
  void EigenRang1Test();
  /**
   * tests if a vector is correctly rotated by the eigenvector matrix
   */
  void PARotationTest();
  /**
   * tests whether the computation of the angular velocity in the principal axis frame yields the same result as a
   * direct computation
   */
  void InertiaPATest();

  RotationTest();
  virtual ~RotationTest();
};
#endif /* SRC_BOUNDARYTEST_H_ */
