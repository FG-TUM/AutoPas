/*
 * RotationTest.cpp
 *
 *  Created on: Jun 06, 2018
 *      Author: raffi
 */

#include "RotationTest.h"
#include <iostream>

using namespace autopas;

const double mass20 = 1.0;
const double sigma20 = 3.0;
const double charge20 = 0;
const double epsilon20 = 3.0;
const double mass30 = 2.0;
const double sigma30 = 2.0;
const double charge30 = 0;
const double epsilon30 = 5.0;
const double CutOff = 5.0;

RotationTest::RotationTest() {
  // TODO Auto-generated constructor stub
}

RotationTest::~RotationTest() {
  // TODO Auto-generated destructor stub
}
void RotationTest::setUp() {
  ParticleType::setType(20, mass20, epsilon20, sigma20, charge20, CutOff, false);
  ParticleType::setType(30, mass30, epsilon30, sigma30, charge30, CutOff, false);
}

void RotationTest::InertiaZeroTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  int ID = 100;

  // create molecule properties
  MoleculeType molprop(ID);

  std::array<double, 3> PartPos = {0, 0, 0};
  molprop.addParticle(PartPos, 20, 0);

  // test inertia matrix
  CPPUNIT_ASSERT(abs(molprop.getI()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getI()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getI()[2]) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getI()[3]) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getI()[4]) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getI()[5]) < 1e-12);

  // test inverse inertia matrix
  CPPUNIT_ASSERT(abs(molprop.getInvI()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getInvI()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getInvI()[2]) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getInvI()[3]) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getInvI()[4]) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getInvI()[5]) < 1e-12);
}

void RotationTest::InertiaTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  int ID = 101;

  // create molecule properties
  MoleculeType molprop(ID);

  std::array<double, 3> PartPos1 = {-1, -1, 1};
  std::array<double, 3> PartPos2 = {1, 1, 1};
  std::array<double, 3> PartPos3 = {0, 0, 2};
  molprop.addParticle(PartPos1, 20, 0);
  molprop.addParticle(PartPos2, 20, 0);
  molprop.addParticle(PartPos3, 20, 0);

  // test inertia matrix
  CPPUNIT_ASSERT(abs(molprop.getI()[0] - 8.0) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getI()[1] - 8.0) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getI()[2] - 4.0) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getI()[3] + 2.0) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getI()[4] - 0.0) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getI()[5] - 0.0) < 1e-12);

  // test inverse inertia matrix
  CPPUNIT_ASSERT(abs(molprop.getInvI()[0] - 2.0 / 15) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getInvI()[1] - 2.0 / 15) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getInvI()[2] - 0.25) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getInvI()[3] - 1.0 / 30) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getInvI()[4] - 0.0) < 1e-12);
  CPPUNIT_ASSERT(abs(molprop.getInvI()[5] - 0.0) < 1e-12);
}

void RotationTest::CalculateWTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  std::array<double, 3> BoxMax = {10, 10, 10};
  AutoPasObject->init(BoxMax, CutOff, linkedCells);

  int ID = 101;

  // create molecule properties
  MoleculeType molprop(ID);

  std::array<double, 3> PartPos1 = {-1, -1, 1};
  std::array<double, 3> PartPos2 = {1, 1, 1};
  std::array<double, 3> PartPos3 = {0, 0, 2};
  molprop.addParticle(PartPos1, 20, 0);
  molprop.addParticle(PartPos2, 20, 0);
  molprop.addParticle(PartPos3, 20, 0);

  // create molecule
  MoleculeMS mol({1, 1, 1}, {0, 0, 0}, ID, 0, &molprop);
  mol.getO() = {1, 0, 0, 0};

  AutoPasObject->addParticle(mol);
  MoleculeMS &m = *AutoPasObject->begin();

  double delta_t = 0.1;

  // set the torque
  m.getA() = {0, 0, 0};
  m.getOldA() = {0, 0, 0};
  m.getW() = {0, 0, 0};

  calculateW(&m, delta_t);

  // test if the oldA has been setted and if the velocity is computed correctly
  CPPUNIT_ASSERT(abs(m.getOldA()[0] - 0) < 1e-12);
  CPPUNIT_ASSERT(abs(m.getOldA()[1] - 0) < 1e-12);
  CPPUNIT_ASSERT(abs(m.getOldA()[2] - 0) < 1e-12);
  CPPUNIT_ASSERT(abs(m.getW()[0] - 0) < 1e-12);
  CPPUNIT_ASSERT(abs(m.getW()[1] - 0) < 1e-12);
  CPPUNIT_ASSERT(abs(m.getW()[2] - 0) < 1e-12);

  // set the torque
  m.getA() = {10, 0, 0};
  m.getOldA() = {0, 0, 0};
  m.getW() = {0, 0, 0};

  calculateW(&m, delta_t);

  // test if the oldA has been setted and if the velocity is computed correctly
  CPPUNIT_ASSERT(abs(m.getOldA()[0] - 10) < 1e-12);
  CPPUNIT_ASSERT(abs(m.getOldA()[1] - 0) < 1e-12);
  CPPUNIT_ASSERT(abs(m.getOldA()[2] - 0) < 1e-12);
  CPPUNIT_ASSERT(m.getW()[0] > 0);
  CPPUNIT_ASSERT(m.getW()[1] > 0);
  CPPUNIT_ASSERT(abs(m.getW()[2] - 0) < 1e-12);

  // set the torque
  m.getA() = {0, 10, 0};
  m.getOldA() = {0, 0, 0};
  m.getW() = {0, 0, 10};

  calculateW(&m, delta_t);

  // test if the oldA has been setted and if the velocity is computed correctly
  CPPUNIT_ASSERT(abs(m.getOldA()[0] - 0) < 1e-12);
  CPPUNIT_ASSERT(abs(m.getOldA()[1] - 10) < 1e-12);
  CPPUNIT_ASSERT(abs(m.getOldA()[2] - 0) < 1e-12);
  CPPUNIT_ASSERT(m.getW()[0] > 0);
  CPPUNIT_ASSERT(m.getW()[1] > 0);
  CPPUNIT_ASSERT(abs(m.getW()[2] - 10) < 1e-12);

  // set the torque
  m.getA() = {0, 0, -10};
  m.getOldA() = {0, 0, 0};
  m.getW() = {-10, 20, 0};

  calculateW(&m, delta_t);

  // test if the oldA has been setted and if the velocity is computed correctly
  CPPUNIT_ASSERT(abs(m.getOldA()[0] - 0) < 1e-12);
  CPPUNIT_ASSERT(abs(m.getOldA()[1] - 0) < 1e-12);
  CPPUNIT_ASSERT(abs(m.getOldA()[2] + 10) < 1e-12);
  CPPUNIT_ASSERT(abs(m.getW()[0] + 10) < 1e-12);
  CPPUNIT_ASSERT(abs(m.getW()[1] - 20) < 1e-12);
  CPPUNIT_ASSERT(m.getW()[2] < 0);

  // set the torque
  m.getA() = {-20, 15, -10};
  m.getOldA() = {0, 0, 0};
  m.getW() = {0, 0, 0};

  calculateW(&m, delta_t);

  // test if the oldA has been setted and if the velocity is computed correctly
  CPPUNIT_ASSERT(abs(m.getOldA()[0] + 20) < 1e-12);
  CPPUNIT_ASSERT(abs(m.getOldA()[1] - 15) < 1e-12);
  CPPUNIT_ASSERT(abs(m.getOldA()[2] + 10) < 1e-12);
  CPPUNIT_ASSERT(m.getW()[0] < 0);
  CPPUNIT_ASSERT(m.getW()[1] > 0);
  CPPUNIT_ASSERT(m.getW()[2] < 0);
}

void RotationTest::EigenDiagonalTest() {
  std::array<double, 6> matrix = {1, -1, 2, 0, 0, 0};

  std::array<double, 3> eigenvalues = getEigenValues(matrix);

  std::array<double, 9> vec = getEigenVectors(eigenvalues, matrix);

  std::array<double, 3> eigenvector0 = {vec[0], vec[1], vec[2]};
  std::array<double, 3> eigenvector1 = {vec[3], vec[4], vec[5]};
  std::array<double, 3> eigenvector2 = {vec[6], vec[7], vec[8]};

  // test eigenvalues
  CPPUNIT_ASSERT(abs(eigenvalues[0] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvalues[1] + 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvalues[2] - 2.0) < 1e-12);

  // test eigenvectors
  CPPUNIT_ASSERT(abs(eigenvector0[0] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvector0[1] - 0.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvector0[2] - 0.0) < 1e-12);

  CPPUNIT_ASSERT(abs(eigenvector1[0] - 0.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvector1[1] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvector1[2] - 0.0) < 1e-12);

  CPPUNIT_ASSERT(abs(eigenvector2[0] - 0.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvector2[1] - 0.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvector2[2] - 1.0) < 1e-12);
}

void RotationTest::EigenMultiplicity3Test() {
  std::array<double, 6> matrix = {2, 1, 1, 2, 2, 2};

  std::array<double, 3> eigenvalues = {2, 2, 2};

  std::array<double, 9> vec = getEigenVectors(eigenvalues, matrix);

  std::array<double, 3> eigenvector0 = {vec[0], vec[1], vec[2]};
  std::array<double, 3> eigenvector1 = {vec[3], vec[4], vec[5]};
  std::array<double, 3> eigenvector2 = {vec[6], vec[7], vec[8]};

  // test eigenvalues
  CPPUNIT_ASSERT(abs(eigenvalues[0] - 2.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvalues[1] - 2.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvalues[2] - 2.0) < 1e-12);

  // test eigenvectors
  CPPUNIT_ASSERT(abs(eigenvector0[0] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvector0[1] - 0.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvector0[2] - 0.0) < 1e-12);

  CPPUNIT_ASSERT(abs(eigenvector1[0] - 0.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvector1[1] - 1.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvector1[2] - 0.0) < 1e-12);

  CPPUNIT_ASSERT(abs(eigenvector2[0] - 0.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvector2[1] - 0.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvector2[2] - 1.0) < 1e-12);
}

void RotationTest::EigenMultiplicity2XZTest() {
  std::array<double, 6> matrix = {1, 1, 3, -2, 0, 0};

  std::array<double, 3> eigenvalues = getEigenValues(matrix);

  std::array<double, 9> vec = getEigenVectors(eigenvalues, matrix);

  std::array<double, 3> eigenvector0 = {vec[0], vec[1], vec[2]};
  std::array<double, 3> eigenvector1 = {vec[3], vec[4], vec[5]};
  std::array<double, 3> eigenvector2 = {vec[6], vec[7], vec[8]};

  // test eigenvalues
  CPPUNIT_ASSERT(abs(eigenvalues[0] - 3.0) < 1e-7);
  CPPUNIT_ASSERT(abs(eigenvalues[1] + 1.0) < 1e-7);
  CPPUNIT_ASSERT(abs(eigenvalues[2] - 3.0) < 1e-7);

  // test eigenvectors
  CPPUNIT_ASSERT(abs(eigenvector0[0] - 0.0) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector0[1] - 0.0) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector0[2] - 1.0) < 1e-8);

  CPPUNIT_ASSERT(abs(eigenvector1[0] - 1.0 / sqrt(2.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector1[1] - 1.0 / sqrt(2.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector1[2] - 0.0) < 1e-8);

  CPPUNIT_ASSERT(abs(eigenvector2[0] - 1.0 / sqrt(2.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector2[1] + 1.0 / sqrt(2.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector2[2] - 0.0) < 1e-8);
}

void RotationTest::EigenMultiplicity2YZTest() {
  std::array<double, 6> matrix = {1, -1, 1, 0, 0, -2};

  std::array<double, 3> eigenvalues = getEigenValues(matrix);

  std::array<double, 9> vec = getEigenVectors(eigenvalues, matrix);

  std::array<double, 3> eigenvector0 = {vec[0], vec[1], vec[2]};
  std::array<double, 3> eigenvector1 = {vec[3], vec[4], vec[5]};
  std::array<double, 3> eigenvector2 = {vec[6], vec[7], vec[8]};

  // test eigenvalues
  CPPUNIT_ASSERT(abs(eigenvalues[0] - 3.0) < 1e-7);
  CPPUNIT_ASSERT(abs(eigenvalues[1] + 1.0) < 1e-7);
  CPPUNIT_ASSERT(abs(eigenvalues[2] + 1.0) < 1e-7);

  // test eigenvectors
  CPPUNIT_ASSERT(abs(eigenvector0[0] + 1.0 / sqrt(2.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector0[1] - 0.0) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector0[2] - 1.0 / sqrt(2.0)) < 1e-8);

  CPPUNIT_ASSERT(abs(eigenvector1[0] + 1.0 / sqrt(2.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector1[1] - 0.0) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector1[2] + 1.0 / sqrt(2.0)) < 1e-8);

  CPPUNIT_ASSERT(abs(eigenvector2[0] - 0.0) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector2[1] + 1.0) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector2[2] - 0.0) < 1e-8);
}

void RotationTest::EigenMultiplicity1Test() {
  std::array<double, 6> matrix = {1, 1, 1, -2, 0, 0};

  std::array<double, 3> eigenvalues = getEigenValues(matrix);

  std::array<double, 9> vec = getEigenVectors(eigenvalues, matrix);

  std::array<double, 3> eigenvector0 = {vec[0], vec[1], vec[2]};
  std::array<double, 3> eigenvector1 = {vec[3], vec[4], vec[5]};
  std::array<double, 3> eigenvector2 = {vec[6], vec[7], vec[8]};

  // test eigenvalues
  CPPUNIT_ASSERT(abs(eigenvalues[0] - 3.0) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvalues[1] + 1.0) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvalues[2] - 1.0) < 1e-8);

  // test eigenvectors
  CPPUNIT_ASSERT(abs(eigenvector0[0] + 1.0 / sqrt(2.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector0[1] - 1.0 / sqrt(2.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector0[2] - 0.0) < 1e-8);

  CPPUNIT_ASSERT(abs(eigenvector1[0] - 1.0 / sqrt(2.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector1[1] - 1.0 / sqrt(2.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector1[2] - 0.0) < 1e-8);

  CPPUNIT_ASSERT(abs(eigenvector2[0] - 0.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvector2[1] - 0.0) < 1e-12);
  CPPUNIT_ASSERT(abs(eigenvector2[2] + 1.0) < 1e-12);
}

void RotationTest::EigenRang2Test() {
  std::array<double, 6> matrix = {4, 4, 4, -2, -2, -2};

  std::array<double, 3> eigenvalues = getEigenValues(matrix);

  std::array<double, 9> vec = getEigenVectors(eigenvalues, matrix);

  std::array<double, 3> eigenvector0 = {vec[0], vec[1], vec[2]};
  std::array<double, 3> eigenvector1 = {vec[3], vec[4], vec[5]};
  std::array<double, 3> eigenvector2 = {vec[6], vec[7], vec[8]};

  // test eigenvalues
  CPPUNIT_ASSERT(abs(eigenvalues[0] - 6.0) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvalues[1] - 0.0) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvalues[2] - 6.0) < 1e-8);

  // test eigenvectors
  CPPUNIT_ASSERT(abs(eigenvector0[0] + 1.0 / sqrt(2.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector0[1] - 0.0) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector0[2] - 1.0 / sqrt(2.0)) < 1e-8);

  CPPUNIT_ASSERT(abs(eigenvector1[0] - 1.0 / sqrt(3.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector1[1] - 1.0 / sqrt(3.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector1[2] - 1.0 / sqrt(3.0)) < 1e-8);

  CPPUNIT_ASSERT(abs(eigenvector2[0] - 1.0 / sqrt(6.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector2[1] + 2.0 / sqrt(6.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector2[2] - 1.0 / sqrt(6.0)) < 1e-8);
}

void RotationTest::EigenRang1Test() {
  std::array<double, 6> matrix = {1, 1, 1, 1, 1, 1};

  std::array<double, 3> eigenvalues = getEigenValues(matrix);

  std::array<double, 9> vec = getEigenVectors(eigenvalues, matrix);

  std::array<double, 3> eigenvector0 = {vec[0], vec[1], vec[2]};
  std::array<double, 3> eigenvector1 = {vec[3], vec[4], vec[5]};
  std::array<double, 3> eigenvector2 = {vec[6], vec[7], vec[8]};

  // test eigenvalues
  CPPUNIT_ASSERT(abs(eigenvalues[0] - 3.0) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvalues[1] - 0.0) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvalues[2] - 0.0) < 1e-8);

  // test eigenvectors
  CPPUNIT_ASSERT(abs(eigenvector0[0] + 1.0 / sqrt(3.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector0[1] + 1.0 / sqrt(3.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector0[2] + 1.0 / sqrt(3.0)) < 1e-8);

  CPPUNIT_ASSERT(abs(eigenvector1[0] - 1.0 / sqrt(2.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector1[1] - 0.0) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector1[2] + 1.0 / sqrt(2.0)) < 1e-8);

  CPPUNIT_ASSERT(abs(eigenvector2[0] - 1.0 / sqrt(6.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector2[1] + 2.0 / sqrt(6.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(eigenvector2[2] - 1.0 / sqrt(6.0)) < 1e-8);
}

void RotationTest::PARotationTest() {
  std::array<double, 3> vec = {1, 1, 1};

  setUp();

  int ID = 102;

  // create molecule properties
  MoleculeType molprop(ID);

  std::array<double, 3> PartPos1 = {-1, -1, 1};
  std::array<double, 3> PartPos2 = {1, 1, 1};
  std::array<double, 3> PartPos3 = {0, 0, 2};
  molprop.addParticle(PartPos1, 20, 0);
  molprop.addParticle(PartPos2, 20, 0);
  molprop.addParticle(PartPos3, 20, 0);

  molprop.RotatePA(vec);

  // test the modified vector
  CPPUNIT_ASSERT(abs(sqrt(am.dot(vec, vec)) - sqrt(3.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(vec[0] + sqrt(2.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(vec[1] - 0.0) < 1e-8);
  CPPUNIT_ASSERT(abs(vec[2] + 1.0) < 1e-8);

  molprop.RotateBackPA(vec);

  // test the modified vector
  CPPUNIT_ASSERT(abs(sqrt(am.dot(vec, vec)) - sqrt(3.0)) < 1e-8);
  CPPUNIT_ASSERT(abs(vec[0] - 1.0) < 1e-8);
  CPPUNIT_ASSERT(abs(vec[1] - 1.0) < 1e-8);
  CPPUNIT_ASSERT(abs(vec[2] - 1.0) < 1e-8);
}

void RotationTest::InertiaPATest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>> *AutoPasObject =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  std::array<double, 3> BoxMax = {10, 10, 10};
  AutoPasObject->init(BoxMax, CutOff, linkedCells);

  int ID = 103;

  // create molecule properties
  MoleculeType molprop(ID);

  std::array<double, 3> PartPos1 = {-1, -1, 1};
  std::array<double, 3> PartPos2 = {1, 1, 1};
  std::array<double, 3> PartPos3 = {0, 0, 2};
  molprop.addParticle(PartPos1, 20, 0);
  molprop.addParticle(PartPos2, 20, 0);
  molprop.addParticle(PartPos3, 20, 0);

  // compute the angular velocity via the inverse inertia matrix
  MoleculeMS mol({1, 1, 1}, {0, 0, 0}, ID, 0, &molprop);
  mol.getO() = {1, 0, 0, 0};

  AutoPasObject->addParticle(mol);
  MoleculeMS &m = *AutoPasObject->begin();

  double delta_t = 0.1;

  m.getProperties()->getEnablePA() = false;

  // set the torque
  m.getA() = {2, 3, -1};
  m.getOldA() = {3, 2, 1};
  m.getW() = {-1, -2.4, 3};

  calculateW(&m, delta_t);

  std::array<double, 3> WTensor = m.getW();

  // compute the angular velocity via the principal axis method
  m.getProperties()->getEnablePA() = true;

  // set the torque
  m.getA() = {2, 3, -1};
  m.getOldA() = {3, 2, 1};
  m.getW() = {-1, -2.4, 3};

  calculateW(&m, delta_t);

  std::array<double, 3> WPA = m.getW();

  CPPUNIT_ASSERT(abs(WTensor[0] - WPA[0]) < 1e-8);
  CPPUNIT_ASSERT(abs(WTensor[1] - WPA[1]) < 1e-8);
  CPPUNIT_ASSERT(abs(WTensor[2] - WPA[2]) < 1e-8);
}
