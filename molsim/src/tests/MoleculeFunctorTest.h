/*
 * MoleculeFunctorTest.h
 *
 *  Created on: June 05, 2018
 *      Author: raffi
 */

#ifndef SRC_MOLECULEFUNCTORTEST_H_
#define SRC_MOLECULEFUNCTORTEST_H_

#include <cppunit/TestResult.h>
#include <cppunit/extensions/HelperMacros.h>
#include "../HelperFunctions.h"
#include "../MoleculeMS.h"
#include "../MoleculeType.h"
#include "../ParticleType.h"

#include <cmath>
#include "autopas/AutoPas.h"
#include "autopas/autopasIncludes.h"

/**
 * @brief TestSuite for LinkedContainer
 */
class MoleculeFunctorTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(MoleculeFunctorTest);
  CPPUNIT_TEST(addParticleTest);
  CPPUNIT_TEST(addMultipleParticleTest);
  CPPUNIT_TEST_SUITE_END();

 public:
  void setUp() override;
  /**
   * Tests if a particle is correctly added to the molecule
   */
  void addParticleTest();

  /**
   * Tests if two particles are correctly added to the molecule
   */
  void addMultipleParticleTest();

  MoleculeFunctorTest();
  virtual ~MoleculeFunctorTest();
};
#endif /* SRC_BOUNDARYTEST_H_ */
