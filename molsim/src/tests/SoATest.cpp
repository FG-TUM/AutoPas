/*
 * SoATest.cpp
 *
 *  Created on: May 27, 2018
 *      Author: raffi
 */

#include "SoATest.h"
#include <iostream>

using namespace autopas;

const double mass1 = 1.0;
const double sigma1 = 3.0;
const double epsilon1 = 3.0;
const double charge1 = 0;
const double CutOff = 5.0;

SoATest::SoATest() {
  // TODO Auto-generated constructor stub
}

SoATest::~SoATest() {
  // TODO Auto-generated destructor stub
}
void SoATest::setUp() { ParticleType::setType(34, mass1, epsilon1, sigma1, charge1, CutOff, false); }

void SoATest::SoAGravityDifferentCellTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>* ObjectSoA =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>* ObjectAoS =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  std::array<double, 3> BoxMax = {10, 10, 10};
  ObjectSoA->init(BoxMax, CutOff, linkedCells);
  ObjectAoS->init(BoxMax, CutOff, linkedCells);

  // create molecule
  MoleculeMS mol1SoA({4.5, 4.5, 4.5}, {0, 0, 0}, 34, 0, nullptr);
  MoleculeMS mol2SoA({5.5, 5.5, 5.5}, {0, 0, 0}, 34, 1, nullptr);

  MoleculeMS mol1AoS({4.5, 4.5, 4.5}, {0, 0, 0}, 34, 2, nullptr);
  MoleculeMS mol2AoS({5.5, 5.5, 5.5}, {0, 0, 0}, 34, 3, nullptr);

  GravityFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fgravity(CutOff);

  ObjectSoA->addParticle(mol1SoA);
  ObjectSoA->addParticle(mol2SoA);

  ObjectAoS->addParticle(mol1AoS);
  ObjectAoS->addParticle(mol2AoS);

  ObjectSoA->iteratePairwise(&fgravity, soa);
  ObjectAoS->iteratePairwise(&fgravity, aos);

  mol1SoA = *ObjectSoA->begin();
  mol2SoA = *(++ObjectSoA->begin());

  mol1AoS = *ObjectAoS->begin();
  mol2AoS = *(++ObjectAoS->begin());

  // compare the calculated forces of the first molecule
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[0] - mol1AoS.getF()[0]) / mol1AoS.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[1] - mol1AoS.getF()[1]) / mol1AoS.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[2] - mol1AoS.getF()[2]) / mol1AoS.getF()[2]) < 1e-12);

  // compare the calculated forces of the second molecule
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[0] - mol2AoS.getF()[0]) / mol2AoS.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[1] - mol2AoS.getF()[1]) / mol2AoS.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[2] - mol2AoS.getF()[2]) / mol2AoS.getF()[2]) < 1e-12);
}

void SoATest::SoAGravitySameCellTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>* ObjectSoA =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>* ObjectAoS =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  std::array<double, 3> BoxMax = {10, 10, 10};
  ObjectSoA->init(BoxMax, CutOff, linkedCells);
  ObjectAoS->init(BoxMax, CutOff, linkedCells);

  // create molecule
  MoleculeMS mol1SoA({1.0, 1.0, 1.0}, {0, 0, 0}, 34, 0, nullptr);
  MoleculeMS mol2SoA({2.0, 2.0, 2.0}, {0, 0, 0}, 34, 1, nullptr);

  MoleculeMS mol1AoS({1.0, 1.0, 1.0}, {0, 0, 0}, 34, 2, nullptr);
  MoleculeMS mol2AoS({2.0, 2.0, 2.0}, {0, 0, 0}, 34, 3, nullptr);

  GravityFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fgravity(CutOff);

  ObjectSoA->addParticle(mol1SoA);
  ObjectSoA->addParticle(mol2SoA);

  ObjectAoS->addParticle(mol1AoS);
  ObjectAoS->addParticle(mol2AoS);

  ObjectSoA->iteratePairwise(&fgravity, soa);
  ObjectAoS->iteratePairwise(&fgravity, aos);

  mol1SoA = *ObjectSoA->begin();
  mol2SoA = *(++ObjectSoA->begin());

  mol1AoS = *ObjectAoS->begin();
  mol2AoS = *(++ObjectAoS->begin());

  // compare the calculated forces of the first molecule
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[0] - mol1AoS.getF()[0]) / mol1AoS.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[1] - mol1AoS.getF()[1]) / mol1AoS.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[2] - mol1AoS.getF()[2]) / mol1AoS.getF()[2]) < 1e-12);

  // compare the calculated forces of the second molecule
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[0] - mol2AoS.getF()[0]) / mol2AoS.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[1] - mol2AoS.getF()[1]) / mol2AoS.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[2] - mol2AoS.getF()[2]) / mol2AoS.getF()[2]) < 1e-12);
}

void SoATest::SoALJDifferentCellTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>* ObjectSoA =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>* ObjectAoS =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  std::array<double, 3> BoxMax = {10, 10, 10};
  ObjectSoA->init(BoxMax, CutOff, linkedCells);
  ObjectAoS->init(BoxMax, CutOff, linkedCells);

  // create molecule
  MoleculeMS mol1SoA({4.5, 4.5, 4.5}, {0, 0, 0}, 34, 0, nullptr);
  MoleculeMS mol2SoA({5.5, 5.5, 5.5}, {0, 0, 0}, 34, 1, nullptr);

  MoleculeMS mol1AoS({4.5, 4.5, 4.5}, {0, 0, 0}, 34, 2, nullptr);
  MoleculeMS mol2AoS({5.5, 5.5, 5.5}, {0, 0, 0}, 34, 3, nullptr);

  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);

  fLJ.setEpsilonPairs(ParticleType::getEpsilonPairs());

  ObjectSoA->addParticle(mol1SoA);
  ObjectSoA->addParticle(mol2SoA);

  ObjectAoS->addParticle(mol1AoS);
  ObjectAoS->addParticle(mol2AoS);

  ObjectSoA->iteratePairwise(&fLJ, soa);
  ObjectAoS->iteratePairwise(&fLJ, aos);

  mol1SoA = *ObjectSoA->begin();
  mol2SoA = *(++ObjectSoA->begin());

  mol1AoS = *ObjectAoS->begin();
  mol2AoS = *(++ObjectAoS->begin());

  // compare the calculated forces of the first molecule
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[0] - mol1AoS.getF()[0]) / mol1AoS.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[1] - mol1AoS.getF()[1]) / mol1AoS.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[2] - mol1AoS.getF()[2]) / mol1AoS.getF()[2]) < 1e-12);

  // compare the calculated forces of the second molecule
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[0] - mol2AoS.getF()[0]) / mol2AoS.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[1] - mol2AoS.getF()[1]) / mol2AoS.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[2] - mol2AoS.getF()[2]) / mol2AoS.getF()[2]) < 1e-12);
}

void SoATest::SoALJSameCellTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>* ObjectSoA =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>* ObjectAoS =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  setUp();

  std::array<double, 3> BoxMax = {10, 10, 10};
  ObjectSoA->init(BoxMax, CutOff, linkedCells);
  ObjectAoS->init(BoxMax, CutOff, linkedCells);

  // create molecule properties
  //	MoleculeType molprop(ID);

  // create molecule
  MoleculeMS mol1SoA({1.0, 1.0, 1.0}, {0, 0, 0}, 34, 0, nullptr);
  MoleculeMS mol2SoA({2.0, 2.0, 2.0}, {0, 0, 0}, 34, 1, nullptr);

  MoleculeMS mol1AoS({1.0, 1.0, 1.0}, {0, 0, 0}, 34, 2, nullptr);
  MoleculeMS mol2AoS({2.0, 2.0, 2.0}, {0, 0, 0}, 34, 3, nullptr);

  /*	std::array<double, 3> PartPos1 = {0.1, 0, 0};
          std::array<double, 3> PartPos2 = {-0.1, 0, 0};

          molprop.addParticle(PartPos1, 34, 0);
          molprop.addParticle(PartPos2, 34, 0);
  */

  GravityFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fgravity(CutOff);
  LennardJonesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJ(CutOff);
  LennardJonesMoleculesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJmol(CutOff);

  fLJ.setEpsilonPairs(ParticleType::getEpsilonPairs());

  ObjectSoA->addParticle(mol1SoA);
  ObjectSoA->addParticle(mol2SoA);

  ObjectAoS->addParticle(mol1AoS);
  ObjectAoS->addParticle(mol2AoS);

  ObjectSoA->iteratePairwise(&fLJ, soa);
  ObjectAoS->iteratePairwise(&fLJ, aos);

  mol1SoA = *ObjectSoA->begin();
  mol2SoA = *(++ObjectSoA->begin());

  mol1AoS = *ObjectAoS->begin();
  mol2AoS = *(++ObjectAoS->begin());

  // compare the calculated forces of the first molecule
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[0] - mol1AoS.getF()[0]) / mol1AoS.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[1] - mol1AoS.getF()[1]) / mol1AoS.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[2] - mol1AoS.getF()[2]) / mol1AoS.getF()[2]) < 1e-12);

  // compare the calculated forces of the second molecule
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[0] - mol2AoS.getF()[0]) / mol2AoS.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[1] - mol2AoS.getF()[1]) / mol2AoS.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[2] - mol2AoS.getF()[2]) / mol2AoS.getF()[2]) < 1e-12);
}

void SoATest::SoALJMolDifferentCellTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>* ObjectSoA =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>* ObjectAoS =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger

  setUp();

  std::array<double, 3> BoxMax = {10, 10, 10};
  ObjectSoA->init(BoxMax, CutOff, linkedCells);
  ObjectAoS->init(BoxMax, CutOff, linkedCells);

  int ID = 0;

  // create molecule properties
  MoleculeType molprop(ID);

  // create molecule
  MoleculeMS mol1SoA({4.5, 4.5, 4.5}, {0, 0, 0}, 34, 0, &molprop);
  MoleculeMS mol2SoA({5.5, 5.5, 5.5}, {0, 0, 0}, 34, 1, &molprop);

  MoleculeMS mol1AoS({4.5, 4.5, 4.5}, {0, 0, 0}, 34, 2, &molprop);
  MoleculeMS mol2AoS({5.5, 5.5, 5.5}, {0, 0, 0}, 34, 3, &molprop);

  std::array<double, 3> PartPos1 = {0.1, 0, 0};
  std::array<double, 3> PartPos2 = {-0.1, 0, 0};

  molprop.addParticle(PartPos1, 34, 0);
  molprop.addParticle(PartPos2, 34, 0);

  LennardJonesMoleculesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJmol(CutOff);

  fLJmol.setEpsilonPairs(ParticleType::getEpsilonPairs());

  ObjectSoA->addParticle(mol1SoA);
  ObjectSoA->addParticle(mol2SoA);

  ObjectAoS->addParticle(mol1AoS);
  ObjectAoS->addParticle(mol2AoS);

  ObjectSoA->iteratePairwise(&fLJmol, soa);
  ObjectAoS->iteratePairwise(&fLJmol, aos);

  mol1SoA = *ObjectSoA->begin();
  mol2SoA = *(++ObjectSoA->begin());

  mol1AoS = *ObjectAoS->begin();
  mol2AoS = *(++ObjectAoS->begin());

  // compare the calculated forces of the first molecule
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[0] - mol1AoS.getF()[0]) / mol1AoS.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[1] - mol1AoS.getF()[1]) / mol1AoS.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[2] - mol1AoS.getF()[2]) / mol1AoS.getF()[2]) < 1e-12);

  // compare the calculated forces of the second molecule
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[0] - mol2AoS.getF()[0]) / mol2AoS.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[1] - mol2AoS.getF()[1]) / mol2AoS.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[2] - mol2AoS.getF()[2]) / mol2AoS.getF()[2]) < 1e-12);

  // compare the calculated torques of the first molecule
  CPPUNIT_ASSERT(abs((mol1SoA.getA()[0] - mol1AoS.getA()[0]) / mol1AoS.getA()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getA()[1] - mol1AoS.getA()[1]) / mol1AoS.getA()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getA()[2] - mol1AoS.getA()[2]) / mol1AoS.getA()[2]) < 1e-12);

  // compare the calculated torques of the second molecule
  CPPUNIT_ASSERT(abs((mol2SoA.getA()[0] - mol2AoS.getA()[0]) / mol2AoS.getA()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getA()[1] - mol2AoS.getA()[1]) / mol2AoS.getA()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getA()[2] - mol2AoS.getA()[2]) / mol2AoS.getA()[2]) < 1e-12);
}

void SoATest::SoALJMolSameCellTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>* ObjectSoA =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>* ObjectAoS =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger

  setUp();

  std::array<double, 3> BoxMax = {10, 10, 10};
  ObjectSoA->init(BoxMax, CutOff, linkedCells);
  ObjectAoS->init(BoxMax, CutOff, linkedCells);

  int ID = 0;

  // create molecule properties
  MoleculeType molprop(ID);

  // create molecule
  MoleculeMS mol1SoA({1.0, 1.0, 1.0}, {0, 0, 0}, 34, 0, &molprop);
  MoleculeMS mol2SoA({2.0, 2.0, 2.0}, {0, 0, 0}, 34, 1, &molprop);

  MoleculeMS mol1AoS({1.0, 1.0, 1.0}, {0, 0, 0}, 34, 2, &molprop);
  MoleculeMS mol2AoS({2.0, 2.0, 2.0}, {0, 0, 0}, 34, 3, &molprop);

  std::array<double, 3> PartPos1 = {0.1, 0, 0};
  std::array<double, 3> PartPos2 = {-0.1, 0, 0};

  molprop.addParticle(PartPos1, 34, 0);
  molprop.addParticle(PartPos2, 34, 0);

  LennardJonesMoleculesFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fLJmol(CutOff);

  fLJmol.setEpsilonPairs(ParticleType::getEpsilonPairs());

  ObjectSoA->addParticle(mol1SoA);
  ObjectSoA->addParticle(mol2SoA);

  ObjectAoS->addParticle(mol1AoS);
  ObjectAoS->addParticle(mol2AoS);

  ObjectSoA->iteratePairwise(&fLJmol, soa);
  ObjectAoS->iteratePairwise(&fLJmol, aos);

  mol1SoA = *ObjectSoA->begin();
  mol2SoA = *(++ObjectSoA->begin());

  mol1AoS = *ObjectAoS->begin();
  mol2AoS = *(++ObjectAoS->begin());

  // compare the calculated forces of the first molecule
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[0] - mol1AoS.getF()[0]) / mol1AoS.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[1] - mol1AoS.getF()[1]) / mol1AoS.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[2] - mol1AoS.getF()[2]) / mol1AoS.getF()[2]) < 1e-12);

  // compare the calculated forces of the second molecule
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[0] - mol2AoS.getF()[0]) / mol2AoS.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[1] - mol2AoS.getF()[1]) / mol2AoS.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[2] - mol2AoS.getF()[2]) / mol2AoS.getF()[2]) < 1e-12);

  // compare the calculated torques of the first molecule
  CPPUNIT_ASSERT(abs((mol1SoA.getA()[0] - mol1AoS.getA()[0]) / mol1AoS.getA()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getA()[1] - mol1AoS.getA()[1]) / mol1AoS.getA()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getA()[2] - mol1AoS.getA()[2]) / mol1AoS.getA()[2]) < 1e-12);

  // compare the calculated torques of the second molecule
  CPPUNIT_ASSERT(abs((mol2SoA.getA()[0] - mol2AoS.getA()[0]) / mol2AoS.getA()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getA()[1] - mol2AoS.getA()[1]) / mol2AoS.getA()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getA()[2] - mol2AoS.getA()[2]) / mol2AoS.getA()[2]) < 1e-12);
}

void SoATest::SoACoulombDifferentCellTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>* ObjectSoA =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>* ObjectAoS =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger

  setUp();

  std::array<double, 3> BoxMax = {20, 20, 20};
  ObjectSoA->init(BoxMax, 2 * CutOff, linkedCells);
  ObjectAoS->init(BoxMax, 2 * CutOff, linkedCells);

  int ID = 0;

  // create molecule properties
  MoleculeType molprop(ID);

  // create molecule
  MoleculeMS mol1SoA({8.5, 8.5, 8.5}, {0, 0, 0}, 34, 0, &molprop);
  MoleculeMS mol2SoA({11.5, 11.5, 11.5}, {0, 0, 0}, 34, 1, &molprop);

  MoleculeMS mol1AoS({8.5, 8.5, 8.5}, {0, 0, 0}, 34, 2, &molprop);
  MoleculeMS mol2AoS({11.5, 11.5, 11.5}, {0, 0, 0}, 34, 3, &molprop);

  std::array<double, 3> PartPos1 = {0.1, 0, 0};
  std::array<double, 3> PartPos2 = {-0.1, 0, 0};

  molprop.addParticle(PartPos1, 34, -0.1);
  molprop.addParticle(PartPos2, 34, 0.1);

  CoulombFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fCoulomb(2 * CutOff);

  ObjectSoA->addParticle(mol1SoA);
  ObjectSoA->addParticle(mol2SoA);

  ObjectAoS->addParticle(mol1AoS);
  ObjectAoS->addParticle(mol2AoS);

  ObjectSoA->iteratePairwise(&fCoulomb, soa);
  ObjectAoS->iteratePairwise(&fCoulomb, aos);

  mol1SoA = *ObjectSoA->begin();
  mol2SoA = *(++ObjectSoA->begin());

  mol1AoS = *ObjectAoS->begin();
  mol2AoS = *(++ObjectAoS->begin());

  // compare the calculated forces of the first molecule
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[0] - mol1AoS.getF()[0]) / mol1AoS.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[1] - mol1AoS.getF()[1]) / mol1AoS.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[2] - mol1AoS.getF()[2]) / mol1AoS.getF()[2]) < 1e-12);

  // compare the calculated forces of the second molecule
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[0] - mol2AoS.getF()[0]) / mol2AoS.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[1] - mol2AoS.getF()[1]) / mol2AoS.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[2] - mol2AoS.getF()[2]) / mol2AoS.getF()[2]) < 1e-12);

  // compare the calculated torques of the first molecule
  CPPUNIT_ASSERT(abs((mol1SoA.getA()[0] - mol1AoS.getA()[0]) / mol1AoS.getA()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getA()[1] - mol1AoS.getA()[1]) / mol1AoS.getA()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getA()[2] - mol1AoS.getA()[2]) / mol1AoS.getA()[2]) < 1e-12);

  // compare the calculated torques of the second molecule
  CPPUNIT_ASSERT(abs((mol2SoA.getA()[0] - mol2AoS.getA()[0]) / mol2AoS.getA()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getA()[1] - mol2AoS.getA()[1]) / mol2AoS.getA()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getA()[2] - mol2AoS.getA()[2]) / mol2AoS.getA()[2]) < 1e-12);
}

void SoATest::SoACoulombSameCellTest() {
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>* ObjectSoA =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger
  AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>* ObjectAoS =
      new AutoPas<MoleculeMS, FullParticleCell<MoleculeMS>>();  // needed to avoid segmentation fault from logger

  setUp();

  std::array<double, 3> BoxMax = {10, 10, 10};
  ObjectSoA->init(BoxMax, 2 * CutOff, linkedCells);
  ObjectAoS->init(BoxMax, 2 * CutOff, linkedCells);

  int ID = 0;

  // create molecule properties
  MoleculeType molprop(ID);

  // create molecule
  MoleculeMS mol1SoA({1.0, 1.0, 1.0}, {0, 0, 0}, 34, 0, &molprop);
  MoleculeMS mol2SoA({4.0, 4.0, 4.0}, {0, 0, 0}, 34, 1, &molprop);

  MoleculeMS mol1AoS({1.0, 1.0, 1.0}, {0, 0, 0}, 34, 2, &molprop);
  MoleculeMS mol2AoS({4.0, 4.0, 4.0}, {0, 0, 0}, 34, 3, &molprop);

  std::array<double, 3> PartPos1 = {0.1, 0, 0};
  std::array<double, 3> PartPos2 = {-0.1, 0, 0};

  molprop.addParticle(PartPos1, 34, -0.1);
  molprop.addParticle(PartPos2, 34, 0.1);

  CoulombFunctor<MoleculeMS, FullParticleCell<MoleculeMS>> fCoulomb(2 * CutOff);

  ObjectSoA->addParticle(mol1SoA);
  ObjectSoA->addParticle(mol2SoA);

  ObjectAoS->addParticle(mol1AoS);
  ObjectAoS->addParticle(mol2AoS);

  ObjectSoA->iteratePairwise(&fCoulomb, soa);
  ObjectAoS->iteratePairwise(&fCoulomb, aos);

  mol1SoA = *ObjectSoA->begin();
  mol2SoA = *(++ObjectSoA->begin());

  mol1AoS = *ObjectAoS->begin();
  mol2AoS = *(++ObjectAoS->begin());

  // compare the calculated forces of the first molecule
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[0] - mol1AoS.getF()[0]) / mol1AoS.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[1] - mol1AoS.getF()[1]) / mol1AoS.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getF()[2] - mol1AoS.getF()[2]) / mol1AoS.getF()[2]) < 1e-12);

  // compare the calculated forces of the second molecule
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[0] - mol2AoS.getF()[0]) / mol2AoS.getF()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[1] - mol2AoS.getF()[1]) / mol2AoS.getF()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getF()[2] - mol2AoS.getF()[2]) / mol2AoS.getF()[2]) < 1e-12);

  // compare the calculated torques of the first molecule
  CPPUNIT_ASSERT(abs((mol1SoA.getA()[0] - mol1AoS.getA()[0]) / mol1AoS.getA()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getA()[1] - mol1AoS.getA()[1]) / mol1AoS.getA()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol1SoA.getA()[2] - mol1AoS.getA()[2]) / mol1AoS.getA()[2]) < 1e-12);

  // compare the calculated torques of the second molecule
  CPPUNIT_ASSERT(abs((mol2SoA.getA()[0] - mol2AoS.getA()[0]) / mol2AoS.getA()[0]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getA()[1] - mol2AoS.getA()[1]) / mol2AoS.getA()[1]) < 1e-12);
  CPPUNIT_ASSERT(abs((mol2SoA.getA()[2] - mol2AoS.getA()[2]) / mol2AoS.getA()[2]) < 1e-12);
}
