/*
 * XYZWriter.h
 *
 *  Created on: 01.03.2010
 *      Author: eckhardw
 */

#ifndef XYZWRITER_H_
#define XYZWRITER_H_

#include <fstream>
#include <list>
#include "../../mdutils.h"
#include "../MoleculeMS.h"

namespace outputWriter {

class XYZWriter {
 public:
  XYZWriter();

  virtual ~XYZWriter();

  template <class Container>
  void plotParticles(Container* particles, const std::string& filename, int iteration);
};

}  // namespace outputWriter

#endif /* XYZWRITER_H_ */
