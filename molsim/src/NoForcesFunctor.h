/* GravityFunctor.h
 *
 *  Created on: Apr 6, 2018
 *      Author: raffi
 */

#ifndef SRC_NOFORCESFUNCTOR_H_
#define SRC_NOFORCESFUNCTOR_H_

#include "MoleculeMS.h"
#include "autopas/autopasIncludes.h"

#include <assert.h>
#include <array>
#include <cmath>

using namespace autopas;

template <class Particle, class ParticleCell, class SoAArraysType = typename Particle::SoAArraysType>
class NoForcesFunctor : public Functor<Particle, ParticleCell, SoAArraysType> {
 public:
  /**@brief Constructor of the functor
   * @param cut-off radius
   */
  NoForcesFunctor<Particle, ParticleCell, SoAArraysType>(double rcut) {
    r_cutoff = rcut;
    r_cutoff_square = rcut * rcut;
  }

  /**
   * @brief Functor for arrays of structures (AoS).
   *
   * This functor should calculate the forces or any other pair-wise interaction
   * between two particles.
   * This should include a cutoff check if needed!
   */
  void AoSFunctor(Particle &i, Particle &j, bool newton3 = true) override {
    // do nothing :D
  }

  /**TODO
   * @brief Functor for structure of arrays (SoA)
   *
   * This functor should calculate the forces or any other pair-wise interaction
   * between all particles in soa.
   * This should include a cutoff check if needed!
   *
   * @param soa Structure of arrays
   */
  void SoAFunctor(SoA<SoAArraysType> &soa, bool newton3 = true) override {}

  /**TODO
   * @brief Functor for structure of arrays (SoA)
   *
   * This functor should calculate the forces or any other pair-wise interaction
   * between all particles of soa1 and soa2.
   * This should include a cutoff check if needed!
   *
   * @param soa1 First structure of arrays.
   * @param soa2 Second structure of arrays.
   */
  void SoAFunctor(SoA<SoAArraysType> &soa1, SoA<SoAArraysType> &soa2, bool newton3 = true) override {}

  void SoAFunctor(SoA<SoAArraysType> &soa,
                  const std::vector<std::vector<size_t, autopas::AlignedAllocator<size_t>>> &neighbourList,
                  size_t iFrom, size_t iTo, bool newton3 = true) override {}
  /**TODO
   * @brief Copies the AoS data of the given cell in the given soa.
   *
   * @param cell Cell from where the data is loaded.
   * @param soa  Structure of arrays where the data is copied to.
   */
  AUTOPAS_FUNCTOR_SOALOADER(cell, _soa, offset, {})

  /**TODO
   * @brief Copies the data stored in the soa in the cell.
   *
   * @param cell Cell where the data should be stored.
   * @param soa  Structure of arrays from where the data is loaded.
   */
  AUTOPAS_FUNCTOR_SOAEXTRACTOR(cell, _soa, offset, {})

 private:
  double r_cutoff;
  double r_cutoff_square;
};

#endif /* SRC_NoForcesFUNCTOR_H_ */
