/*
 * ConstantFunctor.h
 *
 *  Created on: Jul 1, 2018
 *      Author: raffi
 */

#ifndef SRC_CONSTANTFUNCTOR_H_
#define SRC_CONSTANTFUNCTOR_H_

#include "MoleculeMS.h"
#include "SingleFunctor.h"
#include "autopas/autopasIncludes.h"

#include <assert.h>
#include <array>
#include <cmath>

using namespace autopas;

template <class Particle>
class ConstantFunctor : SingleFunctor<Particle> {
 public:
  // apply constant forces to specific particles

  ConstantFunctor<Particle>() {
    endtime = 0;
    force_status = false;
  }

  ~ConstantFunctor<Particle>() {}

  void Functor(Particle& p) override {
    if (!force_status) return;

    if (forceMap.find(p.getID()) != forceMap.end()) p.addF(forceMap[p.getID()]);
  }

  /*
   * add the particle with the given index to the force map
   * @param pair pair of index and force array to be inserted
   */
  void addParticle(std::pair<unsigned long, std::array<double, 3>> pair) { forceMap.insert(pair); }

  void setEndTime(double t) { endtime = t; }

  double getEndTime() { return endtime; }

  void setForceStatus(bool status) { force_status = status; }

  bool getForceStatus() { return force_status; }

 private:
  std::map<unsigned long, std::array<double, 3>> forceMap;
  double endtime;
  bool force_status;
};

#endif /* CONSTANTFUNCTOR_H_ */
