/*
 * MoleculeType.h
 *
 *  Created on: Dec 12, 2017
 *      Author: jan
 */

#ifndef MOLECULETYPE_H_
#define MOLECULETYPE_H_

#include "ParticleType.h"
#include "autopas/autopasIncludes.h"

#include <array>
#include <vector>

/** @brief Class that defines properties of a Molecule type
 *
 */
class MoleculeType {
 private:
  // general properties of the molecule
  int NumPart;
  bool fixed;

  // true if the inertia tensor shall be used in its principal axis representation
  bool enable_pa;

  // id is a unique index of this molecule type
  int id;

  // i is the second moment of inertia of the molecule, needed to compute the angular acceleration from the torque
  // (I*alpha = torque) the components of the tensor are : i = {xx, yy, zz, xy, yz, xz} there are only 6 components since
  // the matrix is symmetric
  std::array<double, 6> i;

  // this is he inverse of the inertia tensor I
  std::array<double, 6> inv_i;

  // stores the eigenvalues of I (along the principal axis)
  std::array<double, 3> i_pa;

  // stores the inverses of the eigenvalues of I (along the principal axis)
  std::array<double, 3> inv_i_pa;

  // rotation matrix to orient the molecule in the frame of the principal axis
  // eigenvectors of I
  std::array<double, 9> rot_pa;

  // stores the type of each particle in the molecule (to get specific data of each particle, such as mass of sigma)
  std::vector<int> types;
  std::vector<int> types_id;

  // stores the offset of each particle from the mass center, one vector per dimension
  std::vector<double> posX;
  std::vector<double> posY;
  std::vector<double> posZ;

  // stores the values of sigma, mass, Hpart and truncation radius for each particle in the molecule
  std::vector<double> Sigmapart;
  std::vector<double> Chargepart;
  std::vector<double> Hpart;
  std::vector<double> Masspart;
  std::vector<double> RtruncLJpart;

  // stores the dipole in the unrotated state
  std::array<double, 3> dipole;

 public:
  /** Constructor
   * @param NumPart number of particles in this molecule
   * @param fixed if true, the particle is spatially fixed and cannot move
   */
  MoleculeType(unsigned long id = 0, bool fixed = false);
  virtual ~MoleculeType();

  /*
   * compute the dipole based on the charges and site poistions stored in the particle
   */
  void computeDipole();
  /*
   * add a Particle to the molecule
   * @param pos position of the particle relative to the mass center
   * @param type of the particle to get its properties through the class ParticleType
   */
  void addParticle(std::array<double, 3> pos, int type, double partial_charge);

  /**
   * returns the moment of inertia of this molecule as inertia tensor.
   * @return moment of inertia
   */
  std::array<double, 6> getI();

  /**
   * gives the inverse of he inertia tensor
   * @return inverse of inertia tensor I
   */
  std::array<double, 6> getInvI();
  /*
   * returns the principal values of I
   * @return array containing the principal values
   */
  std::array<double, 3> getIPA();
  /**
   * returns the inverses of the principal values of I
   */
  std::array<double, 3> getInvIPA();
  /**
   * returns whether principal axis shall be used or not
   */
  bool& getEnablePA();
  /**
   * rotates a vector into the principal axis frame
   * @param v vector to rotate
   */
  void RotatePA(std::array<double, 3>& v);
  /**
   * rotates a vector back from the principal axis frame to the initial coordinate system
   * @param v vector to rotate
   */
  void RotateBackPA(std::array<double, 3>& v);
  /*
   *returns the ID of the given molecule type
   */
  int& getID();

  /*
   * return the number of particles in the molecule
   */
  int getNumPart();
  /*
   * get the vector of all X - positions
   */
  std::vector<double> getPosX();

  /*
   * get the vector of all Y - positions
   */
  std::vector<double> getPosY();

  /*
   * get the vector of all Z - positions
   */
  std::vector<double> getPosZ();

  /*
   * get all sigmas
   */
  std::vector<double> getSigmapart();

  /*
   * get all charges
   */
  std::vector<double> getChargepart();

  /*
   * get all zero-force distances
   */
  std::vector<double> getHpart();

  /*
   * get all masses
   */
  std::vector<double> getMasspart();

  /*
   * get all truncation radia
   */
  std::vector<double> getRtruncLJpart();

  /*
   * get the vector of all types
   */
  std::vector<int> getTypes();
  /*
   * get the vector of all unique type IDs
   */
  std::vector<int> getTypesID();

  /*
   * get the dipole in the initial orientation
   */
  std::array<double, 3> getDipole();

  /*
   * check if Molecule is movable
   * @return true if Molecule is unmovable
   */
  bool getFixed();
};

inline int& MoleculeType::getID() { return id; }
inline std::array<double, 6> MoleculeType::getI() { return i; }
inline std::array<double, 6> MoleculeType::getInvI() { return inv_i; }
inline bool& MoleculeType::getEnablePA() { return enable_pa; }
inline bool MoleculeType::getFixed() { return fixed; }
inline int MoleculeType::getNumPart() { return NumPart; }
inline std::vector<double> MoleculeType::getPosX() { return posX; }
inline std::vector<double> MoleculeType::getPosY() { return posY; }
inline std::vector<double> MoleculeType::getPosZ() { return posZ; }
inline std::vector<int> MoleculeType::getTypes() { return types; }
inline std::vector<int> MoleculeType::getTypesID() { return types_id; }
inline std::vector<double> MoleculeType::getSigmapart() { return Sigmapart; }
inline std::vector<double> MoleculeType::getChargepart() { return Chargepart; }
inline std::vector<double> MoleculeType::getHpart() { return Hpart; }

inline std::vector<double> MoleculeType::getMasspart() { return Masspart; }

inline std::vector<double> MoleculeType::getRtruncLJpart() { return RtruncLJpart; }
inline std::array<double, 3> MoleculeType::getDipole() { return dipole; }
inline std::array<double, 3> MoleculeType::getIPA() { return i_pa; }

inline std::array<double, 3> MoleculeType::getInvIPA() { return inv_i_pa; }

#endif /* MOLECULETYPE_H_ */
