/* MembraneFunctor.h
 *
 *  Created on: Apr 6, 2018
 *      Author: raffi
 */

#ifndef SRC_MEMBRANEFUNCTOR_H_
#define SRC_MEMBRANEFUNCTOR_H_

#include "MoleculeMS.h"
#include "SingleFunctor.h"
#include "autopas/autopasIncludes.h"

#include <assert.h>
#include <array>
#include <cmath>

const double Sqrt2 = 1.41421356237;

using namespace autopas;

template <class Particle>
class MembraneFunctor : SingleFunctor<Particle> {
 public:
  MembraneFunctor<Particle>() {
    radius = 0;
    stiffness = 0;
    MembraneStatus = false;
  }

  ~MembraneFunctor<Particle>() {}

  /**
   * @brief This functor should calculate the membrane forces
   * between a molecule and its direct and diagonal neighbours
   * This should include a cutoff check if needed!
   *
   * @param p the considered Particle
   */
  void Functor(Particle& p) override {
    ArrayMath am;

    if (!MembraneStatus) return;

    for (int n = 0; n < 2; n++) {
      if (p.getDirectNeighbours()[n] != nullptr) {
        Particle& pother = *p.getDirectNeighbours()[n];

        // compute direct potential
        std::array<double, 3> dir = am.sub(p.getR(), pother.getR());
        double norm2 = am.dot(dir, dir);
        std::array<double, 3> f = am.mulScalar(dir, stiffness * (1 - radius / norm2));

        // add the new force
        p.subF(f);
        pother.addF(f);
      }
    }

    // apply potentail to diagonal neighbours
    for (int n = 0; n < 2; n++) {
      if (p.getDiagonalNeighbours()[n] != nullptr) {
        Particle& pother = *p.getDiagonalNeighbours()[n];

        // compute diagonal potential
        std::array<double, 3> dir = am.sub(p.getR(), pother.getR());
        double norm2 = am.dot(dir, dir);
        std::array<double, 3> f = am.mulScalar(dir, stiffness * (1 - Sqrt2 * radius / norm2));

        // add the new force
        p.subF(f);
        pother.addF(f);
      }
    }
  }

  /*
   * set the membrane parameters
   * @param k stiffness of the membrane
   * @param r_zero unstressed distance between molecules
   *
   */
  void SetParameters(double k, double r_zero, bool status) {
    stiffness = k;
    radius = r_zero;
    MembraneStatus = status;
  }

  bool getMembraneStatus() { return MembraneStatus; }

 private:
  double stiffness;
  double radius;
  bool MembraneStatus;
};

#endif /* SRC_MEMBRANEJONESFUNCTOR_H_ */
