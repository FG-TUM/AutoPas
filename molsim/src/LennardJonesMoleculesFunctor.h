/* GravityFunctor.h
 *
 *  Created on: May 28, 2018
 *      Author: raffi
 */

#ifndef SRC_LENNARDJONESMOLECULESFUNCTOR_H_
#define SRC_LENNARDJONESMOLECULESFUNCTOR_H_

#include "../mdutils.h"
#include "HelperFunctions.h"
#include "MoleculeMS.h"

#include <assert.h>
#include <array>
#include <cmath>

using namespace autopas;

template <class Particle, class ParticleCell, class SoAArraysType = typename Particle::SoAArraysType>
class LennardJonesMoleculesFunctor : public Functor<Particle, ParticleCell, SoAArraysType> {
 public:
  /**@brief Constructor of the functor
   * @param cut-off radius
   */
  LennardJonesMoleculesFunctor<Particle, ParticleCell, SoAArraysType>(double rcut) {
    r_cutoff = rcut;
    r_cutoff_square = rcut * rcut;
  }

  /**
   * @brief Functor for arrays of structures (AoS).
   *
   * This functor should calculate the forces or any other pair-wise interaction
   * between two particles.
   * This should include a cutoff check if needed!
   */
  void AoSFunctor(Particle &i, Particle &j, bool newton3 = true) override {
    ArrayMath am;

    std::array<double, 3> r = am.sub(i.getR(), j.getR());

    double rabs2 = am.dot(r, r);
    assert(!(rabs2 == 0.0));

    // save the old angular momentum
    i.getOldA() = i.getA();
    j.getOldA() = j.getA();

    std::array<double, 3> zero = {0, 0, 0};

    // variable to store the newly computed torque
    std::array<double, 3> iTorque = zero;
    std::array<double, 3> jTorque = zero;

    // get the used properties of the particles to avoid multiple function calls
    std::vector<double> iSigma = i.getSigmaPart();
    std::vector<double> jSigma = j.getSigmaPart();
    std::vector<int> iTypeID = i.getTypeIDPart();
    std::vector<int> jTypeID = j.getTypeIDPart();

    std::vector<double> iPosX = i.getRealPosX();
    std::vector<double> iPosY = i.getRealPosY();
    std::vector<double> iPosZ = i.getRealPosZ();
    std::vector<double> jPosX = j.getRealPosX();
    std::vector<double> jPosY = j.getRealPosY();
    std::vector<double> jPosZ = j.getRealPosZ();

    if (rabs2 < r_cutoff_square) {
      for (int n = 0; n < i.getNumPart(); ++n) {
        for (int m = 0; m < j.getNumPart(); ++m) {
          double sigma = (iSigma[n] + jSigma[m]) / 2;
          double epsilon = ParticleType::getEpsilon(iTypeID[n], jTypeID[m]);

          std::array<double, 3> iPos = {iPosX[n], iPosY[n], iPosZ[n]};
          std::array<double, 3> jPos = {jPosX[m], jPosY[m], jPosZ[m]};
          std::array<double, 3> rPart = am.sub(iPos, jPos);

          double rabs2Part = am.dot(rPart, rPart);
          assert(!(rabs2Part == 0.0));

          double fraction2 = sigma * sigma / rabs2Part;
          double fraction6 = fraction2 * fraction2 * fraction2;
          double fraction12 = fraction6 * fraction6;

          std::array<double, 3> addforce = am.mulScalar(rPart, 24 * epsilon / rabs2Part * (fraction6 - 2 * fraction12));

          j.addF(addforce);
          i.subF(addforce);

          iTorque = am.add(iTorque, crossProduct(am.sub(i.getR(), iPos), addforce));
          jTorque = am.add(jTorque, crossProduct(am.sub(jPos, j.getR()), addforce));
        }
      }
    }
    i.setA(iTorque);
    j.setA(jTorque);
  }

  /**TODO
   * @brief Functor for structure of arrays (SoA)
   *
   * This functor should calculate the forces or any other pair-wise interaction
   * between all particles in soa.
   * This should include a cutoff check if needed!
   *
   * @param soa Structure of arrays
   */
  void SoAFunctor(SoA<SoAArraysType> &soa, bool newton3 = true) override {
    if (soa.getNumParticles() == 0) return;

    unsigned long *const __restrict__ ID = soa.template begin<MoleculeMS::id>();

    double *const __restrict__ posX = soa.template begin<MoleculeMS::posX>();
    double *const __restrict__ posY = soa.template begin<MoleculeMS::posY>();
    double *const __restrict__ posZ = soa.template begin<MoleculeMS::posZ>();

    double *const __restrict__ forceX = soa.template begin<MoleculeMS::forceX>();
    double *const __restrict__ forceY = soa.template begin<MoleculeMS::forceY>();
    double *const __restrict__ forceZ = soa.template begin<MoleculeMS::forceZ>();

    int *const __restrict__ uniquetypeID = soa.template begin<MoleculeMS::uniquetypeID>();
    double *const __restrict__ sigma = soa.template begin<MoleculeMS::sigma>();
    double *const __restrict__ epsilon = soa.template begin<MoleculeMS::epsilon>();

    for (unsigned int i = 0; i < soa.getNumParticles(); ++i) {
      double fxacc = 0;
      double fyacc = 0;
      double fzacc = 0;

#pragma omp simd reduction(+ : fxacc, fyacc, fzacc)
      for (unsigned int j = i + 1; j < soa.getNumParticles(); ++j) {
        if (ID[i] == ID[j]) continue;

        const double drx = posX[i] - posX[j];
        const double dry = posY[i] - posY[j];
        const double drz = posZ[i] - posZ[j];

        const double drx2 = drx * drx;
        const double dry2 = dry * dry;
        const double drz2 = drz * drz;

        const double dr2 = drx2 + dry2 + drz2;

        if (dr2 > r_cutoff_square) continue;

        // apply mixing rules
        const double SIGMA = (sigma[i] + sigma[j]) / 2;

        double EPSILON = 0;
        if (epsilon[i] == epsilon[j]) {
          EPSILON = epsilon[i];
        } else if (epsilon[j] > epsilon[i]) {
          EPSILON = epsilon_pairs[uniquetypeID[j] * (uniquetypeID[j] - 1) / 2 + uniquetypeID[i]];
        } else {
          EPSILON = epsilon_pairs[uniquetypeID[i] * (uniquetypeID[i] - 1) / 2 + uniquetypeID[j]];
        }

        const double invdr2 = 1. / dr2;
        const double frac2 = SIGMA * SIGMA * invdr2;
        const double frac6 = frac2 * frac2 * frac2;
        const double frac12 = frac6 * frac6;
        const double factor = 24 * EPSILON * (frac6 - 2 * frac12) * invdr2;

        const double fx = drx * factor;
        const double fy = dry * factor;
        const double fz = drz * factor;

        fxacc -= fx;
        fyacc -= fy;
        fzacc -= fz;

        forceX[j] += fx;
        forceY[j] += fy;
        forceZ[j] += fz;
      }

      forceX[i] += fxacc;
      forceY[i] += fyacc;
      forceZ[i] += fzacc;
    }
  }

  /**
   * @brief Functor for structure of arrays (SoA)
   *
   * This functor should calculate the forces or any other pair-wise interaction
   * between all particles of soa1 and soa2.
   * This should include a cutoff check if needed!
   *
   * @param soa1 First structure of arrays.
   * @param soa2 Second structure of arrays.
   */
  void SoAFunctor(SoA<SoAArraysType> &soa1, SoA<SoAArraysType> &soa2, bool newton3 = true) override {
    if (soa1.getNumParticles() == 0 || soa2.getNumParticles() == 0) return;

    // first cell
    unsigned long *const __restrict__ ID1 = soa1.template begin<MoleculeMS::id>();

    double *const __restrict__ posX1 = soa1.template begin<MoleculeMS::posX>();
    double *const __restrict__ posY1 = soa1.template begin<MoleculeMS::posY>();
    double *const __restrict__ posZ1 = soa1.template begin<MoleculeMS::posZ>();

    double *const __restrict__ forceX1 = soa1.template begin<MoleculeMS::forceX>();
    double *const __restrict__ forceY1 = soa1.template begin<MoleculeMS::forceY>();
    double *const __restrict__ forceZ1 = soa1.template begin<MoleculeMS::forceZ>();

    int *const __restrict__ uniquetypeID1 = soa1.template begin<MoleculeMS::uniquetypeID>();
    double *const __restrict__ sigma1 = soa1.template begin<MoleculeMS::sigma>();
    double *const __restrict__ epsilon1 = soa1.template begin<MoleculeMS::epsilon>();

    // second cell
    unsigned long *const __restrict__ ID2 = soa2.template begin<MoleculeMS::id>();

    double *const __restrict__ posX2 = soa2.template begin<MoleculeMS::posX>();
    double *const __restrict__ posY2 = soa2.template begin<MoleculeMS::posY>();
    double *const __restrict__ posZ2 = soa2.template begin<MoleculeMS::posZ>();

    double *const __restrict__ forceX2 = soa2.template begin<MoleculeMS::forceX>();
    double *const __restrict__ forceY2 = soa2.template begin<MoleculeMS::forceY>();
    double *const __restrict__ forceZ2 = soa2.template begin<MoleculeMS::forceZ>();

    int *const __restrict__ uniquetypeID2 = soa2.template begin<MoleculeMS::uniquetypeID>();
    double *const __restrict__ sigma2 = soa2.template begin<MoleculeMS::sigma>();
    double *const __restrict__ epsilon2 = soa2.template begin<MoleculeMS::epsilon>();

    for (unsigned int i = 0; i < soa1.getNumParticles(); ++i) {
      double fxacc = 0;
      double fyacc = 0;
      double fzacc = 0;

#pragma omp simd reduction(+ : fxacc, fyacc, fzacc)
      for (unsigned int j = 0; j < soa2.getNumParticles(); ++j) {
        if (ID1[i] == ID2[j]) continue;

        const double drx = posX1[i] - posX2[j];
        const double dry = posY1[i] - posY2[j];
        const double drz = posZ1[i] - posZ2[j];

        const double drx2 = drx * drx;
        const double dry2 = dry * dry;
        const double drz2 = drz * drz;

        const double dr2 = drx2 + dry2 + drz2;

        if (dr2 > r_cutoff_square) continue;

        // apply mixing rules
        const double SIGMA = (sigma1[i] + sigma2[j]) / 2;

        double EPSILON = 0;
        if (epsilon1[i] == epsilon2[j]) {
          EPSILON = epsilon1[i];
        } else if (epsilon2[j] > epsilon1[i]) {
          EPSILON = epsilon_pairs[uniquetypeID2[j] * (uniquetypeID2[j] - 1) / 2 + uniquetypeID1[i]];
        } else {
          EPSILON = epsilon_pairs[uniquetypeID1[i] * (uniquetypeID1[i] - 1) / 2 + uniquetypeID2[j]];
        }

        const double invdr2 = 1. / dr2;
        const double frac2 = SIGMA * SIGMA * invdr2;
        const double frac6 = frac2 * frac2 * frac2;
        const double frac12 = frac6 * frac6;
        const double factor = 24 * EPSILON * (frac6 - 2 * frac12) * invdr2;

        const double fx = drx * factor;
        const double fy = dry * factor;
        const double fz = drz * factor;

        fxacc -= fx;
        fyacc -= fy;
        fzacc -= fz;

        forceX2[j] += fx;
        forceY2[j] += fy;
        forceZ2[j] += fz;
      }

      forceX1[i] += fxacc;
      forceY1[i] += fyacc;
      forceZ1[i] += fzacc;
    }
  }
  /**
   * functor used for Verlet-lists
   */
  void SoAFunctor(SoA<SoAArraysType> &soa,
                  const std::vector<std::vector<size_t, autopas::AlignedAllocator<size_t>>> &neighbourList,
                  size_t iFrom, size_t iTo, bool newton3 = true) override {
    auto numParts = soa.getNumParticles();

    if (numParts == 0)
      ;

    double *const __restrict__ posX = soa.template begin<MoleculeMS::posX>();
    double *const __restrict__ posY = soa.template begin<MoleculeMS::posY>();
    double *const __restrict__ posZ = soa.template begin<MoleculeMS::posZ>();

    double *const __restrict__ forceX = soa.template begin<MoleculeMS::forceX>();
    double *const __restrict__ forceY = soa.template begin<MoleculeMS::forceY>();
    double *const __restrict__ forceZ = soa.template begin<MoleculeMS::forceZ>();

    int *const __restrict__ uniquetypeID = soa.template begin<MoleculeMS::uniquetypeID>();
    double *const __restrict__ sigma = soa.template begin<MoleculeMS::sigma>();
    double *const __restrict__ epsilon = soa.template begin<MoleculeMS::epsilon>();

    for (unsigned int i = iFrom; i < iTo; ++i) {
      double fxacc = 0;
      double fyacc = 0;
      double fzacc = 0;

      const size_t listSizeI = neighbourList[i].size();
      const size_t *const __restrict__ currentList = neighbourList[i].data();

#ifdef __AV512F__
      const size_t vecsize = 16;
#else
      const size_t vecsize = 12;
#endif
      size_t joff = 0;

      if (listSizeI >= vecsize) {
        alignas(64) std::array<double, vecsize> xtmp, ytmp, ztmp, sigmatmp, epsilontmp, typeIDtmp, xArr, yArr, zArr,
            fxArr, fyArr, fzArr, sigmaArr, epsilonArr, typeIDArr;

        for (size_t tmpj = 0; tmpj < vecsize; tmpj++) {
          xtmp[tmpj] = posX[i];
          ytmp[tmpj] = posY[i];
          ztmp[tmpj] = posZ[i];

          sigmatmp[tmpj] = sigma[i];
          epsilontmp[tmpj] = epsilon[i];
          typeIDtmp[tmpj] = uniquetypeID[i];
        }

        // loop over the verlet list
        for (; joff < listSizeI - vecsize + 1; joff += vecsize) {
#pragma omp simd safelen(vecsize)
          for (size_t tmpj = 0; tmpj < vecsize; tmpj++) {
            xArr[tmpj] = posX[currentList[joff + tmpj]];
            yArr[tmpj] = posY[currentList[joff + tmpj]];
            zArr[tmpj] = posZ[currentList[joff + tmpj]];

            sigmaArr[tmpj] = sigma[currentList[joff + tmpj]];
            epsilonArr[tmpj] = epsilon[currentList[joff + tmpj]];
            typeIDArr[tmpj] = uniquetypeID[currentList[joff + tmpj]];
          }

#pragma omp simd reduction(+ : fxacc, fyacc, fzacc) safelen(vecsize)
          for (size_t j = 0; j < vecsize; j++) {
            const double drx = xtmp[j] - xArr[j];
            const double dry = ytmp[j] - yArr[j];
            const double drz = ztmp[j] - zArr[j];

            const double drx2 = drx * drx;
            const double dry2 = dry * dry;
            const double drz2 = drz * drz;

            const double dr2 = drx2 + dry2 + drz2;

            if (dr2 > r_cutoff_square) continue;

            // apply mixing rules
            const double SIGMA = (sigmatmp[j] + sigmaArr[j]) / 2;

            double EPSILON = 0;
            if (epsilontmp[j] == epsilonArr[j]) {
              EPSILON = epsilonArr[j];
            } else if (epsilonArr[j] > epsilontmp[j]) {
              EPSILON = epsilon_pairs[typeIDArr[j] * (typeIDArr[j] - 1) / 2 + typeIDtmp[j]];
            } else {
              EPSILON = epsilon_pairs[typeIDtmp[j] * (typeIDtmp[j] - 1) / 2 + typeIDArr[j]];
            }

            const double invdr2 = 1. / dr2;
            const double frac2 = SIGMA * SIGMA * invdr2;
            const double frac6 = frac2 * frac2 * frac2;
            const double frac12 = frac6 * frac6;
            const double factor = 24 * EPSILON * (frac6 - 2 * frac12) * invdr2;

            const double fx = drx * factor;
            const double fy = dry * factor;
            const double fz = drz * factor;

            fxacc -= fx;
            fyacc -= fy;
            fzacc -= fz;

            fxArr[j] = fx;
            fyArr[j] = fy;
            fzArr[j] = fz;
          }

#pragma omp simd safelen(vecsize)
          for (size_t tmpj = 0; tmpj < vecsize; tmpj++) {
            const size_t j = currentList[joff + tmpj];
            forceX[j] += fxArr[tmpj];
            forceY[j] += fyArr[tmpj];
            forceZ[j] += fzArr[tmpj];
          }
        }
      }

      for (size_t jNeighIndex = joff; jNeighIndex < listSizeI; ++jNeighIndex) {
        size_t j = neighbourList[i][jNeighIndex];
        if (i == j) continue;

        const double drx = posX[i] - posX[j];
        const double dry = posY[i] - posY[j];
        const double drz = posZ[i] - posZ[j];

        const double drx2 = drx * drx;
        const double dry2 = dry * dry;
        const double drz2 = drz * drz;

        const double dr2 = drx2 + dry2 + drz2;

        if (dr2 > r_cutoff_square) continue;

        // apply mixing rules
        const double SIGMA = (sigma[i] + sigma[j]) / 2;

        double EPSILON = 0;
        if (epsilon[i] == epsilon[j]) {
          EPSILON = epsilon[i];
        } else if (epsilon[j] > epsilon[i]) {
          EPSILON = epsilon_pairs[uniquetypeID[j] * (uniquetypeID[j] - 1) / 2 + uniquetypeID[i]];
        } else {
          EPSILON = epsilon_pairs[uniquetypeID[i] * (uniquetypeID[i] - 1) / 2 + uniquetypeID[j]];
        }

        const double invdr2 = 1. / dr2;
        const double frac2 = SIGMA * SIGMA * invdr2;
        const double frac6 = frac2 * frac2 * frac2;
        const double frac12 = frac6 * frac6;
        const double factor = 24 * EPSILON * (frac6 - 2 * frac12) * invdr2;

        const double fx = drx * factor;
        const double fy = dry * factor;
        const double fz = drz * factor;

        fxacc -= fx;
        fyacc -= fy;
        fzacc -= fz;

        forceX[j] += fx;
        forceY[j] += fy;
        forceZ[j] += fz;
      }

      forceX[i] += fxacc;
      forceY[i] += fyacc;
      forceZ[i] += fzacc;
    }
  }

  /**TODO
   * @brief Copies the AoS data of the given cell in the given soa.
   *
   * @param cell Cell from where the data is loaded.
   * @param soa  Structure of arrays where the data is copied to.
   */
  AUTOPAS_FUNCTOR_SOALOADER(
      cell, _soa, offset,
      // count total number of sites in the cell
      unsigned count = 0;
      for (auto it = cell.begin(); it.isValid(); ++it) { count += it->getNumPart(); } _soa.resizeArrays(offset + count);

      if (cell.numParticles() == 0) return;

      unsigned long *const __restrict__ id = _soa.template begin<MoleculeMS::id>();

      double *const __restrict__ posX = _soa.template begin<MoleculeMS::posX>();
      double *const __restrict__ posY = _soa.template begin<MoleculeMS::posY>();
      double *const __restrict__ posZ = _soa.template begin<MoleculeMS::posZ>();

      double *const __restrict__ forceX = _soa.template begin<MoleculeMS::forceX>();
      double *const __restrict__ forceY = _soa.template begin<MoleculeMS::forceY>();
      double *const __restrict__ forceZ = _soa.template begin<MoleculeMS::forceZ>();

      int *const __restrict__ uniquetypeID = _soa.template begin<MoleculeMS::uniquetypeID>();

      double *const __restrict__ sigma = _soa.template begin<MoleculeMS::sigma>();

      double *const __restrict__ epsilon = _soa.template begin<MoleculeMS::epsilon>();

      size_t i = offset;

      for (auto it = cell.begin(); it.isValid(); ++it) {
        Particle &p = *it;

        const std::vector<double> x = p.getRealPosX();
        const std::vector<double> y = p.getRealPosY();
        const std::vector<double> z = p.getRealPosZ();

        for (int j = 0; j < p.getNumPart(); ++j) {
          id[i] = p.getID();
          posX[i] = x[j];
          posY[i] = y[j];
          posZ[i] = z[j];
          forceX[i] = p.getF()[0];
          forceY[i] = p.getF()[1];
          forceZ[i] = p.getF()[2];
          uniquetypeID[i] = p.getTypeIDPart()[j];
          sigma[i] = ParticleType::getSigma(uniquetypeID[i]);
          epsilon[i] = ParticleType::getEpsilon(uniquetypeID[i]);

          ++i;
        }
      })

  /**
   * @brief Copies the data stored in the soa in the cell.
   *
   * @param cell Cell where the data should be stored.
   * @param soa  Structure of arrays from where the data is loaded.
   */
  AUTOPAS_FUNCTOR_SOAEXTRACTOR(cell, _soa, offset,

                               if (_soa.getNumParticles() == 0) return;

                               double *const __restrict__ forceX = _soa.template begin<MoleculeMS::forceX>();
                               double *const __restrict__ forceY = _soa.template begin<MoleculeMS::forceY>();
                               double *const __restrict__ forceZ = _soa.template begin<MoleculeMS::forceZ>();

                               double *const __restrict__ posX = _soa.template begin<MoleculeMS::posX>();
                               double *const __restrict__ posY = _soa.template begin<MoleculeMS::posY>();
                               double *const __restrict__ posZ = _soa.template begin<MoleculeMS::posZ>();

                               auto it = cell.begin();

                               size_t i = offset; for (auto it = cell.begin(); it.isValid(); ++it) {
                                 auto R = it->getR();
                                 std::array<double, 3> fadd = {0, 0, 0};
                                 std::array<double, 3> torque = {0, 0, 0};

                                 for (int j = 0; j < it->getNumPart(); ++j) {
                                   // add forces
                                   fadd[0] += forceX[i];
                                   fadd[1] += forceY[i];
                                   fadd[2] += forceZ[i];

                                   // compute torques
                                   ArrayMath am;
                                   std::array<double, 3> siteR = {posX[i], posY[i], posZ[i]};
                                   std::array<double, 3> siteF = {forceX[i], forceY[i], forceZ[i]};
                                   torque = am.add(torque, crossProduct(am.sub(siteR, R), siteF));

                                   ++i;
                                 }
                                 it->getF() = fadd;
                                 it->getA() = torque;
                               })

  void setEpsilonPairs(std::vector<float> pairs) { epsilon_pairs = pairs; }

 private:
  double r_cutoff;
  double r_cutoff_square;
  std::vector<float> epsilon_pairs;
};

#endif /* SRC_LennardJonesMoleculesFunctor_H_ */
