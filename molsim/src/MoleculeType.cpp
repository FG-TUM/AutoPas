/*
 * MoleculeType.cpp
 *
 *  Created on: Dec 12, 2017
 *      Author: jan
 */

#include "MoleculeType.h"
#include <math.h>
#include <iostream>
#include "Eigenvalues.h"
#include "HelperFunctions.h"
#include "autopas/autopasIncludes.h"

MoleculeType::MoleculeType(unsigned long id, bool fixed) {
  this->NumPart = 0;
  this->fixed = fixed;
  this->id = id;
  this->i = {0, 0, 0, 0, 0, 0};
  this->inv_i = {0, 0, 0, 0, 0, 0};
  this->dipole = {0.0, 0.0, 0.0};
  this->i_pa = {0, 0, 0};
  this->inv_i_pa = {0, 0, 0};
  this->rot_pa = {0, 0, 0, 0, 0, 0, 0, 0, 0};
  this->enable_pa = true;
}

MoleculeType::~MoleculeType() {
  // TODO Auto-generated destructor stub
}

void MoleculeType::addParticle(std::array<double, 3> pos, int type, double partial_charge) {
  this->types.push_back(type);
  this->posX.push_back(pos[0]);
  this->posY.push_back(pos[1]);
  this->posZ.push_back(pos[2]);
  int ID = ParticleType::getID(type);
  this->types_id.push_back(ID);
  double m = ParticleType::getM(ID);
  this->Sigmapart.push_back(ParticleType::getSigma(ID));
  this->Chargepart.push_back(ParticleType::getCharge(ID) + partial_charge);
  this->Masspart.push_back(m);
  this->RtruncLJpart.push_back(ParticleType::getRtruncLJ(ID));
  this->Hpart.push_back(ParticleType::getHpart(ID));
  this->NumPart++;

  // update the inertia tensor
  std::array<double, 6> I;
  I[0] = m * (pos[1] * pos[1] + pos[2] * pos[2]);
  I[1] = m * (pos[0] * pos[0] + pos[2] * pos[2]);
  I[2] = m * (pos[0] * pos[0] + pos[1] * pos[1]);
  I[3] = -m * pos[0] * pos[1];
  I[4] = -m * pos[1] * pos[2];
  I[5] = -m * pos[0] * pos[2];

  this->i = am.add(this->i, I);

  I = this->i;

  double det =
      I[0] * I[1] * I[2] + 2 * I[3] * I[4] * I[5] - I[0] * I[4] * I[4] - I[2] * I[3] * I[3] - I[1] * I[5] * I[5];

  std::array<double, 6> invI = {0, 0, 0, 0, 0, 0};
  if (det != 0) {
    // compute the inverse of the inertia tensor if its determinant is not zero
    invI[0] = I[2] * I[1] - I[4] * I[4];
    invI[1] = I[0] * I[2] - I[5] * I[5];
    invI[2] = I[0] * I[1] - I[3] * I[3];
    invI[3] = I[5] * I[4] - I[2] * I[3];
    invI[4] = I[5] * I[3] - I[0] * I[4];
    invI[5] = I[3] * I[4] - I[5] * I[1];

    invI = am.mulScalar(invI, 1.0 / det);
  }
  this->inv_i = invI;

  // compute the eigenvalues of I
  this->i_pa = getEigenValues(I);

  // compute the inverse of i_pa
  std::array<double, 3> invipa = {0, 0, 0};
  if (this->i_pa[0] != 0) invipa[0] = 1.0 / this->i_pa[0];
  if (this->i_pa[1] != 0) invipa[1] = 1.0 / this->i_pa[1];
  if (this->i_pa[2] != 0) invipa[2] = 1.0 / this->i_pa[2];
  this->inv_i_pa = invipa;

  // compute the normalized eigenvectors of I
  this->rot_pa = getEigenVectors(this->i_pa, I);
}

void MoleculeType::computeDipole() {
  double x = 0;
  double y = 0;
  double z = 0;
  for (int i = 0; i < this->NumPart; i++) {
    for (int j = i + 1; j < this->NumPart; j++) {
      x += (this->Chargepart[i] - this->Chargepart[j]) * (this->posX[i] - this->posX[j]);
      y += (this->Chargepart[i] - this->Chargepart[j]) * (this->posY[i] - this->posY[j]);
      z += (this->Chargepart[i] - this->Chargepart[j]) * (this->posZ[i] - this->posZ[j]);
    }
  }

  this->dipole = {x, y, z};
}
void MoleculeType::RotatePA(std::array<double, 3>& v) {
  std::array<double, 3> old_v = v;
  std::array<double, 9> eigvec = this->rot_pa;

  v[0] = eigvec[0] * old_v[0] + eigvec[3] * old_v[1] + eigvec[6] * old_v[2];
  v[1] = eigvec[1] * old_v[0] + eigvec[4] * old_v[1] + eigvec[7] * old_v[2];
  v[2] = eigvec[2] * old_v[0] + eigvec[5] * old_v[1] + eigvec[8] * old_v[2];
}

void MoleculeType::RotateBackPA(std::array<double, 3>& v) {
  std::array<double, 3> old_v = v;
  std::array<double, 9> eigvec = this->rot_pa;

  v[0] = eigvec[0] * old_v[0] + eigvec[1] * old_v[1] + eigvec[2] * old_v[2];
  v[1] = eigvec[3] * old_v[0] + eigvec[4] * old_v[1] + eigvec[5] * old_v[2];
  v[2] = eigvec[6] * old_v[0] + eigvec[7] * old_v[1] + eigvec[8] * old_v[2];
}
