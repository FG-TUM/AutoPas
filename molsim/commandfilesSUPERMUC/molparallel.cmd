#!/bin/bash

#SBATCH -o /home/hpc/pr63so/ga78jey2/AutoPas/molsim/AutoPasMS.%j.%N.out
#SBATCH -D /home/hpc/pr63so/ga78jey2/AutoPas/molsim
#SBATCH -J MolAutoPas
#SBATCH --clusters=mpp2
#SBATCH --ntasks=8
#SBATCH --get-user-env
#SBATCH --mem=800mb
#SBATCH --time=01:30:00
export OMP_NUM_THREADS=8
./MolSim setting-liquid.xml

