#!/bin/bash

#@ job_type = parallel
#@ class = test
#@ island_count=1
#@ node = 1
#@ tasks_per_node = 1
#@ job_name = worldrecord
#@ node_usage = not_shared
#@ wall_clock_limit = 0:30:00
#@ initialdir = $(home)/AutoPas/molsim
#@ output = worldrecord.$(schedd_host).$(jobid).out 
#@ error = worldrecord.$(schedd_host).$(jobid).err
#@ notification=complete
#@ notify_user=f.gratl@tum.de
#@ energy_policy_tag = my_energy_tag 
#@ minimize_time_to_solution = yes
#@ queue
. /etc/profile
. /etc/profile.d/modules.sh

export OMP_NUM_THREADS=16
export AUTOPAS_TRAVERSAL=SLICED
export KMP_AFFINITY="granularity=core,compact,1"

./MolSim ../inputfiles/setting-worldrecord2.5.xml
./MolSim ../inputfiles/setting-worldrecord3.5.xml
./MolSim ../inputfiles/setting-worldrecord5.0.xml


