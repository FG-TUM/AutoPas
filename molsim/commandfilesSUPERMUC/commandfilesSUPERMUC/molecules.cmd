#!/bin/bash

#@ job_type = parallel
#@ class = test
#@ island_count=1
#@ node = 1
#@ tasks_per_node = 1
#@ job_name = molecules
#@ node_usage = not_shared
#@ wall_clock_limit = 0:30:00
#@ initialdir = $(home)/AutoPas/molsim
#@ output = molecules.$(schedd_host).$(jobid).out 
#@ error = molecules.$(schedd_host).$(jobid).err
#@ notification=complete
#@ notify_user=f.gratl@tum.de
#@ energy_policy_tag = my_energy_tag 
#@ minimize_time_to_solution = yes
#@ queue
. /etc/profile
. /etc/profile.d/modules.sh

export OMP_NUM_THREADS=16
export AUTOPAS_TRAVERSAL=C08
export KMP_AFFINITY="granularity=core,compact,1"

#./MolSim ../inputfiles/setting-argon.xml
./MolSim ../inputfiles/setting-carbonMonoxyde.xml
./MolSim ../inputfiles/setting-water.xml
./MolSim ../inputfiles/setting-ammonia.xml
#./MolSim ../inputfiles/setting-methane.xml


