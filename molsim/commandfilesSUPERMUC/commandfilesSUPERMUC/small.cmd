#!/bin/bash

#@ job_type = parallel
#@ class = test
#@ island_count=1
#@ node = 1
#@ tasks_per_node = 1
#@ job_name = small
#@ node_usage = not_shared
#@ wall_clock_limit = 0:30:00
#@ initialdir = $(home)/AutoPas/molsim
#@ output = small.$(schedd_host).$(jobid).out 
#@ error = small.$(schedd_host).$(jobid).err
#@ notification=complete
#@ notify_user=f.gratl@tum.de
#@ energy_policy_tag = my_energy_tag 
#@ minimize_time_to_solution = yes
#@ queue
. /etc/profile
. /etc/profile.d/modules.sh

export OMP_NUM_THREADS=16
export AUTOPAS_TRAVERSAL=C08
export KMP_AFFINITY="granularity=core,compact,1"


#./MolSim ../inputfiles/setting-small-4.xml
#./MolSim ../inputfiles/setting-small-2.xml
#./MolSim ../inputfiles/setting-small1.xml
#./MolSim ../inputfiles/setting-small2.xml
#./MolSim ../inputfiles/setting-small3.xml
#./MolSim ../inputfiles/setting-small4.xml
#./MolSim ../inputfiles/setting-small6.xml
#./MolSim ../inputfiles/setting-small8.xml
#./MolSim ../inputfiles/setting-small12.xml
#./MolSim ../inputfiles/setting-small16.xml
#./MolSim ../inputfiles/setting-small20.xml
#./MolSim ../inputfiles/setting-small22.xml
#./MolSim ../inputfiles/setting-small24.xml
./MolSim ../inputfiles/setting-small32.xml
./MolSim ../inputfiles/setting-small64.xml



