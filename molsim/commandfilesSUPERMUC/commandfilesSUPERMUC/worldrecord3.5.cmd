#!/bin/bash

#@ job_type = parallel
#@ class = test
#@ island_count=1
#@ node = 1
#@ tasks_per_node = 1
#@ job_name = worldrecord3.5
#@ node_usage = not_shared
#@ wall_clock_limit = 0:30:00
#@ initialdir = $(home)/AutoPas/molsim
#@ output = worldrecord3.5real.$(schedd_host).$(jobid).out 
#@ error = worldrecord3.5real.$(schedd_host).$(jobid).err
#@ notification=complete
#@ notify_user=f.gratl@tum.de
#@ energy_policy_tag = my_energy_tag 
#@ minimize_time_to_solution = yes
#@ queue
. /etc/profile
. /etc/profile.d/modules.sh

#module load scorep
module load amplifier_xe
#module switch mpi.ibm mpi.intel

export OMP_NUM_THREADS=1
export AUTOPAS_TRAVERSAL=SLICED
export KMP_AFFINITY="granularity=core,compact,1"

#./MolSim ../inputfiles/setting-worldrecord3.5.xml
export OMP_NUM_THREADS=2
#./MolSim ../inputfiles/setting-worldrecord3.5.xml
export OMP_NUM_THREADS=4
#./MolSim ../inputfiles/setting-worldrecord3.5.xml
export OMP_NUM_THREADS=8
#./MolSim ../inputfiles/setting-worldrecord3.5.xml
#scorep -o ./MolSim ../inputfiles/setting-worldrecord3.5.xml
export OMP_NUM_THREADS=16
#amplxe-cl -collect hpc-performance -knob collect-memory-bandwidth=true -knob dram-bandwidth-limits=false ./MolSim ../inputfiles/setting-worldrecord3.5.xml 
amplxe-cl -collect hotspots -knob enable-user-tasks=true -knob analyze-openmp=true ./MolSim ../inputfiles/setting-worldrecord3.5.xml
#./MolSim ../inputfiles/setting-worldrecord3.5.xml
export OMP_NUM_THREADS=32
#./MolSim ../inputfiles/setting-worldrecord3.5.xml



