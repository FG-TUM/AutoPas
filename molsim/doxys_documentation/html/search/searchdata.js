var indexSectionsWithContent =
{
  0: "_abcdefghilmnopqrstuvwxyz~",
  1: "cdfgilmnpstuvx",
  2: "ox",
  3: "cefghlmnpsuvx",
  4: "_abcdefghilmnopqrstuvwxyz~",
  5: "_abcdefghilmnoprstuvxyz",
  6: "abcdefghilmnopqrstuvxyz",
  7: "abcfpv",
  8: "cdefghilmnoprsuv",
  9: "acdhpsx"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Macros"
};

