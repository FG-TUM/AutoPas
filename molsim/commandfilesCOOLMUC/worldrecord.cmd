#!/bin/bash

#SBATCH -o /home/hpc/pr63so/ga78jey2/AutoPas/molsim/WR.%j.%N.out
#SBATCH -D /home/hpc/pr63so/ga78jey2/AutoPas/molsim
#SBATCH -J WRAutoPas
#SBATCH --clusters=mpp2
#SBATCH --ntasks=112
#SBATCH --get-user-env
#SBATCH --time=08:00:00
export OMP_NUM_THREADS=28
export AUTOPAS_TRAVERSAL=C08 
./MolSim ../inputfiles/setting-worldrecord.xml 



