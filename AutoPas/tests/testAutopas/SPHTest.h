/**
 * @file SPHTest.h
 * @author seckler
 * @date 22.01.18
 */

#pragma once

#include <gtest/gtest.h>
#include "AutoPasTestBase.h"
#include "autopas/autopasIncludes.h"
#include "autopas/sph/autopassph.h"
#include "testingHelpers/RandomGenerator.h"

class SPHTest : public AutoPasTestBase {
  void SetUp() override{};

  void TearDown() override{};
};
