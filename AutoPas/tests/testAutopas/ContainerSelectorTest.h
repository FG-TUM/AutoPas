/**
 * @file ContainerSelectorTest.h
 * @author F. Gratl
 * @date 22.06.18
 */

#pragma once

#include <gtest/gtest.h>
#include "AutoPasTestBase.h"
#include "autopas/selectors/ContainerSelector.h"
#include "testingHelpers/commonTypedefs.h"

class ContainerSelectorTest : public AutoPasTestBase {
 public:
  ContainerSelectorTest() = default;
  ~ContainerSelectorTest() = default;
};
