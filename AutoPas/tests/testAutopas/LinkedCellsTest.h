/**
 * @file LinkedCellsTest.h
 * @author seckler
 * @date 27.04.18
 */

#pragma once

#include "AutoPasTestBase.h"
#include "autopas/cells/FullParticleCell.h"
#include "autopas/containers/LinkedCells.h"
#include "autopas/particles/Particle.h"
#include "testingHelpers/commonTypedefs.h"

class LinkedCellsTest : public AutoPasTestBase {};
