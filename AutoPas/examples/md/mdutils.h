/**
 * @file main.cpp
 * @date 18.01.2018
 * @author tchipev
 */

#pragma once

#include <cstdlib>
#include <iomanip>
#include <iostream>
#include "autopas/autopasIncludes.h"

using namespace std;
using namespace autopas;

class PrintableMolecule : public MoleculeLJ {
 public:
  PrintableMolecule() : MoleculeLJ() {}
  PrintableMolecule(std::array<double, 3> r, std::array<double, 3> v, unsigned long i) : MoleculeLJ(r, v, i) {}
  void print() {
    cout << "Molecule with position: ";
    for (auto &r : getR()) {
      cout << setw(10) << r << ", ";
    }
    cout << "and force: ";

    for (auto &f : getF()) {
      cout << setw(15) << f << ", ";
    }
    cout << "ID: " << setw(5) << getID();
    cout << endl;
  }
};

double fRand(double fMin, double fMax) {
  double f = static_cast<double>(rand()) / RAND_MAX;
  return fMin + f * (fMax - fMin);
}

std::array<double, 3> randomPosition(const std::array<double, 3> &boxMin, const std::array<double, 3> &boxMax) {
  std::array<double, 3> r{};
  for (int d = 0; d < 3; ++d) {
    r[d] = fRand(boxMin[d], boxMax[d]);
  }
  return r;
}

template <class Molecule, class MoleculeCell>
void fillContainerWithMolecules(int numMolecules, ParticleContainer<Molecule, MoleculeCell> *cont) {
  srand(42);  // fixed seedpoint

  std::array<double, 3> boxMin(cont->getBoxMin()), boxMax(cont->getBoxMax());

  for (int i = 0; i < numMolecules; ++i) {
    auto id = static_cast<unsigned long>(i);
    PrintableMolecule m(randomPosition(boxMin, boxMax), {0., 0., 0.}, id);
    cont->addParticle(m);
  }

  //	for (auto it = cont->begin(); it.isValid(); ++it) {
  //		it->print();
  //	}
}
